$(document).ready(function(){
    jQuery(window).load(function(){
        buildSemanticCore();
    })
});

function setEvents() {
    $('.js-semantic-core__plain-text-cell').on('paste', function(e) {
        e.preventDefault();
        $(this).html((e.originalEvent || e).clipboardData.getData('text/plain'));
    });


    $(document).on('keydown', '.js-semantic-core__plain-text-cell', function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            moveFocus = true;
            focusOwner = $(this);
            tableManager();
        }
    });


    $(document).on('change keyup paste', '.js-semantic-core__plain-text-cell', function(e) {
        tableManager();
    });


    $(document).on('focusout', '.js-semantic-core__plain-text-cell', function(e) {
        if (ignoreFocusOut == false) tableManager();
    });


    $(document).on('click', '.js-semantic-core__submit', function(e) {
        var core = new Array();
        var form = $('.js-form-wrapper .js-form');

        $('.js-semantic-core__table .js-row').each(function(index, el) {
            var id = $(this).data('entry');
            var word = $(this).find('.js-word').text().trim();
            var weight = $(this).find('.js-weight').text().trim();
            core.push({'id': id, 'word': word, 'weight': weight})
        });

        form.find('.js-semantic-core').val(JSON.stringify(core));

        var formData = new FormData(form[0]);
        var method = form.attr('method');
        var action = form.attr('action');

        var errorsBlock = $('.js-semantic-core__errors');
        errorsBlock.empty();
        errorsBlock.slideUp(300);
        //form.submit()
        $.ajax({url: action, type: method, data: formData, cache: false, processData: false, contentType: false})
            .done(function(data) {
                var response = jQuery.parseJSON(data);
                var errors = response.errors;

                if (errors.length == 0) {
                    errorsBlock.append('<p class="success">Данные успешно сохранены.</p>');
                } else {
                    $.each(errors, function(i, val) {
                        errorsBlock.append('<p class="error">' + val + '</p>');
                    });
                }

                $(document.body).animate({
                    'scrollTop':   $('#content').offset().top
                }, 750, function(){
                    errorsBlock.slideDown(300);
                });
            })
            .fail(function(data) {
                errorsBlock.append('<p class="error">Неопознанная техническая ошибка. Обратитесь к разработчику.</p>');
                $(document.body).animate({
                    'scrollTop':   $('#content').offset().top
                }, 750, function(){
                    errorsBlock.slideDown(300);
                });
            });
    });
}

function manage_table_rows(){
    function sendFocus(newRow) {
        ignoreFocusOut = true;
        var nextCell = focusOwner.next('.js-semantic-core__plain-text-cell');

        if (nextCell.length > 0) {
            nextCell.focus();
        } else {
            if (!lastRowIsEmpty) {
                if (!newRow) newRow = insertRow();
                newRow.find('.js-semantic-core__plain-text-cell').first().focus();
            } else {
                var thisRow = focusOwner.nearestParent('.js-row');
                var nextRow = thisRow.next('.js-row');
                nextRow.find('.js-semantic-core__plain-text-cell').first().focus();
            }
        }

        moveFocus = false;
        focusOwner = null;
        ignoreFocusOut = false;
    }

    function insertRow() {
        var table = $('.js-semantic-core__table');
        var newRow = table.find('.js-row-source').clone();

        newRow.find('.js-replace-class').each(function(index, el) {
            $(this).removeClass('js-replace-class');
            $(this).addClass($(this).data('class'));
        });

        newRow.removeClass('js-row-source');
        newRow.removeClass('hide');
        newRow.addClass('js-row');
        table.find('.js-body').append(newRow);

        return newRow;
    }

    var lastRowIsEmpty = true;
    var table = $('.js-semantic-core__table').first();
    var row = table.find('.js-row').last();
    row.find('.js-data-cell').each(function(index, el) {
        var text = $(this).text().trim();
        if (text.length > 0) {
            lastRowIsEmpty = false;
            return false;
        }
    });

    if (lastRowIsEmpty == false) {
        var newRow = insertRow();
        if (moveFocus) sendFocus(newRow);
    } else {
        if (moveFocus && focusOwner) sendFocus(null);
    }
}


function buildControlPanel() {
    var source = $('.js-semantic-core__control-panel.js-source');
    var panel = source.clone();
    source.remove();
    panel.removeClass('js-source').removeClass('hide');
    $('body').append(panel);
}


function buildSemanticCore() {
    var jqxhr = $.get('/manage/build-semantic-core/', function(data) {
        var wrapper = $('.js-semantic-core__table-wrapper');
        wrapper.empty();
        wrapper.html(data);
        buildControlPanel();
        setEvents();
    });
};


var tableManager = _.debounce(manage_table_rows, 500);
var moveFocus = false;
var focusOwner = null;
var ignoreFocusOut = false;
var lastRowIsEmpty = false;


$.fn.nearestParent = function(selector) {
    var parent = $(this).parent();
    if (parent.length == 0) return null;
    if (parent.is(selector)) return parent; else return parent.nearestParent(selector);
}