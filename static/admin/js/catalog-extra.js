$.fn.nearestParent = function(selector) {
    var parent = $(this).parent();
    if (parent.length == 0) return null;
    if (parent.is(selector)) return parent; else return parent.nearestParent(selector);
}


function initEditors() {
    editableBlocks = $('.catalog-extra-products__note-inner').not('.js-source');

    for (var i = 0; i < editableBlocks.length; i++) {
        CKEDITOR.inline(editableBlocks[i]);
    }
}


function replaceAll(target, search, replacement) {
    return target.replace(new RegExp(search, 'gi'), replacement);
};


function updateEditors() {
    for (var i in CKEDITOR.instances) {
        CKEDITOR.instances[i].updateElement();
    }
}

function getID() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }

    isNotUnique = true;
    var id = '';

    while (isNotUnique) {
        id = 'id_' + s4() + s4() + s4() + s4() + s4() + s4() + s4() + s4();
        isNotUnique = $('#' + id).length > 0
    }

    return id;
}


function getCaretPosition(editableDiv) {
    var caretPos = 0,
        sel, range;

        if (window.getSelection) {
            sel = window.getSelection();

            if (sel.rangeCount) {
                range = sel.getRangeAt(0);

                if (range.commonAncestorContainer.parentNode == editableDiv) {
                    caretPos = range.endOffset;
                }
            }
        } else if (document.selection && document.selection.createRange) {
            range = document.selection.createRange();

            if (range.parentElement() == editableDiv) {
                var tempEl = document.createElement("span");
                editableDiv.insertBefore(tempEl, editableDiv.firstChild);

                var tempRange = range.duplicate();
                tempRange.moveToElementText(tempEl);
                tempRange.setEndPoint("EndToEnd", range);
                caretPos = tempRange.text.length;
            }
    }

    return caretPos;
}


function setColumnVisibility() {
    if ($('.js-show-images').prop('checked')) {
        $('.js-photo-column').removeClass('hide');
    } else {
        $('.js-photo-column').addClass('hide');
    }

    if ($('.js-show-notes').prop('checked')) {
        $('.js-note-column').removeClass('hide');
    } else {
        $('.js-note-column').addClass('hide');
    }

    if ($('.js-show-features').prop('checked')) {
        $('.js-feature-column').removeClass('hide');
    } else {
        $('.js-feature-column').addClass('hide');
    }

    if ($('.js-show-basic').prop('checked')) {
        $('.js-category-column').removeClass('hide');
        $('.js-brand-column').removeClass('hide');
        $('.js-line-column').removeClass('hide');
        $('.js-price-column').removeClass('hide');
    } else {
        $('.js-category-column').addClass('hide');
        $('.js-brand-column').addClass('hide');
        $('.js-line-column').addClass('hide');
        $('.js-price-column').addClass('hide');
    }
}


function cloneSourceElement(selector) {
    return $(selector + '.js-source').clone().removeClass('js-source');
}


function updateProducts() {
    updateEditors();
    var form = $('.filter-form');
    var url = form.attr('action') + '?' + form.serialize();
    $('.catalog-extra__save-button').addClass('hide');

    var jqxhr = $.ajax(url)
        .done(function(response) {
            var data = jQuery.parseJSON(response);
            var container = $('.catalog-extra-product-container');
            var table = cloneSourceElement('.catalog-extra-products');

            var imageHead = cloneSourceElement('.catalog-extra-products__image-head');

            if (data.max_images > 0) imageHead.attr('colspan', data.max_images + 1);

            var headCells = [
                cloneSourceElement('.catalog-extra-products__name-head'),
                cloneSourceElement('.catalog-extra-products__category-head'),
                cloneSourceElement('.catalog-extra-products__brand-head'),
                cloneSourceElement('.catalog-extra-products__line-head'),
                cloneSourceElement('.catalog-extra-products__price-head'),
                cloneSourceElement('.catalog-extra-products__note-head'),
                imageHead,
            ];

            var fieldActionSelect = $('.catalog-extra-action-cell');
            fieldActionSelect.find('.js-dynamic').remove();

            var existFeatureSelect = $('.catalog-extra-action-exist-feature');
            existFeatureSelect.find('.js-dynamic').remove();

            $.each(data.features, function(featureIndex, feature) {
                var featureHead = cloneSourceElement('.catalog-extra-products__feature-head');
                var title = feature.name;
                if (feature.units) title = title + ', ' + feature.units;

                featureHead.text(title);
                headCells.push(featureHead)

                fieldActionSelect.append('<option class="js-dynamic" data-pk="' + feature.pk + '" value="js-feature-data__' + feature.pk + '">' + title + '</option>');
                existFeatureSelect.append('<option class="js-dynamic" data-pk="' + feature.pk + '" value="js-feature-data__' + feature.pk + '">' + title + '</option>')
            });

            var headRow = table.find('.catalog-extra-products__head-row');

            $.each(headCells, function(index, headCell) {
                headRow.append(headCell);
            });

            var allFeatureSelect = $('.catalog-extra-action-all-feature');
            allFeatureSelect.find('.js-dynamic').remove();

            $.each(data.another_features, function(featureIndex, feature) {
                var title = feature.name;
                if (feature.units) title = title + ', ' + feature.units;
                allFeatureSelect.append('<option class="js-dynamic" data-pk="' + feature.pk + '" value="js-feature-data__' + feature.pk + '">' + title + '</option>');
            });

            var categorySelect = $('.catalog-extra-products__category.js-source select');
            categorySelect.find('.js-dynamic').remove();

            $.each(data.categories, function(categoryIndex, category) {
                categorySelect.append('<option class="js-dynamic" value="' + category.pk + '">' + category.name + '</option>');
            });

            var brandSelect = $('.catalog-extra-products__brand.js-source select');
            brandSelect.find('.js-dynamic').remove();

            $.each(data.brands, function(brandindex, brand) {
                brandSelect.append('<option class="js-dynamic" value="' + brand.pk + '">' + brand.name + '</option>');
            });

            var lineSelect = $('.catalog-extra-products__line.js-source select');
            lineSelect.find('.js-dynamic').remove();

            $.each(data.lines, function(lineIndex, line) {
                lineSelect.append('<option class="js-dynamic" value="' + line.pk + '">' + line.name + '</option>');
            });

            $.each(data.products, function(productIndex, product) {
                var productRow = cloneSourceElement('.catalog-extra-products__product-row');

                var nameCell = cloneSourceElement('.catalog-extra-products__name');
                nameCell.text(product.name);

                productRow.append(nameCell);

                var categoryCell = cloneSourceElement('.catalog-extra-products__category');
                categoryCell.find('select').val(product.category);
                productRow.append(categoryCell);

                var brandCell = cloneSourceElement('.catalog-extra-products__brand');
                brandCell.find('select').val(product.brand);
                productRow.append(brandCell);

                var lineCell = cloneSourceElement('.catalog-extra-products__line');
                lineCell.find('select').val(product.line);
                productRow.append(lineCell);

                priceCell = cloneSourceElement('.catalog-extra-products__price');
                priceCell.text(product.price);

                productRow.append(priceCell);

                var noteCell = cloneSourceElement('.catalog-extra-products__note');
                noteCell.find('div').removeClass('js-source').html(product.note);

                productRow.append(noteCell);

                $.each(product.images, function(imageIndex, image) {
                    var imageCell = cloneSourceElement('.catalog-extra-products__image');

                    if (image) {
                        imageCell.data('pk', image.pk);
                        imageCell.find('img').attr('src', '/files/' + image.image);
                        imageCell.find('.catalog-extra-products__image-size').text(image.width + 'x' + image.height);

                        if (image.is_main) imageCell.find('.js-set-main-photo').prop('checked', true);
                    } else {
                        imageCell.html('');
                    }

                    productRow.append(imageCell);
                });

                var uploaderCell = cloneSourceElement('.catalog-extra-products__image-uploader-container');
                var uploader = cloneSourceElement('.catalog-extra-products__image-uploader-outer');

                uploaderCell.find('div').append(uploader);

                productRow.append(uploaderCell);

                $.each(product.features, function(featureIndex, features) {
                    var featureCell = cloneSourceElement('.catalog-extra-products__feature');
                    featureCell.addClass('js-feature-data__' + features.pk);
                    featureCell.data('pk', features.pk);

                    var values = '';
                    $.each(features.value, function(i, f) {
                        values = values + '<p>' + f + '</p>';
                    });
                    featureCell.html(values);
                    productRow.append(featureCell);
                });

                productRow.data('pk', product.pk);
                productRow.addClass('js-product-' + product.pk);

                table.append(productRow);
            });

            container.empty();
            container.append(table);
            setColumnVisibility();
            initEditors();

            var paginator = $('.catalog-extra-paginator');
            paginator.empty();
            $.each(data.pages, function(index, page) {
                var p = page;
                if (page == data.page) {
                    p = '<span class="catalog-extra-paginator__current">' + p + '</span> '
                } else {
                    p = '<span class="catalog-extra-paginator__page">' + p + '</span> '
                }

                paginator.append(p);
            });

            $('.catalog-filter__submit').show();
            $('.catalog-extra__save-button').removeClass('hide');

        })
        .fail(function() {
            alertify.error('Ой, кажется что-то пошло не так :(');
        });
}

function saveProducts() {
    $('.catalog-extra__save-button').addClass('hide');

    var productRows = $('.catalog-extra-products__product-row').not('.js-source');
    var form = $('.save-form');

    var names = new Object();
    var prices = new Object();
    var categories = new Object();
    var brands = new Object();
    var lines = new Object();
    var notes = new Object();
    var features = new Object();
    var imageActions = new Object();
    var newImages = [];

    productRows.each(function(){
        var row = $(this);
        var pk = row.data('pk');

        names[pk] = row.find('.js-name-data').text();
        notes[pk] = row.find('.js-note-data').html();
        prices[pk] = row.find('.js-price-data').text();
        categories[pk] = row.find('.js-category-data').find('select').val();
        brands[pk] = row.find('.js-brand-data').find('select').val();
        lines[pk] = row.find('.js-line-data').find('select').val();

        features[pk] = new Object();
        row.find('.js-feature-column').each(function(){
            features[pk][$(this).data('pk')] = $(this).html();
        });

        row.find('.catalog-extra-products__image').each(function(){
            var imagePk = $(this).data('pk');
            if (imagePk) {
                imageActions[imagePk] = new Object();
                imageActions[imagePk].is_main = $(this).find('.js-set-main-photo').prop('checked');
            }
        });

        row.find('.catalog-extra-products__uploader').each(function(){
            if ($(this).val()) {
                var id = getID();
                $(this).attr('id', id);
                $(this).attr('name', id);
                $(this).attr('form', 'save-form');
                newImages.push({'pk': pk, 'field': id});
            }
        });
    });

    form.find('.save-form__names').val(JSON.stringify(names));
    form.find('.save-form__prices').val(JSON.stringify(prices));
    form.find('.save-form__categories').val(JSON.stringify(categories));
    form.find('.save-form__brands').val(JSON.stringify(brands));
    form.find('.save-form__lines').val(JSON.stringify(lines));
    form.find('.save-form__notes').val(JSON.stringify(notes));
    form.find('.save-form__features').val(JSON.stringify(features));
    form.find('.save-form__image-actions').val(JSON.stringify(imageActions));
    form.find('.save-form__new-images').val(JSON.stringify(newImages));

    $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: new FormData(form[0]),
        cache: false,
        processData: false,
        contentType: false,
    })
    .done(function(data) {
        updateProducts();
    })
}

$(document).ready(function() {
    $(document).on('click', '.catalog-filter-icon, .catalog-filter__close', function(event) {
        $('.catalog-filter').slideToggle();
    });

    $(document).on('click', '.catalog-filter__submit', function(event) {
        $('.catalog-filter__page-no').val(1);
        var button = $(this);
        $('.catalog-filter').slideUp(300, function(){
            button.hide();
            updateProducts();
        })

    });

    $(document).on('click', '.js-set-main-photo', function(event) {
        if ($(this).prop('checked')) {
            var row = $(this).nearestParent('.catalog-extra-products__product-row');
            row.find('.js-set-main-photo').not($(this)).prop('checked', false);
        }
    });

    $(document).on('click', '.catalog-extra-products__select-all', function(event) {
        var table = $(this).nearestParent('.catalog-extra-products');
        table.find('.catalog-extra-products__select-this').prop('checked', $(this).prop('checked'));
    });

    $(document).on('change', '.js-show-basic, .js-show-images, .js-show-notes, .js-show-features', function(event) {
        setColumnVisibility();
    });

    $(document).on('click', '.catalog-extra-products__select-this', function(event) {
        var table = $(this).nearestParent('.catalog-extra-products');

        var hasTrue = false;
        var hasFalse = false;
        var commonFlag = table.find('.catalog-extra-products__select-all');

        table.find('.catalog-extra-products__select-this').each(function(){
            if ($(this).prop('checked')) {
                hasTrue = true;
            } else {
                hasFalse = true;
            }
        });

        if (hasTrue && !hasFalse) {
            commonFlag.prop('checked', true);
        } else if (hasFalse && !hasTrue) {
            commonFlag.prop('checked', false);
        } else {
            commonFlag.prop('indeterminate', true);
        }
    });

    $(document).on('click', '.catalog-extra-products__add-more-uploader', function(event) {
        var cell = $(this).nearestParent('.catalog-extra-products__image-uploader-container');
        var uploader = cloneSourceElement('.catalog-extra-products__image-uploader-outer');
        cell.find('.catalog-extra-products__uploader-container-inner').append(uploader);
    });

    $(document).on('click', '.catalog-extra-paginator__page', function(event) {
        $('.catalog-filter__page-no').val($(this).text());
        updateProducts();;
    });


    $(document).on('paste', '.catalog-extra-products__name, .catalog-extra-products__feature, .catalog-extra-products__price', function(event) {
        event.preventDefault();
        var caretPosition = getCaretPosition(this) || 0,
            injection = (event.originalEvent || event).clipboardData.getData('text/plain'),
            content = $(this).text() || '';

        injection = $('<div>' + injection + '</div>').text().replace(/(\r\n|\n|\r)/gm,"");

        if ((caretPosition) <= content.length) {
            var before = content.substring(0, caretPosition) || '',
                after = content.substring(caretPosition, content.length) || '';

            content = before + injection + after;
        } else {
            content = injection;
        }
        $(this).html(content);
    });

    $(document).on('change', '.catalog-extra-action', function(event) {
        var action = $(this).val();
        var table = $(this).nearestParent('.catalog-extra-actions');
        var exec = table.find('.catalog-extra-actions__exec-container');
        var cell = table.find('.catalog-extra-actions__cell-container');
        var replacement = table.find('.catalog-extra-actions__replace-container');
        var existFeature = table.find('.catalog-extra-actions__exist-feature-container');
        var featureValue = table.find('.catalog-extra-actions__feature-value-container');
        var allFeature = table.find('.catalog-extra-actions__all-feature-container');

        switch (action) {
            case '':
                exec.addClass('hide');
                cell.addClass('hide');
                replacement.addClass('hide');
                existFeature.addClass('hide');
                featureValue.addClass('hide');
                allFeature.addClass('hide');
                break;
            case 'remove':
                exec.removeClass('hide');
                cell.addClass('hide');
                replacement.addClass('hide');
                existFeature.addClass('hide');
                featureValue.addClass('hide');
                allFeature.addClass('hide');
                break;
            case 'find-n-replace':
                exec.removeClass('hide');
                cell.removeClass('hide');
                replacement.removeClass('hide');
                existFeature.addClass('hide');
                featureValue.addClass('hide');
                allFeature.addClass('hide');
                break;
            case 'set-feature':
                exec.removeClass('hide');
                cell.addClass('hide');
                replacement.addClass('hide');
                existFeature.removeClass('hide');
                featureValue.removeClass('hide');
                allFeature.addClass('hide');
                break;
            case 'add-feature':
                exec.removeClass('hide');
                cell.addClass('hide');
                replacement.addClass('hide');
                existFeature.addClass('hide');
                featureValue.addClass('hide');
                allFeature.removeClass('hide');
                break;
        }
    });

    $(document).on('click', '.catalog-extra-actions__exec', function(event) {

        var table = $(this).nearestParent('.catalog-extra-actions');
        var actionSelect = table.find('.catalog-extra-action');
        var action = actionSelect.val();

        var productTable = $('.catalog-extra-products').not('.js-source');
        var checked = productTable.find('.catalog-extra-products__select-this:checked');

        var isValid = true;

        if (action != 'add-feature') {
            if (checked.length == 0) {
                isValid = false;
                alertify.error('Не выбраны товары над которыми осуществляется действие.');
            }
        }

        if (isValid) {
            updateEditors();

            switch (action) {
                case 'remove':
                    alertify.confirm("Подтверждение действия", "Точно исполняем?",
                        function(){
                            actionSelect.val('');
                            actionSelect.trigger('change');

                            checked.each(function(){
                                $(this).nearestParent('.catalog-extra-products__product-row').remove();
                            })

                            productTable.find('.catalog-extra-products__select-all').prop('indeterminate', false).prop('checked', false);

                            if (productTable.find('.catalog-extra-products__product-row').length == 0) {
                                alertify.warning('Получилось, но похоже все товары выборки были удалены.');
                            } else {
                                alertify.success('Получилось!');
                            }
                        },
                        function(){}
                    );
                    break;
                case 'find-n-replace':
                    var cellSelect = table.find('.catalog-extra-action-cell');
                    var replaceWhatContainer = table.find('.js-replace-what');
                    var replaceWithContainer = table.find('.js-replace-with');

                    var cell = cellSelect.val();
                    var replaceWhat = replaceWhatContainer.val();
                    var replaceWith = replaceWithContainer.val() || '';

                    if (!cell) {
                        isValid = false;
                        alertify.error('Не выбран стобец для замены.');
                    }

                    if (!replaceWhat) {
                        isValid = false;
                        alertify.error('Не введен заменяемый текст.');
                    }

                    if (isValid) {
                        alertify.confirm("Подтверждение действия", "Точно исполняем?",
                            function(){
                                checked.each(function(){
                                    var row = $(this).nearestParent('.catalog-extra-products__product-row');
                                    row.find('.' + cell).each(function(){
                                        var html = $(this).html();
                                        html = replaceAll(html, replaceWhat, replaceWith);
                                        $(this).html(html);
                                    })
                                })

                                alertify.success('Получилось!');

                                cellSelect.val('');
                                replaceWhatContainer.val('');
                                replaceWithContainer.val('');
                                actionSelect.val('');
                                actionSelect.trigger('change');

                                $('.catalog-extra-products__select-all').prop('checked', false);
                                $('.catalog-extra-products__select-this').prop('checked', false);
                            },
                            function(){}
                        );
                    }
                    break;
                case 'add-feature':
                    var featureSelect = table.find('.catalog-extra-action-all-feature');
                    var value = featureSelect.val();

                    if (!value) {
                        isValid = false;
                        alertify.error('Не выбрана добавляемая характеристика.');
                    }

                    if (isValid) {
                        var existSelect = table.find('.catalog-extra-action-exist-feature');
                        var cellSelect = table.find('.catalog-extra-action-cell');

                        var oldOption = featureSelect.find('option:selected');
                        var featurePk = oldOption.data('pk');

                        var featureName = oldOption.text();

                        cellSelect.append(oldOption.clone().removeAttr('selected'));
                        existSelect.append(oldOption.clone().removeAttr('selected'));
                        oldOption.remove();
                        alertify.success('Получилось!');

                        var headCell = cloneSourceElement('.catalog-extra-products__feature-head');

                        headCell.html(featureName);
                        productTable.find('.catalog-extra-products__head-row').append(headCell);

                        productTable.find('.catalog-extra-products__product-row').each(function() {
                            var featureCell = cloneSourceElement('.catalog-extra-products__feature');
                            featureCell.addClass(value);
                            featureCell.data('pk', featurePk);
                            $(this).append(featureCell);
                        });

                        cellSelect.val('');
                        featureSelect.val('');
                        actionSelect.val('');
                        actionSelect.trigger('change');

                        $('.js-show-features').prop('checked', true);
                        setColumnVisibility();

                        $('.catalog-extra-products__select-all').prop('checked', false);
                        $('.catalog-extra-products__select-this').prop('checked', false);
                    }

                    break;
                case 'set-feature':
                    var cellSelect = table.find('.catalog-extra-action-exist-feature');
                    var valueContainer = table.find('.catalog-extra-action-feature-value');

                    var cell = cellSelect.val();
                    var value = valueContainer.val() || '';
                    var values = [];

                    $.each(value.split('\n') || [], function(i, val) {
                        if (val && replaceAll(val, ' ', '') != '') values.push(val);
                    });

                    if (!cell) {
                        isValid = false;
                        alertify.error('Не выбрана характеристика для установки значения.');
                    }

                    if (isValid) {
                        alertify.confirm("Подтверждение действия", "Точно исполняем?",
                            function(){
                                checked.each(function(){
                                    var row = $(this).nearestParent('.catalog-extra-products__product-row');
                                    row.find('.' + cell).each(function(){
                                        var html = '';
                                        $.each(values, function(i, val) {
                                            html = html + '<p>' + val + '</p>'
                                        });

                                        $(this).html(html);
                                    })
                                })

                                alertify.success('Получилось!');

                                cellSelect.val('');
                                valueContainer.val('');
                                actionSelect.val('');
                                actionSelect.trigger('change');
                                $('.js-show-features').prop('checked', true);
                                setColumnVisibility();
                                $('.catalog-extra-products__select-all').prop('checked', false);
                                $('.catalog-extra-products__select-this').prop('checked', false);
                            },
                            function(){}
                        );
                    }

                    break;
            }
        }
    });

    $(document).on('click', '.catalog-extra__save-button', function(event) {
        alertify.confirm("Подтверждение действия", "Точно сохраняем?",
            function(){saveProducts()},
            function(){}
        );
    });

    $('.catalog-filter__category-tree').Tree();
    $('.catalog-filter__brand-select').chosen({width: '100%'});
    $('.catalog-filter__line-select').chosen({width: '100%'});

    updateProducts();
});