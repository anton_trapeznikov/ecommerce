CKEDITOR.editorConfig = function( config ) {
    config.toolbar_adminStandart =
    [
        ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', 'FontSize', 'Image'],
        ['Cut', 'Copy', 'PasteText'],
        ['Undo', 'Redo'],
        ['RemoveFormat',],
        ['NumberedList', 'BulletedList',],
        ['Link', ],
        ['Source'],
    ];
    config.toolbar_adminExtended =
    [
        ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', 'TextColor', 'FontSize'],
        ['Cut', 'Copy', 'PasteText'],
        ['Undo', 'Redo'],
        ['RemoveFormat', 'Format'],
        ['NumberedList', 'BulletedList',],
        ['Link', ],
        ['Source'],
    ];
    config.toolbar_page =
    [
        ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', 'TextColor', 'FontSize'],
        ['Cut', 'Copy', 'PasteText'],
        ['Undo', 'Redo'],
        ['RemoveFormat', 'Format'],
        ['NumberedList', 'BulletedList',],
        ['Link', ],
        ['Source'],
    ];
};
