from django.conf.urls import url
import apps.forms.views

urlpatterns = [
    url(r'^get/(?P<whois>[-\w]+)/$', apps.forms.views.form, name='form'),
]
