from django.contrib import admin
from apps.forms.models import *


class CallAdmin(admin.ModelAdmin):
    list_display = ('name', 'phone', 'created_at',)
    search_fields = ['name', 'phone']
    readonly_fields = ('updated_at', 'created_at',)


class FeedbackAdmin(admin.ModelAdmin):
    list_display = ('name', 'phone', 'email', 'created_at',)
    search_fields = ['name', 'phone', 'email']
    readonly_fields = ('updated_at', 'created_at',)


admin.site.register(Call, CallAdmin)
admin.site.register(Feedback, FeedbackAdmin)
