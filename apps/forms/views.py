from django.http import Http404, HttpResponse
from django.views.decorators.csrf import ensure_csrf_cookie
from django.template.response import TemplateResponse
from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.conf import settings
from ecommerce.models import NotificationRecipients
from apps.forms.models import *
from apps.forms.forms import *
import json


@ensure_csrf_cookie
def form(request, whois):
    whois = whois.lower()
    form = None

    if whois == 'call':
        form = CallForm(request.POST)
    elif whois == 'feedback':
        form = FeedbackForm(request.POST)
    elif whois == 'quick-order':
        form = QuickOrderForm(request.POST)

    if form is None:
        raise Http404

    if request.method == 'POST':
        context = {
            'success': False,
        }

        if form.is_valid():
            model_instance = form.save(commit=False)
            model_instance.save()

            names = {
                'call': 'Заказ обратного звонка',
                'feedback': 'Форма обратной связи'
            }

            if whois in names.keys():
                model_instance.form_name = names[whois]

                notification = render_to_string('forms/notification.html', {
                    'form': model_instance,
                })

                recipients = NotificationRecipients.objects.all()
                emails = list([nr.email.lower() for nr in recipients])
                send_mail(
                    subject='Обращение с сайта',
                    message=notification,
                    html_message=notification,
                    from_email=settings.EMAIL_HOST_USER,
                    recipient_list=emails,
                    fail_silently=False
                )
                context['success'] = True
        else:
            context['success'] = False
            context['errors-fields'] = form.errors.keys()

        return HttpResponse(json.dumps(context), content_type='application/json')
    else:
        content = TemplateResponse(request, 'forms/form.html', {'form': form, 'whois': whois})
        content = content.render()

        return HttpResponse(content.rendered_content)
