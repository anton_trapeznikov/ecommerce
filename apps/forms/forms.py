from django import forms
from django.forms import ModelForm
from apps.forms.models import *
from apps.order.models import Order


class CallForm(ModelForm):
    class Meta:
        model = Call
        exclude = ['created_at', 'updated_at']


class FeedbackForm(ModelForm):
    class Meta:
        model = Feedback
        exclude = ['created_at', 'updated_at']


class QuickOrderForm(ModelForm):
    class Meta:
        model = Order
        exclude = [
            'created_at', 'updated_at', 'price',
            'length', 'uid', 'address', 'comment', 'email'
        ]

        widgets = {
            'phone': forms.TextInput(attrs={
                'autofocus': 'autofocus',
                'required': 'required',
                'placeholder': 'Номер телефона',
            }),
            'name': forms.TextInput(attrs={'placeholder': 'Ваше имя'}),
        }


class OrderForm(ModelForm):
    class Meta:
        model = Order
        exclude = ['created_at', 'updated_at', 'price', 'length', 'uid']

        widgets = {
            'phone': forms.TextInput(attrs={
                'autofocus': 'autofocus',
                'required': 'required',
                'placeholder': 'Номер телефона',
            }),
            'email': forms.EmailInput(attrs={
                'placeholder': 'Email для уведомлений',
            }),
            'name': forms.TextInput(attrs={'placeholder': 'Ваше имя'}),
            'address': forms.Textarea(attrs={'placeholder': 'Адрес доставки'}),
            'comment': forms.Textarea(attrs={'placeholder': 'Комментарий'}),
        }
