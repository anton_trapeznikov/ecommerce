from django.db import models
from apps.helpers.models import LogMixin


class Call(LogMixin):
    name = models.CharField(max_length=100, verbose_name='Имя')
    phone = models.CharField(max_length=100, verbose_name='Телефон')

    class Meta():
        verbose_name = 'Заказ обратного звонка'
        verbose_name_plural = 'Заказ обратного звонка'

    def __str__(self):
        return '%s (%s)' % (self.phone, self.name)


class Feedback(LogMixin):
    name = models.CharField(max_length=100, verbose_name='Имя')
    phone = models.CharField(default='', max_length=100, blank=True, verbose_name='Телефон')
    email = models.EmailField(default='', blank=True, verbose_name='Email')
    message = models.TextField(default='', blank=True, verbose_name='Сообщение')

    class Meta():
        verbose_name = 'Форма обратной связи'
        verbose_name_plural = 'Форма обратной связи'

    def __str__(self):
        return self.email
