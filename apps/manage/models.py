from django.db import models
from apps.catalog.models import *
from ecommerce.models import *


class SemanticCore(models.Model):
    word = models.CharField(max_length=255, unique=True, verbose_name='Запрос')
    weight = models.IntegerField(default=0, blank=True, db_index=True, verbose_name='Вес')
    homepage = models.BooleanField(default=False, blank=True, db_index=True, verbose_name='На главную страницу')
    catalog_index = models.BooleanField(default=False, blank=True, db_index=True, verbose_name='Главная каталога')
    root_index = models.BooleanField(default=False, blank=True, db_index=True, verbose_name='Главная брендов')
    page = models.ForeignKey(Page, blank=True, null=True, verbose_name='Контентная страница')
    category = models.ForeignKey(Category, blank=True, null=True, verbose_name='Категория')
    product = models.ForeignKey(Product, blank=True, null=True, verbose_name='Товар')
    brand = models.ForeignKey(Brand, blank=True, null=True, verbose_name='Бренд')

    def __str__(self):
        return self.word

    class Meta:
        verbose_name = 'Семантическое ядро'
        verbose_name_plural = 'Семантическое ядро'


class FakeModelOne(models.Model):
    class Meta():
        verbose_name = 'Расширенное управление каталогом'
        verbose_name_plural = 'Расширенное управление каталогом'

    def __str__(self):
        return '%s' % self.pk
