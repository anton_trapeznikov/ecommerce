from django.contrib import admin
from apps.manage.models import *


class SemanticCoreAdmin(admin.ModelAdmin):
    list_display = ('word', 'weight', 'homepage', 'catalog_index', 'root_index', 'page', 'category', 'product', 'brand')
    search_fields = ['word', ]


# admin.site.register(SemanticCore, SemanticCoreAdmin)
admin.site.register(FakeModelOne)
