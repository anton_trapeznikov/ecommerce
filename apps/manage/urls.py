from django.conf.urls import url
import apps.manage.views

urlpatterns = [
    url(r'^build-semantic-core/$', apps.manage.views.build_semantic_core, name='build_semantic_core'),
    url(r'^save-semantic-core/$', apps.manage.views.save_semantic_core, name='save_semantic_core'),
]