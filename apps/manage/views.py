from django.contrib.auth.decorators import permission_required
from django.contrib.auth.decorators import login_required
from django.template.response import TemplateResponse
from django.core.exceptions import ObjectDoesNotExist
from django.template.loader import render_to_string
from django.http import Http404, HttpResponse
from django.utils.html import strip_tags
from apps.helpers.utils import to_int, obj_by_pk
from apps.catalog.models import *
from ecommerce.models import *
from apps.manage.models import *
import random
import json


@login_required
@permission_required(['manage.add_semanticcore', 'manage.change_semanticcore', 'manage.delete_semanticcore',])
def build_semantic_core(request):
    core = SemanticCore.objects.all().order_by('word');

    response = TemplateResponse(request, 'manage/semantic_core_table.html', {
        'core': core,
        'word_index': json.dumps([c.pk for c in core])
    })
    response = response.render()

    return HttpResponse(response.rendered_content)


@login_required
@permission_required(['manage.add_semanticcore', 'manage.change_semanticcore', 'manage.delete_semanticcore',])
def save_semantic_core(request):
    if request.method == 'POST':
        try:
            word_index = json.loads(request.POST.get('word-index', ''))
        except:
            word_index = []

        try:
            core_data = json.loads(request.POST.get('semantic-core', ''))
        except:
            core_data = []

        processed_words = []
        errors = []

        current_row = 1
        for c in core_data:
            is_valid = True

            word = c['word'] or ''
            word = strip_tags(word.strip().lower())
            weight = to_int(c['weight'], None)
            wpk = to_int(c['id'], None)

            is_not_empty = wpk or word or weight

            if is_not_empty:
                entry = None
                weight = weight or 1
                if not word:
                    is_valid = False
                    errors.append('Ошибка в строке №%s. Не введено ключевое слово.' % current_row)

                if is_valid:
                    can_save = False
                    wpk = to_int(c['id'], None)

                    if wpk:
                        entry = obj_by_pk(SemanticCore, wpk)

                    if entry is None:
                        entry = SemanticCore()
                        can_save = True

                    if entry.pk is None:
                        is_unique = SemanticCore.objects.filter(word=word).count() == 0
                    else:
                        is_unique = SemanticCore.objects.filter(word=word).exclude(pk=entry.pk).count() == 0
                        processed_words.append(entry.pk)

                    if not is_unique:
                        is_valid = False
                        errors.append('Ошибка в строке №%s. Ключевое слово уже существует в семантическом ядре.' % current_row)

                    if is_valid:
                        if entry.word != word:
                            entry.word = word
                            can_save = True

                        if entry.weight != weight:
                            entry.weight = weight
                            can_save = True

                        if can_save:
                            entry.save()

            current_row += 1

        response = json.dumps({'errors': errors,})
        return HttpResponse(response)

    raise Http404
