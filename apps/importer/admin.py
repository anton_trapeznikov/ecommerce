from django.contrib import admin
from apps.importer.models import ImportTask


@admin.register(ImportTask)
class ImportAdmin(admin.ModelAdmin):
    list_filter = ('is_processed',)
    list_display = (
        'created_at',
        'updated_at',
        'is_processed',
        'updated_products',
    )
    readonly_fields = (
        'updated_at',
        'created_at',
        'is_processed',
        'invalid_rows',
        'updated_products'
    )
