from django.core.management.base import BaseCommand
from django.core.exceptions import ObjectDoesNotExist
from apps.catalog.models import Category, Brand, Product
from apps.importer.models import ImportTask
import hashlib
import xlrd


class Command(BaseCommand):
    def get_hash(self, val):
        return hashlib.md5(val.lower().encode('utf-8')).hexdigest()

    def get_brand_id(self, brand_name):
        if not hasattr(self, 'brand_map'):
            self.brand_map = {}

        if brand_name in self.brand_map:
            return self.brand_map[brand_name]

        brand = Brand.objects.filter(name=brand_name).first()
        if brand is None:
            brand = Brand(name=brand_name)
            brand.save()

        result = brand.pk if brand else None
        self.brand_map[brand_name] = result
        return result

    def get_category_id(self, branch):
        if not branch:
            return None

        if not hasattr(self, 'category_map'):
            self.category_map = {}
        key = self.get_hash(''.join(branch))

        if key in self.category_map:
            return self.category_map[key]

        prev = None

        for name in branch:
            if not name:
                break

            if name[0].islower():
                name = name[0].upper() + name[1:]

            category = Category.objects.filter(
                parent=prev,
                name=name
            ).first()

            if category is None:
                category = Category(name=name, parent=prev)
                category.save()

            prev = category

        result = prev.pk if prev else None
        self.category_map[key] = result
        return result

    def handle(self, *args, **options):
        task = ImportTask.objects.filter(
            is_processed=False
        ).order_by('created_at').first()

        if not task:
            print('No new tasks available.')
        else:
            book = xlrd.open_workbook(task.import_file.path)
            sheet = book.sheet_by_index(0)

            invalid_rows = []
            inserting_count = 0
            updated_count = 0

            import_data = []

            for row_index, row_num in enumerate(range(sheet.nrows)):
                if row_num == 0:
                    continue

                row = sheet.row_values(row_num)

                product_data = {
                    'code': None,
                    'brand_id': None,
                    'category_id': None,
                    'name': None,
                    'instock': None,
                    'price': None,
                }

                branch = []

                for index, value in enumerate(row):
                    value = value or ''
                    data = str(value).strip()[:255]

                    if 0 <= index <= 13:
                        if data:
                            if index == 0:
                                product_data['code'] = data[:32]

                            if index == 1:
                                product_data['name'] = data

                            if index == 2:
                                data = data.replace(',', '.')
                                instock = int(float(data)) == float(data) == 1
                                product_data['instock'] = instock

                            if index == 3:
                                product_data['price'] = "{0:.2f}".format(
                                    round(float(data.replace(',', '.')), 2)
                                )

                            if index == 4:
                                # TODO discount
                                pass

                            if index == 5:
                                product_data['brand_id'] = self.get_brand_id(
                                    data
                                )

                            if 6 <= index <= 13:
                                branch.append(data)
                    else:
                        continue

                product_data['category_id'] = self.get_category_id(
                    branch
                )

                is_valid = None not in product_data.values()

                if not is_valid:
                    invalid_rows.append(str(row_num))

                else:
                    p = product_data
                    product = Product.objects.filter(code=p['code']).first()
                    if product is None:
                        product = Product()

                    can_save = False

                    if p['name'] != product.name:
                        product.name = p['name']
                        can_save = True

                    if p['code'] != product.code:
                        product.code = p['code']
                        can_save = True

                    if p['instock'] != product.instock:
                        product.instock = p['instock']
                        can_save = True

                    price = "{0:.2f}".format(round(product.price or 0, 2))
                    if p['price'] != price:
                        product.price = float(p['price'])
                        can_save = True

                    if p['category_id'] != product.category_id:
                        product.category_id = p['category_id']
                        can_save = True

                    if p['brand_id'] != product.brand_id:
                        product.brand_id = p['brand_id']
                        can_save = True

                    if can_save:
                        product.save()
                        updated_count += 1

            task.updated_products = updated_count
            task.invalid_rows = ', '.join(invalid_rows)
            task.is_processed = True
            task.save()
            print('Done')
