from django.db import models
from apps.helpers.models import LogMixin
from apps.helpers.utils import generate_upload_name


class ImportTask(LogMixin):
    is_processed = models.BooleanField(
        default=False,
        blank=True,
        db_index=True,
        verbose_name='Исполнено'
    )
    import_file = models.FileField(
        upload_to=generate_upload_name,
        verbose_name='Файл с данными',
        help_text='Файл Excel заданной структуры.'
    )
    invalid_rows = models.TextField(
        default='',
        blank=True,
        verbose_name='Строки с ошибками',
        help_text='Номера строк в файле, где имеются ошибки.'
    )
    updated_products = models.PositiveIntegerField(
        blank=True,
        null=True,
        verbose_name='Обновлено товаров',
        help_text='Количество добавленных/изменененных товаров.'
    )

    def __str__(self):
        return 'Задача №%s' % self.pk

    class Meta:
        verbose_name = 'Задача импорта каталога'
        verbose_name_plural = 'Задачи импорта каталога'
