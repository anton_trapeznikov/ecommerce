from django.apps import AppConfig


class ImporterConfig(AppConfig):
    name = 'apps.importer'
    verbose_name = "Импорт товаров из Excel"