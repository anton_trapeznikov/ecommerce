from django.core.exceptions import ObjectDoesNotExist
from django.db.models.query import QuerySet
from haystack.query import EmptySearchQuerySet
from haystack.query import SearchQuerySet
from django.utils.text import slugify
from unidecode import unidecode
from Stemmer import Stemmer
from hashlib import md5
from time import time
from os import path
import random
import math
import re


quotes_re = re.compile(r'([\'"].*?[\'"])', re.S)


def declination(number=0, cases=[]):
    '''
        cases должен содержать склонения сущности для чисел 1, 3, 5.
    '''

    number = to_int(number)

    if len(cases) == 3:
        ind_cases = [2, 0, 1, 1, 1, 2]

        if (number % 100 > 4) and (number % 100 < 20):
            return cases[2]
        else:
            i = number % 10
            if i < 5:
                return cases[ind_cases[i]]
            else:
                return cases[ind_cases[5]]

    return ''


def distribute_columns(lst, size):
    # lst - списко сущностей, которые необходимо разложить на столбцы.
    # У каждой сущности должен быть атрибут column_weight
    # size - количество столбцов

    '''
        Алгоритм раскладывания по столбцам

        1. Сортируем сущности lst по весу (по убыванию)
        2. Присваем первое значение веса первому столбцу
        3. Присваем следующие значения следующим столбцам, пока сумма весов не превысит первый
        4. goto 2
    '''

    col_count = to_int(size, 0)

    if lst and len(lst) > 1 and col_count > 1:
        if hasattr(lst[0], 'column_weight'):
            objects = sorted(lst, key=lambda k: k.column_weight, reverse=True)
        else:
            objects = list(lst)

        columns = list([[] for x in range(col_count)])
        weights = list([0 for x in range(col_count)])
        col = 0

        for o in objects:
            is_last = o == objects[-1]

            weight = o.column_weight if hasattr(o, 'column_weight') else 1
            is_appointed = False

            while not is_appointed:
                can_increase = False
                can_append = False

                if col == 0:
                    can_append = True
                    can_increase = True
                else:
                    if is_last and col < col_count - 1 and weights[col + 1] == 0 and weights[col] > 0:
                        can_increase = True
                    elif weights[col] + weight > weights[0]:
                        can_increase = True
                    else:
                        can_append = True

                if can_append:
                    weights[col] += weight
                    columns[col].append(o)
                    is_appointed = True

                if can_increase:
                    col += 1
                    if col >= col_count:
                        col = 0

        return columns

    return None


def paginate(items, current_page=1, item_per_page=None):
    if isinstance(items, QuerySet):
        item_count = items.count()
    else:
        item_count = len(items)

    item_per_page = to_int(item_per_page, 20)

    page_count = item_count / item_per_page

    if page_count != math.floor(page_count):
        page_count = int(math.ceil(page_count))
    else:
        page_count = int(math.floor(page_count))

    page = to_int(current_page, 1)

    if page > page_count:
        page = page_count

    if page < 1:
        page = 1

    if item_count > item_per_page:
        item_slice = ((page - 1) * item_per_page, page * item_per_page)
        items = items[item_slice[0]:item_slice[1]]

    return {
        'items': items,
        'item_count': item_count,
        'page_count': page_count,
        'page': page,
    }


def get_metatags_params(whois, obj=None, title=None, keywords=None, description=None):
    meta_tags = [
        {
            'value': title,
            'attr': 'metatag_title',
        },
        {
            'value': keywords,
            'attr': 'metatag_keywords',
        },
        {
            'value': description,
            'attr': 'metatag_description',
        }
    ]

    if obj:
        for meta_tag in meta_tags:
            if meta_tag['value'] is None:
                value = ''

                if hasattr(obj, meta_tag['attr']):
                    value = getattr(obj, meta_tag['attr'])

                if not value and hasattr(obj, 'name'):
                    value = getattr(obj, 'name')

                meta_tag['value'] = value

    return {
        'whois': whois,
        'title': meta_tags[0]['value'] or '',
        'keywords': meta_tags[1]['value'] or '',
        'description': meta_tags[2]['value'] or '',
    }


# Транслитерация строки
def transliteration(line):
    return slugify(unidecode(line))


def obj_by_pk(model, pk):
    obj_pk, obj = None, None

    if pk:
        try:
            obj_pk = int(pk)
        except (ValueError, TypeError, KeyError):
            obj_pk = None

    if obj_pk:
        try:
            obj = model.objects.get(pk=obj_pk)
        except ObjectDoesNotExist:
            obj = None

    return obj


def to_int(val, default=None):
    result = default

    if val:
        try:
            result = int(val)
        except (ValueError, TypeError, KeyError):
            result = default

    return result


def to_float(val, default=None):
    result = default

    if val:
        try:
            result = float(val)
        except (ValueError, TypeError, KeyError):
            result = default

    return result


def to_string(val, default=None):
    result = default

    if val:
        try:
            result = str(val)
        except (ValueError, TypeError, KeyError):
            result = default

    return result


# Генерирует псевдоуникальный путь и имя для загружаемого файла
def generate_upload_name(instance, filename, prefix=None):
    ext = path.splitext(filename)[1]
    name = str(instance.pk or '') + filename + str(time())
    filename = md5(name.encode('utf8')).hexdigest() + ext
    basedir = path.join(instance._meta.app_label, instance._meta.model_name)

    if prefix:
        basedir = path.join(basedir, prefix)

    return path.join(basedir, filename[:2], filename[2:4], filename[4:8], filename[8:16], filename[16:32], filename)


def get_category_parent_by_level(category, level):
    level = to_int(level, 0)

    if category:
        if category.level == level:
            return category
        elif category.level < level:
            return None
        elif category.level - 1 == level:
            return category.parent
        else:
            ancestors = category.get_ancestors().filter(level=level)
            return ancestors[0] if ancestors else None

    return None


def build_order_number():
    chars =  [c.upper() for c in 'абвгдежзиклмнопрстуфхчшэюя']
    digits = [str(d) for d in range(10)]

    num = ''
    for i in range(5):
        num += random.choice(digits)

    return '%s—%s—%s' % (random.choice(chars), num, random.choice(chars))


def filter_phrase(query, phrase):
    return query.filter_or(text__startswith=phrase) \
                .filter_or(text__contains="*" + phrase + "*") \
                .filter_or(text__exact="*" + phrase + "*") \
                .filter_or(text=phrase)


def make_queryset(query_string, queryset_class=SearchQuerySet, empty_queryset_class=EmptySearchQuerySet):
    query_string = query_string.strip()

    if query_string:
        # Разбиваем на группы для поиска
        query_parts = []
        query_quotes = quotes_re.split(query_string)
        query_quotes = filter(None, [qp.strip() for qp in query_quotes])

        for qp in query_quotes:
            if qp[0] == '"' or qp[0] == "\"":
                query_parts.append(qp[1:-1])
            else:
                query_spaces = filter(None, [
                    qp.strip() for qp in qp.split(' ')
                ])
                for qs in query_spaces:
                    query_parts.append(qs)

        # Включаем эти группы в поиск

        queryset = queryset_class()

        for qp in query_parts:
            queryset_part = filter_phrase(queryset_class(), qp)
            if len(qp) > 2:
                queryset &= queryset_part

        return queryset
    else:
        queryset = empty_queryset_class()

    return queryset


def get_language(word):
    word = word.lower()
    is_russian, is_latin = True, True
    result = None

    lat = [l for l in u'abcdefghijklmnopqrstuvwxyz']
    rus = [r for r in u'абвгдеёжзийклмнопрстуфхцчшщъыьэюя']

    for w in word:
        if w not in lat:
            is_latin = False

        if w not in rus:
            is_russian = False

    if is_russian:
        result = 'rus'
    elif is_latin:
        result = 'eng'

    return result


def stem(word, stemmers):
    '''
    stemmers = {
            'rus': Stemmer('russian'),
            'eng': Stemmer('english'),
        }
    '''

    lang = get_language(word)

    if lang == 'rus':
        return stemmers['rus'].stemWord(word)
    elif lang == 'eng':
        return stemmers['eng'].stemWord(word)
    else:
        return word


def get_query(query):
    stemmed_words = []
    stemmers = {
        'rus': Stemmer('russian'),
        'eng': Stemmer('english'),
    }

    for qw in query.lower().replace('*', '').split():
        stemmed_words.append(stem(qw, stemmers))

    return stemmed_words
