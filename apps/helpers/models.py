from django.db import models
from apps.helpers.utils import transliteration
from random import randint


class UrlMixin(models.Model):
    name = models.CharField(max_length=255, db_index=True, verbose_name='Название')
    slug = models.SlugField(max_length=255, unique=True, verbose_name='ЧПУ')

    class Meta:
        abstract = True

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            original_slug = transliteration(self.name)
            slug, is_unique = original_slug, False

            while not is_unique:
                is_unique = self.__class__.objects.filter(slug=slug).count() == 0

                if is_unique:
                    self.slug = slug
                else:
                    slug = '%s-%s' % (original_slug, randint(1000, 9999))

        super(UrlMixin, self).save(*args, **kwargs)


class ActiveMixin(models.Model):
    is_active = models.BooleanField(default=True, db_index=True, verbose_name='Активность')

    class Meta:
        abstract = True


class MetatagsMixin(models.Model):
    metatag_title = models.CharField(max_length=255, default='', blank=True, verbose_name='Title (метатег)')
    metatag_keywords = models.CharField(max_length=255, default='', blank=True, verbose_name='Keywords (метатег)')
    metatag_description = models.CharField(max_length=255, default='', blank=True, verbose_name='Description (метатег)')

    class Meta:
        abstract = True


class LogMixin(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Создан')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Изменен')

    class Meta:
        abstract = True


class SearchMixin(models.Model):
    search_tags = models.CharField(
        max_length=255,
        blank=True,
        default='',
        verbose_name='Теги для поиска',
        help_text='Эти слова будут добавлены в поисковый индекс'
    )

    class Meta:
        abstract = True


class OrderMixin(models.Model):
    order_no = models.IntegerField(
        blank=True,
        default=500,
        db_index=True,
        verbose_name='Порядок сортировки',
        help_text='Целое число. Сортировка элементов идет от меньшего значения к большему'
    )

    class Meta:
        abstract = True
        ordering = ['-order_no', ]


class ChangedFieldsMixin():
    @property
    def changed_fields(self):
        if hasattr(self, '_changed_fields'):
            return getattr(self, '_changed_fields')
        else:
            raise NotImplementedError("Не был вызван get_changed_fields() метод.")

    def get_changed_fields(self, another_instance):
        self._changed_fields = []
        if another_instance:
            for field_name in self._meta.get_all_field_names():
                if hasattr(self, field_name) and getattr(self, field_name) != getattr(another_instance, field_name):
                    self._changed_fields.append(field_name)

        return True
