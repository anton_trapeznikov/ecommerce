# -*- coding: utf-8 -*-

from django import template
from apps.helpers.thumbnail import *
from os import path

register = template.Library()


@register.simple_tag
def thumbnail(image, size, mode='', blank_photo=''):
    return get_thumbnail(image, size, mode, blank_photo)


@register.simple_tag
def watermarked(image, blank_photo=''):
    if blank_photo:
        blank_photo = path.normpath(blank_photo)

    if image:
        if hasattr(image, 'name'):
            image = image.name

        tp = ThumbnailProcessor(image)
        return tp.watermark(blank=blank_photo or None)
    else:
        return blank_photo
