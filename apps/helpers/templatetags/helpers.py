from django import template
from apps.helpers.utils import *
from apps.denormalization.models import *

from decimal import *
import locale
import math


locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')
register = template.Library()


@register.filter(name='declination_of_number')
def declination_of_number(number=0, cases=''):
    cases = cases.split('__') if cases else []
    number = to_int(number)

    if len(cases) == 3 and number:
        return declination(number=number, cases=cases)

    return ''


# Возвращает остаток целочисленного деления dividend на devider
@register.filter(name='modulo')
def modulo(dividend, devider):
    try:
        dividend = int(dividend)
        devider = int(devider)
    except (ValueError, TypeError):
        dividend = None
        devider = None
        raise template.TemplateSyntaxError('cast error')

    if devider and dividend:
        if devider != 0:
            return dividend % devider
        else:
            raise template.TemplateSyntaxError('division by zero')


# Возвращает результат целочисленного деления dividend на devider
@register.filter(name='div')
def div(dividend, devider):
    try:
        dividend = int(dividend)
        devider = int(devider)
    except (ValueError, TypeError):
        dividend = None
        devider = None
        raise template.TemplateSyntaxError('cast error')

    if devider and dividend:
        if devider != 0:
            return dividend // devider + (dividend % devider > 0)
        else:
            raise template.TemplateSyntaxError('division by zero')


@register.filter(name='append')
def append(src, item):
    result = src

    if isinstance(result, list) and item:
        return result.append(item)
    elif isinstance(result, tuple) and item:
        res_list = [list(i) for i in result]
        res_list.append(item)
        return tuple(tuple(i) for i in res_list)
    elif isinstance(result, (str, buffer, unicode)) and item:
        return '%s%s' % (result, item)
    elif isinstance(result, (int)) and item:
        try:
            val = int(item)
        except ValueError:
            val = 0
        return result + val
    elif isinstance(result, float) and item:
        try:
            val = float(item)
        except ValueError:
            val = 0
        return result + val
    elif isinstance(result, Decimal) and item:
        try:
            val = Decimal(item)
        except ValueError:
            val = 0
        return result + val

    return result


@register.filter(name='subtraction')
def subtraction(src, item):
    result = src

    if isinstance(result, list) and item:
        while item in result:
            result.remove(item)
        return result
    elif isinstance(result, tuple) and item:
        result = list(result)
        while item in result:
            result.remove(item)
        return tuple(result)
    elif isinstance(result, (int)) and item:
        try:
            val = int(item)
        except ValueError:
            val = 0
        return result - val
    elif isinstance(result, float) and item:
        try:
            val = float(item)
        except ValueError:
            val = 0
        return result - val
    elif isinstance(result, Decimal) and item:
        try:
            val = Decimal(item)
        except ValueError:
            val = 0
        return result - val

    return result


@register.filter(name='multiplication')
def multiplication(val, factor):
    result = float(val) * float(factor)

    if result == int(round(result)):
        return int(result)
    else:
        return result


@register.filter(name='round_floor')
def round_floor(value):
    return int(value)


@register.filter(name='round_up')
def round_up(value):
    if value != int(value) and value != 0:
        return int(round(float(value) + 0.5))
    else:
        return int(value)


@register.filter(name='number_with_point')
def number_with_point(value):
    str_value = str(value)
    return str_value.replace(',', '.')


@register.filter
def get_int(num):
    return to_int(num) or ''


@register.filter
def space_digits(num):
    str_num = str(num).replace(',', '.')

    int_num = int(math.trunc(float(str_num)))
    residue = float(str_num) - int_num

    str_num = list(str(int_num))
    str_num.reverse()
    length = len(str_num)
    result = []

    while length > 0:
        if len(str_num) > 3:
            result += str_num[:3]
            del str_num[:3]
            result.append('&nbsp;')
        else:
            result += str_num
            str_num = []

        length = len(str_num)

    if result:
        result.reverse()
    else:
        result = [0, ]

    result = ''.join(result)

    if residue > 0 and int_num < 10000:
        residue = int(round(residue * 100))
        if residue > 0:
            result = '%s.%s' % (result, residue)

    return result


@register.filter
def get_phone(phone):
    phone = phone or ''
    phone = str(phone).split(',')[0]
    digits = [str(d) for d in range(10)]

    clean_phone = ''
    for p in phone:
        if p in digits:
            clean_phone += p

    if clean_phone:
        if clean_phone[0] != '7':
            if clean_phone[0] == '8':
                if clean_phone[:3] == '800':
                    clean_phone = '7%s' % clean_phone
                else:
                    clean_phone = '7%s' % clean_phone[1:]
            else:
                clean_phone = '7%s' % clean_phone

        clean_phone = '+%s' % clean_phone

    return clean_phone
