# -*- coding: utf-8 -*-

from django.conf import settings
from PIL import Image, ImageOps, ImageEnhance
from hashlib import md5
from os import path


def get_thumbnail(image, size, mode='', blank_photo=''):
    if blank_photo:
        blank_photo = path.normpath(blank_photo)

    if image:
        if hasattr(image, 'name'):
            image = image.name

        tp = ThumbnailProcessor(image)

        return tp.thumbnail(mode=mode, size=size, blank=blank_photo or None)
    else:
        return '%s/%s' % (settings.STATIC_URL, blank_photo)


class ThumbnailProcessor(object):
    def __init__(self, image_path):
        self._file_path = None
        self._image = None
        self._errors = []

        if settings.MEDIA_ROOT not in image_path:
            image_path = path.join(settings.MEDIA_ROOT, image_path)

        if path.exists(image_path):
            if path.isfile(image_path):
                self._file_path = image_path

    def _save(self, image, to):
        desired_quality = 70

        if image.mode == 'RGBA':
            image.save(
                to,
                quality=desired_quality,
                optimize=True,
            )
        else:
            image.save(
                to,
                "JPEG",
                quality=desired_quality,
                optimize=True,
                progressive=True,
            )

    def __init_size(self):
        self._image_width = int(self._image.size[0])
        self._image_height = int(self._image.size[1])

    def __init_image(self):
        self._image = Image.open(self._file_path)

        self.__init_size()

        if self._image.mode == 'P':
            self._image = self._image.convert(mode='RGBA', colors=65535)

        return True

    # Метод возвращает сгенерированное имя файла превью\водяного знака
    def _build_file_name(self, is_thumbnail=False, thumbnail_postfix=''):
        if self._file_path:
            file_dir = path.dirname(self._file_path)
            filename_parts = path.basename(self._file_path).split('.')

            if is_thumbnail:
                file_name = '%s__%s.%s' % (
                    md5(filename_parts[0].encode('utf8')).hexdigest(),
                    thumbnail_postfix,
                    filename_parts[-1]
                )
            else:
                file_name = 'wm_%s.%s' % (md5(filename_parts[0].encode('utf8')).hexdigest(), 'jpg')

            return path.join(file_dir, file_name)

        return None

    # Метод формирует из абсолютного пути относительный, начинающийся с MEDIA_URL
    def _get_normalize_path(self, entry_path):
        if entry_path:
            start_prefix = entry_path.find(settings.MEDIA_URL)

            if start_prefix != -1:
                return entry_path[start_prefix:]

        return entry_path

    # Определение размеров превью
    def _parse_size(self, size):
        sizes = size.split('*')

        if (len(sizes) > 2) or (len(sizes) == 0):
            return False
        elif len(sizes) == 1:
            width, height = sizes[0], sizes[0]
        else:
            width, height = sizes[0], sizes[1]

        # Определение неизвестного размера при формате xN или Nx
        if not width or not height:
            if (not width) and height:
                width = float(self._image_width * int(height)) / self._image_height
            elif width and (not height):
                height = float(int(width) * self._image_height) / self._image_width
            else:
                width, height = self._image_width, self._image_height

        self._thumb_width = int(width)
        self._thumb_height = int(height)

        return True

    def _parse_mode(self, mode):
        if mode not in ('fit', 'crop'):
            self._mode = 'fit'
        else:
            self._mode = mode

        return True

    def thumbnail(self, mode='', size='', blank=None):
        if self._file_path:

            postfix = '%s_%s' % (mode, size.replace('*', 'x'))
            thumbnail_path = self._build_file_name(is_thumbnail=True, thumbnail_postfix=postfix)

            # Если файл существует, то возвращаем ссылку на него, в противном случае формируем его и возвращаем ссылку
            if path.exists(thumbnail_path):
                if path.isfile(thumbnail_path):
                    return self._get_normalize_path(thumbnail_path)

            if self.__init_image() and self._parse_size(size) and self._parse_mode(mode.lower()):
                if self._mode == 'crop':
                    self._image = ImageOps.fit(self._image, (self._thumb_width, self._thumb_height), Image.ANTIALIAS)
                else:
                    ratio = float(1)

                    iw, tw = self._image_width, self._thumb_width
                    ih, th = self._image_height, self._thumb_height

                    while (iw / ratio > tw) or (ih / ratio > th):
                        ratio += 0.1

                    if ratio > 1:
                        new_width = int(round((self._image_width / ratio)))
                        new_height = int(round((self._image_height / ratio)))
                        self._image = self._image.resize((new_width, new_height), Image.ANTIALIAS)

                try:
                    self._image.save(thumbnail_path)
                except (Exception, KeyError):
                    return blank or None

                return self._get_normalize_path(thumbnail_path)

        return blank or None

    def _reduce_opacity(self, img, opacity):
        try:
            opacity = float(opacity)
        except ValueError:
            opacity = 1.0

        if opacity < 0:
            opacity = 1.0

        try:
            if img.mode != 'RGBA':
                img = img.convert('RGBA')
            else:
                img = img.copy()

            alpha = img.split()[3]
            alpha = ImageEnhance.Brightness(alpha).enhance(opacity)
            img.putalpha(alpha)

            return img
        except Exception:
            return -1

    # Метод создает файл с наложенный водянным знаком
    def _create_watermark(self, blank=None):
        if self._file_path:
            position = settings.WATERMARK_POSITION
            opacity = settings.WATERMARK_OPACITY
            watermark_file = path.join(settings.STATIC_ROOT, settings.WATERMARK_PATH)
            watermark_file = path.normpath(watermark_file)

            if watermark_file and path.exists(watermark_file) and self.__init_image():
                if path.isfile(watermark_file):

                    watermark_blank, watermarked_image, rgb_image = None, None, None

                    try:
                        opacity = float(opacity)
                    except ValueError:
                        opacity = 1.0

                    if opacity < 0:
                        opacity = 1.0

                    try:
                        watermark_blank = Image.open(watermark_file)

                        if opacity < 1:
                            watermark_blank = self._reduce_opacity(watermark_blank, opacity)

                        if self._image.mode != 'RGBA':
                            self._image = self._image.convert('RGBA')

                        layer = Image.new('RGBA', self._image.size, (0, 0, 0, 0))

                        if position == 'tile':
                            for y in range(0, self._image_height, watermark_blank.size[1]):
                                for x in range(0, self._image_width, watermark_blank.size[0]):
                                    layer.paste(watermark_blank, (x, y))
                        elif position == 'scale':
                            ratio = min(
                                float(self._image_width) / watermark_blank.size[0],
                                float(self._image_height) / watermark_blank.size[1]
                            )
                            width = int(watermark_blank.size[0] * ratio)
                            height = int(watermark_blank.size[1] * ratio)
                            watermark_blank = watermark_blank.resize((width, height))

                            layer.paste(
                                watermark_blank, ((self._image_width - width) / 2, (self._image_height - height) / 2)
                            )
                        else:
                            layer.paste(watermark_blank, position)

                        watermarked_image_path = self._build_file_name(is_thumbnail=False)
                        watermarked_image = Image.composite(layer, self._image, layer)

                        # Принудительно в jpeg
                        rgb_image = Image.new("RGB", watermarked_image.size, (255, 255, 255))
                        rgb_image.paste(watermarked_image, watermarked_image)
                        rgb_image.save(watermarked_image_path)

                        # watermarked_image.save(watermarked_image_path)

                        return watermarked_image_path
                    except Exception:
                        return False
                    finally:
                        if watermark_blank:
                            del watermark_blank

                        if watermarked_image:
                            del watermarked_image

                        if rgb_image:
                            del rgb_image

        return blank or None

    # Метод возвращает ссылку (относительно MEDIA_URL) на изображение с наложенным водяным знаком
    def watermark(self, blank=None):
        watermark_path = self._build_file_name(is_thumbnail=False)

        # Если файл существует, то возвращаем ссылку на него, в противном случае формируем его и возвращаем ссылку
        if watermark_path:
            if path.exists(watermark_path):
                if path.isfile(watermark_path):
                    return self._get_normalize_path(watermark_path)

            return self._get_normalize_path(self._create_watermark(blank=blank))

        return None
