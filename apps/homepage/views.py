from django.template.response import TemplateResponse
from apps.helpers.utils import get_metatags_params


def homepage(request):
    request.meta_tags = get_metatags_params(whois='homepage')
    return TemplateResponse(request, 'homepage/homepage.html', {
        'with_json_ld_contacts': True,
    })
