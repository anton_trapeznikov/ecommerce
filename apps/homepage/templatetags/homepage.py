import locale
import random
from django import template
from django.db.models import Q
from django.utils import timezone
from apps.homepage.models import (
    BannerBlock, ProductBlock, CategoryBlock, BrandBlock
)
from ecommerce.models import *
from apps.denormalization.models import (
    PreparedProduct, PreparedCategory, PreparedBrand
)
from apps.helpers.utils import *


locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')
register = template.Library()


@register.inclusion_tag(
    'homepage/homepage__content__inner.html',
    takes_context=True
)
def build_homepage(context):
    current_date = timezone.now().date()
    banners = list(BannerBlock.objects.filter(
        Q(begin_date__isnull=True) | Q(begin_date__lte=current_date)
    ).filter(
        Q(end_date__isnull=True) | Q(end_date__gte=current_date)
    ))

    request = context['request']

    entries = []

    product_blocks = ProductBlock.objects.filter(is_active=True)
    for p in product_blocks:
        p.cards = PreparedProduct.objects.filter(entry__in=p.products.all())
        p.whois = 'products'

    entries += list(product_blocks)

    category_blocks = CategoryBlock.objects.filter(is_active=True)
    for c in category_blocks:
        c.cards = distribute_columns(
            PreparedCategory.objects.filter(entry__in=c.categories.all()),
            5
        )
        c.whois = 'categories'

    entries += list(category_blocks)

    brand_blocks = BrandBlock.objects.filter(is_active=True)
    for b in brand_blocks:
        b.cards = PreparedBrand.objects.filter(entry__in=b.brands.all())
        b.whois = 'brands'

    entries += list(brand_blocks)

    entries = sorted(entries, key=lambda k: k.order_no)

    text = ''
    for tb in TextBlocks.objects.filter(block='homepage'):
        text = tb.text

    return {
        'request': request,
        'entries': entries,
        'text': text,
        'banner': random.choice(banners) if banners else None,
    }
