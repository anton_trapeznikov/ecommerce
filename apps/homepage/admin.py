from django.contrib import admin
from apps.homepage.models import *
from apps.helpers.thumbnail import get_thumbnail


@admin.register(BannerBlock)
class BannerBlockAdmin(admin.ModelAdmin):
    list_display = ('get_thumbnail', 'begin_date', 'end_date', 'title')

    def get_thumbnail(self, obj):
        image = obj.image
        thumbnail = get_thumbnail(image or '', mode='fit', size='128')

        if thumbnail and len(thumbnail) > 20:
            return '<a href="/admin/homepage/bannerblock/%s/"><img src="%s"></a>' % (obj.pk, thumbnail)
        else:
            return '&mdash;'

    get_thumbnail.short_description = 'Баннер'
    get_thumbnail.allow_tags = True


@admin.register(ProductBlock)
class ProductBlockAdmin(admin.ModelAdmin):
    list_filter = ('is_active', 'head')
    list_display = ('head', 'is_active', 'order_no')
    raw_id_fields = ('products',)


@admin.register(CategoryBlock)
class CategoryBlockAdmin(admin.ModelAdmin):
    list_filter = ('is_active',)
    list_display = ('order_no', 'is_active', 'caption')
    filter_horizontal = ('categories',)


@admin.register(BrandBlock)
class BrandBlockAdmin(admin.ModelAdmin):
    list_filter = ('is_active',)
    list_display = ('order_no', 'is_active', 'caption')
    filter_horizontal = ('brands',)
