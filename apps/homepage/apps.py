from django.apps import AppConfig


class HomepageConfig(AppConfig):
    name = 'apps.homepage'
    verbose_name = "Главная страница"
