from django.db import models
from apps.helpers.models import ActiveMixin, OrderMixin
from apps.catalog.models import Product, Category, Brand
from apps.helpers.utils import generate_upload_name


class BannerBlock(models.Model):
    image = models.ImageField(
        upload_to=generate_upload_name,
        verbose_name='Картинка',
    )
    begin_date = models.DateField(
        null=True,
        blank=True,
        verbose_name='Дата начала публикации',
        help_text='Дата, начиная с которой (включительно), данный '
                  'баннер публикуется на главной странице. Не '
                  'заполенная дата трактуется как дата от начала времен. '
                  'Если на публикацию в заданный интервал попадает несколько '
                  'баннеров, то в каждый период денормализации (~15 мин) '
                  'публикуется случайно выбранный из подходящих баннеров.'
    )
    end_date = models.DateField(
        null=True,
        blank=True,
        verbose_name='Дата окончания публикации',
        help_text='Дата завершения (включительно) публикации данного '
                  'баннера на главной странице. Не заполенная дата '
                  'трактуется как дата конца света. '
                  'Если на публикацию в заданный интервал попадает несколько '
                  'баннеров, то в каждый период денормализации (~15 мин) '
                  'публикуется случайно выбранный из подходящих баннеров.',
    )
    url = models.CharField(
        max_length=512,
        blank=True,
        null=True,
        verbose_name='Адрес ссылки',
        help_text='Если заполнено, то баннер будет являться ссылкой '
                  'на введенный адрес'
    )
    title = models.CharField(
        max_length=512,
        blank=True,
        null=True,
        verbose_name='Всплывающая подсказка',
        help_text='Если заполнено, то при наведении мыши '
                  'будет появлятся введенный здесь текст.'
    )

    class Meta:
        verbose_name = 'Баннер'
        verbose_name_plural = 'Баннеры'

    def __str__(self):
        return 'Баннер #%s' % self.pk


class ProductBlock(ActiveMixin, OrderMixin):
    CAPTION = (
        ('bestseller', 'Хит продаж'),
        ('sale', 'Распродажа'),
        ('popular', 'Популярные товары'),
        ('new', 'Новинки'),
        ('actions', 'Акции'),
        ('last', 'Последние поступления'),
        ('exclusive', 'Эксклюзив'),
        ('recommendation', 'Рекомендуемые товары'),
        ('unique', 'Уникальные товары'),
        ('month', 'Товары месяца'),
    )

    products = models.ManyToManyField(
        Product,
        blank=True,
        verbose_name='Товары'
    )
    head = models.CharField(
        max_length=48,
        choices=CAPTION,
        verbose_name='Тип блока'
    )
    caption = models.CharField(
        max_length=64,
        blank=True,
        default='',
        verbose_name='Заголовок'
    )
    caption_url = models.CharField(
        max_length=64,
        blank=True,
        default='',
        verbose_name='Ссылка заголовка'
    )

    class Meta:
        verbose_name = 'Товарный блок'
        verbose_name_plural = 'Товарные блоки'

    def __str__(self):
        return 'Товарный блок #%s' % self.pk

    @property
    def head_name(self):
        name = ''
        for c in self.CAPTION:
            if c[0] == self.head:
                name = c[1]

        return name


class CategoryBlock(ActiveMixin, OrderMixin):
    categories = models.ManyToManyField(Category, verbose_name='Категории')
    caption = models.CharField(
        max_length=256,
        blank=True,
        default='',
        verbose_name='Заголовок'
    )

    class Meta:
        verbose_name = 'Список категорий'
        verbose_name_plural = 'Списки категорий'

    def __str__(self):
        return 'Список категорий #%s' % self.pk


class BrandBlock(ActiveMixin, OrderMixin):
    brands = models.ManyToManyField(Brand, verbose_name='Бренды')
    icon = models.BooleanField(
        default=False,
        blank=True,
        verbose_name='Крупные иконки',
        help_text='Если установлен, то список брендов отображается'
        'в виде списка логотипов',
    )
    caption = models.CharField(
        max_length=256,
        blank=True,
        default='',
        verbose_name='Заголовок'
    )

    class Meta:
        verbose_name = 'Список брендов'
        verbose_name_plural = 'Списки брендов'

    def __str__(self):
        return 'Список брендов #%s' % self.pk
