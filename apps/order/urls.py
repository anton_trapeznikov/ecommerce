from django.conf.urls import url
import apps.order.views

urlpatterns = [
    url(r'^$', apps.order.views.index, name='index'),
    url(r'^change/$', apps.order.views.change_cart, name='change_cart'),
    url(r'^save/$', apps.order.views.save_order, name='save_order'),
]