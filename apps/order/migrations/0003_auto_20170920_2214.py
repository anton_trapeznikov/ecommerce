# -*- coding: utf-8 -*-
# Generated by Django 1.9.12 on 2017-09-20 17:14
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0002_auto_20170920_2203'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cart',
            name='is_confirmed',
        ),
        migrations.RemoveField(
            model_name='order',
            name='cart',
        ),
    ]
