from django.conf import settings
from apps.order.models import Cart
from django.core.exceptions import ObjectDoesNotExist
import json


def cart_processor(request):
    uid = request.COOKIES.get(settings.CART_COOKIES_NAME, None)
    cart = None

    if uid:
        cart = Cart.get_by_uid(uid=uid)

    cart_meta = {
        'length': cart.length if cart else 0,
        'cost': cart.price if cart else 0,
    }

    return {
        'remove_cart_cookie': uid and cart is None,
        'cart_meta': json.dumps(cart_meta),
        'cart_cookie_name': settings.CART_COOKIES_NAME,
    }