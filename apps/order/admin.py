from django.contrib import admin
from django import forms
from apps.order.models import *


class OrderItemsInline(admin.TabularInline):
    model = OrderItem
    extra = 1
    fk_name = 'order'
    readonly_fields = ('product', 'price', 'quantity')


class OrderAdmin(admin.ModelAdmin):
    list_display = ('phone', 'created_at', 'price', 'length', 'name')
    inlines = [OrderItemsInline,]
    readonly_fields = ('uid', 'price', 'length',)

admin.site.register(Order, OrderAdmin)
