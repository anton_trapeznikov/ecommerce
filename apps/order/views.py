from django.conf import settings
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.template.response import TemplateResponse
from django.core.exceptions import ObjectDoesNotExist
from apps.helpers.utils import to_int, obj_by_pk, build_order_number
from apps.denormalization.models import PreparedProduct
from apps.catalog.models import Product
from apps.forms.forms import OrderForm
from apps.order.models import *
import json


def save_order(request):
    is_quick_order = request.POST.get('quick-order') == 'yes'

    products = []

    if is_quick_order:
        slug = None
        parts = request.META.get('HTTP_REFERER', '').split('/')
        if 'catalog' in parts and 'product' in parts:
            c_ind = parts.index('catalog')
            p_ind = parts.index('product')
            if p_ind == c_ind + 1:
                if len(parts) > p_ind + 1:
                    slug = parts[p_ind + 1]

        if slug:
            product = Product.objects.filter(slug=slug).first()
            if product and product.price:
                products.append(
                    {
                        'product': product,
                        'quantity': 1,
                        'price': product.price,
                        'total_price': product.price,
                    }
                )

    else:
        uid = request.COOKIES.get(settings.CART_COOKIES_NAME, None)
        cart = Cart.get_by_uid(uid=uid, with_create=True) if uid else None
        cart_items = cart.get_items() if cart else []

        for ci in cart_items:
            if ci.product.price:
                products.append(
                    {
                        'product': ci.product,
                        'quantity': ci.quantity,
                        'price': ci.product.price,
                        'total_price': ci.product.price * ci.quantity,
                    }
                )

    if len(products) == 0:
        raise Http404

    form = OrderForm(request.POST)
    if not form.is_valid():
        raise Http404

    # order_number = build_order_number()

    order = form.save(commit=False)
    order.length = len(products)
    order.price = sum([p['total_price'] for p in products])
    order.save()
    order_items = []

    for p in products:
        order_item = OrderItem(order=order)
        order_item.product = p['product']
        order_item.price = p['price']
        order_item.quantity = p['quantity']

        order_items.append(order_item)

    OrderItem.objects.bulk_create(order_items, 25)

    order.send_notify()

    if not is_quick_order:
        cart.delete()

    return HttpResponseRedirect(order.get_thank_you_page_url())


def index(request):
    uid = request.COOKIES.get(settings.CART_COOKIES_NAME, None)
    cart = Cart.get_by_uid(uid=uid, with_create=True) if uid else None
    cart_items = cart.get_items() if cart else []
    form = OrderForm() if cart else None

    return TemplateResponse(request, 'order/cart.html', {
        'cart': cart,
        'cart_items': cart_items,
        'form': form,
        'total': sum([ci.total for ci in cart_items]),
    })


def change_cart(request):
    uid = request.COOKIES.get(settings.CART_COOKIES_NAME, None)
    cart = Cart.get_by_uid(uid=uid, with_create=True) if uid else None
    action = request.GET.get('action', None)
    delete_empty_cart = False

    if cart:
        product = obj_by_pk(Product, request.GET.get('pid', None))
        count = to_int(request.GET.get('count', 1), 1)

        if product and count and action == 'add':
            cart.set_product(product=product, count=count)
        elif product and count and action == 'set':
            cart.set_product(product=product, count=count, append_mode=False)
        elif product and action == 'remove':
            cart.remove_product(product=product)
            delete_empty_cart = True
        elif action == 'clear':

            cart.clear()
            delete_empty_cart = True

    result = {
        'length': cart.length if cart else 0,
        'cost': cart.price if cart else 0,
    }

    if delete_empty_cart and cart:
        if result['length'] == result['cost'] == 0:
            cart.delete()

    return HttpResponse(json.dumps(result))