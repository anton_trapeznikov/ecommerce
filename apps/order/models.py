from django.db import models
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.conf import settings
from apps.helpers.models import LogMixin
from apps.catalog.models import Product, Brand
from apps.helpers.utils import to_int, obj_by_pk
from apps.denormalization.models import PreparedProduct
from ecommerce.models import NotificationRecipients
import uuid


class Cart(LogMixin):
    uid = models.SlugField(max_length=32, unique=True, verbose_name='URL')
    price = models.FloatField(blank=True, default=0, verbose_name='Цена')
    length = models.IntegerField(blank=True, default=0, verbose_name='Количество товаров')

    class Meta:
        verbose_name = 'Корзина'
        verbose_name_plural = 'Корзина'

    def __str__(self):
        return 'Корзина №%s от %s' % (self.pk, self.created_at)

    def save(self, *args, **kwargs):
        if not self.uid:
            uid = uuid.uuid4().hex

            is_unique = False
            while not is_unique:
                is_unique = Cart.objects.filter(uid=uid).count() == 0

                if is_unique:
                    self.uid = uid
                else:
                    uid = uuid.uuid4().hex

        super(Cart, self).save(*args, **kwargs)

    @staticmethod
    def get_by_uid(uid, with_create=False):
        cart = None

        if uid:
            try:
                cart = Cart.objects.get(uid=uid)
                cart.check_status()
            except ObjectDoesNotExist:
                if with_create:
                    cart = Cart(uid=uid)
                    cart.save()
                else:
                    cart = None

        return cart

    @staticmethod
    def remove_old():
        time_threshold = timezone.now() - timezone.timedelta(days=30)
        Cart.objects.filter(created_at__lt=time_threshold).delete()

        return True

    def _calculate(self, with_save=True):
        if self.pk and self.updated_at:
            length = 0
            cost = 0

            product_index = {v['product_id']: v['quantity'] for v in CartItem.objects.filter(cart=self).values('product_id', 'quantity')}

            products = PreparedProduct.objects.filter(entry_id__in=list(product_index.keys()))

            for p in products:
                cost += p.price * product_index[p.entry_id]
                length += 1

            self.price = cost
            self.length = length

            if with_save:
                self.save()

    def remove_product(self, product):
        cart_product = product if isinstance(product, Product) else None
        CartItem.objects.filter(cart=self, product=cart_product).delete()
        self.check_status(force=True)

    def clear(self):
        CartItem.objects.filter(cart=self).delete()
        self.check_status(force=True)

    def get_items(self):
        cart_items, items = [], CartItem.objects.filter(cart=self).order_by('pk')
        product_index = {p.entry_id: p for p in PreparedProduct.objects.filter(entry_id__in=[i.product_id for i in items])}

        for i in items:
            if i.product_id in product_index:
                i.prepared_product = product_index[i.product_id]
                i.total = i.prepared_product.price * i.quantity
                cart_items.append(i)

        return cart_items

    def set_product(self, product, count, append_mode=True):
        cart_product = product if isinstance(product, Product) else None
        count = to_int(count, 1)
        count = 1 if count < 1 else count

        if cart_product and count:
            try:
                cart_item = CartItem.objects.get(cart=self, product=cart_product)

                if append_mode:
                    cart_item.quantity += count
                else:
                    cart_item.quantity = count

                cart_item.save()
            except ObjectDoesNotExist:
                cart_item = CartItem(cart=self, product=cart_product, quantity=count)
                cart_item.save()

        self.check_status(force=True)

    def check_status(self, force=False):
        if self.pk and self.updated_at:
            if force == True:
                self._calculate()
            elif self.updated_at.date() != timezone.now().date():
                self._calculate()

        return True


class CartItem(models.Model):
    cart = models.ForeignKey(Cart, verbose_name='Корзина')
    product = models.ForeignKey(Product, verbose_name='Товар')
    quantity = models.FloatField(default=1, blank=True, verbose_name='Количество')

    class Meta:
        verbose_name = 'Товар в корзине'
        verbose_name_plural = 'Товары в корзине'
        unique_together = ('cart', 'product')


class Order(LogMixin):
    price = models.FloatField(verbose_name='Итоговая цена')
    length = models.IntegerField(verbose_name='Количество товаров')
    name = models.CharField(max_length=64, blank=True, default='', verbose_name='Имя')
    phone = models.CharField(max_length=64, verbose_name='Телефон')
    email = models.EmailField(max_length=128, blank=True, null=True, verbose_name='Email')
    address = models.TextField(blank=True, default='', verbose_name='Адрес доставки')
    comment = models.TextField(blank=True, default='', verbose_name='Комментарий')
    uid = models.SlugField(max_length=32, unique=True, verbose_name='GUID')

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказ'

    def send_notify(self):
        items = OrderItem.objects.filter(order=self).select_related('product')

        bids = set([i.product.brand_id for i in items])
        brands = {b.pk: b.name for b in Brand.objects.filter(pk__in=bids)}

        for i in items:
            i.total_price = i.quantity * i.price
            i.product.brand_name = brands.get(i.product.brand_id, '')
            i.image = i.product._main_image or ''

        employee_notify = render_to_string('order/employee_notify.html', {
            'order': self,
            'items': items,
        })

        recipients = NotificationRecipients.objects.all()
        emails = list([nr.email.lower() for nr in recipients])

        send_mail(
            subject='Новый заказ',
            message=employee_notify,
            html_message=employee_notify,
            from_email=settings.EMAIL_HOST_USER,
            recipient_list=emails,
            fail_silently=False
        )

        if self.email:
            client_notify = render_to_string('order/client_notify.html', {
                'order': self,
                'items': items,
            })

            send_mail(
                subject='Ваш заказ в belorekb.ru принят.',
                message=client_notify,
                html_message=client_notify,
                from_email=settings.EMAIL_HOST_USER,
                recipient_list=[self.email, ],
                fail_silently=False
            )

    def save(self, *args, **kwargs):
        if not self.uid:
            uid = uuid.uuid4().hex

            is_unique = False
            while not is_unique:
                is_unique = Order.objects.filter(uid=uid).count() == 0

                if is_unique:
                    self.uid = uid
                else:
                    uid = uuid.uuid4().hex

        super(Order, self).save(*args, **kwargs)

    def get_thank_you_page_url(self):
        return '/thank-you/?order=%s' % self.uid


class OrderItem(models.Model):
    order = models.ForeignKey(Order, verbose_name='Заказ')
    product = models.ForeignKey(Product, verbose_name='Товар')
    price = models.FloatField(verbose_name='Цена')
    quantity = models.FloatField(default=1, verbose_name='Количество')

    class Meta:
        verbose_name = 'Заказанный товар'
        verbose_name_plural = 'Заказанные товары'
        unique_together = ('order', 'product')

    def __str__(self):
        return '%s' % self.pk
