# -*- coding: utf-8 -*-

from apps.catalog.models import *
from apps.denormalization.core import Denormalizer
from apps.denormalization.models import PreparedProduct
import json


class ProductDenormalizer(Denormalizer):
    def __init__(self, is_full, log):
        Denormalizer.__init__(
            self,
            denormalizer_model='PreparedProduct',
            catalog_model='Product',
            is_full=is_full,
            log=log
        )

    def _build_sources(self):
        source_id = []

        if self._is_full:
            source_id = [p.pk for p in Product.objects.all().only('pk').order_by('pk')]
        else:
            pass

        source_id = list(source_id)
        chunk_size = 500
        source_id_chunks = [source_id[i:i + chunk_size] for i in range(0, len(source_id), chunk_size)]

        if not self._log.steps:
            self._log.steps = len(source_id_chunks)

        step = self._log.step
        self._log.step += 1
        self._log.save()

        if 0 <= step < self._log.steps:
            self._sources = Product.objects.filter(pk__in=source_id_chunks[step])
        else:
            self._sources = []

    def _denormalization(self, obj):
        validity = obj.price and \
            obj.price > 0 and \
            obj.is_active and \
            self.category_valid(obj.category) and \
            self.brand_valid(obj.brand)

        if validity:
            common_fields = (
                'name', 'slug', 'note', 'analog',
                'search_tags', 'metatag_title', 'metatag_keywords',
                'metatag_description', 'brand', 'category', 'withdrawn',
                'price', 'instock', 'line', 'kind',
            )

            dobj = PreparedProduct(entry=obj)
            ProductDenormalizer.copy_fields(obj, dobj, common_fields)
            dobj.url = obj.get_absolute_url()
            dobj.category_data = json.dumps(self.serialize_category(obj.category, slug=True, url=True))
            dobj.brand_data = json.dumps(self.serialize_brand(obj.brand, logo=True, slug=True, url=True))
            dobj.line_data = json.dumps(self._build_line(obj))
            dobj.kind_data = json.dumps(self._build_kind(obj))
            dobj.metatag_title = dobj.metatag_title or dobj.name
            dobj.metatag_keywords = dobj.metatag_keywords or dobj.name
            dobj.metatag_description = dobj.metatag_description or dobj.name
            dobj.image = obj._main_image.name if obj._main_image else None
            dobj.features = json.dumps(self._build_features(obj))
            dobj.images = json.dumps(self._build_images(obj))
            dobj.breadcrumbs = json.dumps(self._build_breadcrumbs(obj))
            dobj.kind_starter = self._build_kind_starter(obj)
            dobj.country = obj.brand.country
            dobj.country_name = dobj.country.name if dobj.country else ''

            if obj.hight_priority:
                dobj.priority = 2
            elif obj.category.low_priority:
                dobj.priority = 0
            else:
                dobj.priority = 1

            dobj.have_image = dobj.image is not None
            features = self._build_features(obj)

            dobj.features = json.dumps(features['features'])
            dobj.specification = features['filter_tags']

            return dobj

        return None

    def _build_line(self, product):
        result = None

        if product.line:
            line = product.line
            result = {
                'id': line.pk,
                'name': line.name,
                'url': line.get_absolute_url(),
                'products': list([p.pk for p in Product.objects.filter(line=line)]),
                'image': line.image.name if line.image else None,
                'note': line.note
            }

        return result

    def _build_kind(self, product):
        result = None
        if product.kind:
            kind = product.kind

            result = {
                'id': kind.pk,
                'name': kind.name,
                'url': kind.get_absolute_url(),
                'products': list([p.pk for p in Product.objects.filter(kind=kind)]),
            }

        return result

    def _build_kind_starter(self, product):
        kind = Product.objects.filter(kind=product.kind, kind__isnull=False).order_by('name')

        if kind and kind[0] != product:
            return False
        else:
            return True

    def _build_features(self, product):
        features = []
        filter_tags = []

        for spec in Specification.objects.filter(product=product).select_related('feature').order_by('feature__name'):
            features.append({
                'feature_id': spec.feature.pk,
                'spec_id': spec.pk,
                'name': spec.feature.name,
                'value': spec.value,
                'units': spec.feature.units,
                'slugify': spec.slugify,
                'for_itemcard': spec.feature.for_itemcard,
                'for_filter': spec.feature.for_filter,
            })

            if spec.feature.for_filter:
                tag = '%s_%s' % (str(spec.feature.pk), spec.slugify)
                filter_tags.append(tag[:255])

        return {
            'features': features,
            'filter_tags': list(set(filter_tags))
        }

    def _build_images(self, product):
        return list([i.image.name for i in ProductImage.objects.filter(product=product).order_by('-is_main')])

    def _build_breadcrumbs(self, product):
        ancestors = product.category.get_ancestors().order_by('level')
        parents = list([self.serialize_category(c, slug=True, url=True) for c in ancestors])
        parents.append(self.serialize_category(product.category, slug=True, url=True))

        return parents
