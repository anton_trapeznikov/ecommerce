from django.apps import AppConfig


class DenormalizationConfig(AppConfig):
    name = 'apps.denormalization'
    verbose_name = "Денормализация"
