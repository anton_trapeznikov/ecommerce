from django.db import models
from apps.catalog.models import *
from django.contrib.postgres.fields import ArrayField
from apps.helpers.models import UrlMixin, MetatagsMixin, LogMixin, SearchMixin
import json


class Log(LogMixin):
    key = models.CharField(max_length=36, unique=True, verbose_name='Код')
    step = models.IntegerField(default=0, blank=True, verbose_name='Завершенных шагов')
    steps = models.IntegerField(null=True, blank=True, verbose_name='Всего шагов')
    whois = models.CharField(max_length=64, verbose_name='Тип денормализации')
    total = models.IntegerField(null=True, blank=True, verbose_name='Затраченное время, сек')
    entries = models.TextField(null=True, blank=True, verbose_name='ID сущностей текущего шага')

    class Meta:
        verbose_name = 'Журнал денормализации'
        verbose_name_plural = 'Журналы денормализации'

    def __str__(self):
        return '%s (%s)' % (self.key, self.whois)


class Prepared(models.Model):
    class Meta:
        abstract = True

    def _unpack_json_object(self, field):
        unpack_field = '_%s' % field

        if not hasattr(self, unpack_field):
            if hasattr(self, field):
                setattr(self, unpack_field, json.loads(getattr(self, field)))
            else:
                setattr(self, unpack_field, None)

        return getattr(self, unpack_field)

    def get_list(self, field):
        data = self._unpack_json_object(field)

        if isinstance(data, (list, tuple)):
            return data

        return None

    def get_dict(self, field):
        data = self._unpack_json_object(field)

        if isinstance(data, dict):
            return data

        return None

    def get_dict_attr(self, field, attr):
        data = self.get_dict(field)

        if data and attr in data:
            return data[attr]

        return None


class PreparedProduct(Prepared, UrlMixin, MetatagsMixin, SearchMixin):
    entry = models.OneToOneField(Product, verbose_name='Товар')
    url = models.TextField(verbose_name='Url')
    breadcrumbs = models.TextField(blank=True, default='', verbose_name='Хлебные крошки')
    category = models.ForeignKey(Category, verbose_name='Категория')
    category_data = models.TextField(blank=True, default='', verbose_name='Денормализованная категория')
    brand = models.ForeignKey(Brand, verbose_name='Бренд')
    brand_data = models.TextField(blank=True, default='', verbose_name='Денормализованный бренд')
    country = models.ForeignKey(
        Country,
        blank=True,
        null=True,
        verbose_name='Страна',
        help_text='Принадлежность бренда'
    )
    country_name = models.CharField(max_length=256, blank=True, default='', verbose_name='Страна')
    line = models.ForeignKey(ProductLine, blank=True, null=True, verbose_name='Продуктовая линия')
    line_data = models.TextField(blank=True, default='', verbose_name='Денормализованная линия')
    kind = models.ForeignKey(ProductKind, blank=True, null=True, verbose_name='Разновидность товара')
    kind_data = models.TextField(blank=True, default='', verbose_name='Денормализованная разновидность')
    kind_starter = models.BooleanField(
        default=True,
        db_index=True,
        verbose_name='Старторый товар группы разновидностей'
    )
    withdrawn = models.BooleanField(default=False, db_index=True, verbose_name='Снят с продажи')
    analog = models.ForeignKey(Product, blank=True, null=True, verbose_name='Аналог', related_name='item_analog')
    instock = models.BooleanField(default=False, db_index=True, verbose_name='В наличии')
    price = models.FloatField(db_index=True, verbose_name='Цена')
    have_image = models.BooleanField(default=False, db_index=True, verbose_name='Имеет фото')
    image = models.TextField(blank=True, null=True, verbose_name='Картинка')
    images = models.TextField(blank=True, default='', verbose_name='Все изображения')
    features = models.TextField(blank=True, default='', verbose_name='Характеристики')
    note = models.TextField(blank=True, default='', verbose_name='Описание')
    priority = models.IntegerField(default=1, blank=True, db_index=True, verbose_name='Приритет')
    # GIN-индекс для этого поля должен создаться руками через RunSQL
    specification = ArrayField(
        models.CharField(max_length=256),
        blank=True,
        null=True,
        verbose_name='Значения характеристик'
    )

    class Meta:
        verbose_name = 'Подготовленный товар'
        verbose_name_plural = 'Подготовленные товары'

    def __str__(self):
        return self.name

    @property
    def brand_name(self):
        return self.get_dict_attr('brand_data', 'name')

    @property
    def brand_url(self):
        return self.get_dict_attr('brand_data', 'url')

    @property
    def line_name(self):
        return self.get_dict_attr('line_data', 'name')

    @property
    def line_url(self):
        return self.get_dict_attr('line_data', 'url')

    @property
    def line_length(self):
        products = self.get_dict_attr('line_data', 'products')
        return len(products)

    @property
    def line_note(self):
        return self.get_dict_attr('line_data', 'note')

    @property
    def line_image(self):
        return self.get_dict_attr('line_data', 'image')

    @property
    def kind_name(self):
        return self.get_dict_attr('kind_data', 'name')

    @property
    def kind_url(self):
        return self.get_dict_attr('kind_data', 'url')

    @property
    def kind_length(self):
        products = self.get_dict_attr('kind_data', 'products')
        return len(products)

    @property
    def kind_products(self):
        return self.get_dict_attr('kind_data', 'products')

    @property
    def parent_categories(self):
        return self.get_list('breadcrumbs')

    @property
    def feauture_list(self):
        return self.get_list('features')

    @property
    def images_list(self):
        return self.get_list('images')



class PreparedCategory(Prepared, UrlMixin, MetatagsMixin, SearchMixin):
    entry = models.OneToOneField(Category, verbose_name='Категория')
    url = models.CharField(max_length=255, unique=True, verbose_name='Url')
    image = models.CharField(max_length=255, blank=True, default='', verbose_name='Картинка')
    menu_name = models.CharField(max_length=256, blank=True, default='', verbose_name='Имя для меню')
    second_name = models.CharField(max_length=256, blank=True, default='', verbose_name='Второе имя')
    menu_data = models.TextField(blank=True, default='', verbose_name='Местоположение и связи')
    descendants = models.TextField(blank=True, default='', verbose_name='ID потомков')
    brands = models.TextField(blank=True, default='', verbose_name='Бренды')
    countries = models.TextField(blank=True, default='', verbose_name='Страны')
    features = models.TextField(blank=True, default='', verbose_name='Характеристики товаров')
    product_count = models.IntegerField(default=0, verbose_name='Количество товаров')
    min_price = models.FloatField(default=0, verbose_name='Минимальная цена товаров')
    max_price = models.FloatField(default=0, verbose_name='Максимальная цена товаров')
    note = models.TextField(blank=True, default='', verbose_name='Описание')
    level = models.IntegerField(default=0, blank=True, db_index=True, verbose_name='Уровень')
    lines = models.TextField(blank=True, default='', verbose_name='Продуктовые линии')

    class Meta:
        verbose_name = 'Подготовленная категория'
        verbose_name_plural = 'Подготовленные категории'

    def __str__(self):
        return self.name

    @property
    def menu(self):
        return self.get_dict('menu_data')

    @property
    def children(self):
        return self.get_dict_attr('menu_data', 'children')


class PreparedBrand(Prepared, UrlMixin, MetatagsMixin, SearchMixin):
    entry = models.OneToOneField(Brand, verbose_name='Категория')
    url = models.CharField(max_length=255, unique=True, verbose_name='Url')
    logo = models.CharField(max_length=255, blank=True, default='', verbose_name='Логотип')
    country = models.ForeignKey(
        Country,
        blank=True,
        null=True,
        verbose_name='Страна',
        help_text='Принадлежность бренда'
    )
    letter = models.CharField(max_length=2, verbose_name='Буква')
    note = models.TextField(blank=True, default='', verbose_name='Описание')
    seo_text = models.TextField(blank=True, default='', verbose_name='SEO-описание')
    product_count = models.IntegerField(default=0, db_index=True, verbose_name='Количество товаров')
    min_price = models.FloatField(default=0, verbose_name='Минимальная цена товаров')
    max_price = models.FloatField(default=0, verbose_name='Максимальная цена товаров')
    categories = models.TextField(blank=True, default='', verbose_name='Категории')

    @property
    def children(self):
        return self.get_list('categories')


class RingProducts(models.Model):
    of = models.OneToOneField(Product, related_name='relink_of')
    to = models.OneToOneField(Product, related_name='relink_to')


class RingCategories(models.Model):
    of = models.OneToOneField(Category, related_name='relink_of')
    to = models.OneToOneField(Category, related_name='relink_to')


class RingBrands(models.Model):
    of = models.OneToOneField(Brand, related_name='relink_of')
    to = models.OneToOneField(Brand, related_name='relink_to')


class PreparedTemplates(LogMixin):
    path = models.CharField(max_length=128, unique=True)
    is_common = models.BooleanField(db_index=True, default=False, blank=True)
    name = models.CharField(max_length=64)
    content = models.TextField()
