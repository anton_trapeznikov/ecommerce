from django.core.cache import cache
from apps.helpers.models import ChangedFieldsMixin
from apps.catalog.models import *


class QueueManager(object):
    def prepare_instance(self, instance):
        if instance:
            instance.is_new = instance.pk is None
            instance._another_instance = instance.__class__.objects.filter(pk=instance.pk).first()

            if ChangedFieldsMixin in instance.__class__.__bases__:
                instance.get_changed_fields(another_instance=instance._another_instance)

            if isinstance(instance, Product):
                instance._old_category = instance._another_instance.category if instance._another_instance else None
                instance._old_brand = instance._another_instance.brand if instance._another_instance else None

    def manage_queue(self, prepared_instance):
        denorm_listening_models = (
            Country,
            Product,
            Category,
            Brand,
            ProductLine,
            ProductKind,
            ProductImage,
            Feature,
            Specification,
        )

        instance = prepared_instance

        if instance and isinstance(instance, denorm_listening_models):
            changed_products = []
            changed_brands = []
            changed_categories = []

            if isinstance(instance, ProductLine):
                changed_products = Product.objects.filter(line=instance)

            if isinstance(instance, ProductKind):
                changed_products = Product.objects.filter(kind=instance)

            if isinstance(instance, ProductImage):
                changed_products.append(instance.product)

            elif isinstance(instance, (Country, Brand)):
                if isinstance(instance, Country):
                    changed_brands = list(Brand.objects.filter(country=instance))
                else:
                    changed_brands = [instance, ]

                changed_products = Product.objects.filter(brand__in=changed_brands)
                changed_categories = self.get_all_assigned_categories(products=changed_products)

            elif isinstance(instance, Product):
                old_category = instance._another_instance.category if instance._another_instance else None
                old_brand = instance._another_instance.brand if instance._another_instance else None

                changed_products.append(instance)
                changed_brands.append(instance.brand)
                changed_categories = self.get_all_assigned_categories(products=changed_products)

                if old_category and old_category != instance.category:
                    changed_categories += self.get_all_assigned_categories(categories=[old_category, ])

                if old_brand and old_brand != instance.brand:
                    changed_brands.append(old_brand)

            elif isinstance(instance, Category):
                old_parent = instance._another_instance.parent if instance._another_instance else None

                changed_products.append(instance)
                changed_categories = self.get_all_assigned_categories(categories=[instance, ])

                if old_parent and old_parent != instance.parent:
                    changed_categories += self.get_all_assigned_categories(categories=[old_parent, ])

                changed_products = Product.objects.filter(category__in=changed_categories)

            elif isinstance(instance, (Feature, Specification)):
                if isinstance(instance, Feature):
                    specs = Specification.objects.filter(feature=instance).select_related('product')
                else:
                    specs = Specification.objects.filter(pk=instance.pk).select_related('product')

                for s in specs:
                    changed_products.append(s.product)

                changed_categories = self.get_all_assigned_categories(products=changed_products)

            if changed_products:
                self.append_queue(key="denorm_queue_products", entries=changed_products)

            if changed_brands:
                self.append_queue(key="denorm_queue_brands", entries=changed_brands)

            if changed_categories:
                self.append_queue(key="denorm_queue_categories", entries=changed_categories)

        return True

    def append_queue(self, key, entries):
        ids = [e.pk for e in entries]
        exists = cache.get(key)

        queue = []

        if exists:
            queue = exists + list(set(ids) - set(exists))
        else:
            queue = list(ids)

        cache.set(key, queue)

    def append_category_queue(self, entry):
        self.append_queue(model=CategoryQueue, entry=entry)

    def append_brand_queue(self, entry):
        self.append_queue(model=BrandQueue, entry=entry)

    def append_product_queue(self, entry):
        self.append_queue(model=ProductQueue, entry=entry)

    def get_all_assigned_categories(self, products=None, categories=None):
        entries = []

        if categories:
            entries = [c.get_root() for c in categories]
        elif products:
            ids = {p.category_id: None for p in products if p.category_id}
            entries = [c.get_root() for c in Category.objects.filter(pk__in=ids.keys())]

        categories = []

        for root in entries:
            categories.append(root)
            categories += list(root.get_descendants())

        return categories
