from django.template.response import TemplateResponse
from django.http import HttpRequest
from django.contrib.auth.models import AnonymousUser
from apps.denormalization.models import PreparedTemplates
from django.conf import settings


def prepare_templates():
    def generate_content(request, template_path, ctx={}):
        content = TemplateResponse(request, template_path, ctx)
        content = content.render()
        return content.rendered_content

    template_list = settings.CACHED_TEMPLATES
    for tpl in template_list:
        template = tpl['template']
        request = HttpRequest()
        request.user = AnonymousUser()
        request.is_template_prepare = True

        ctx = {}

        dn_template, result = PreparedTemplates.objects.get_or_create(path=template)
        dn_template.content = generate_content(request, template, ctx)
        dn_template.is_common = tpl['is_common']
        dn_template.name = tpl['name']
        dn_template.save()
