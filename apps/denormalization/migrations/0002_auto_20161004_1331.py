# -*- coding: utf-8 -*-
# Generated by Django 1.9.10 on 2016-10-04 08:31
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('denormalization', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='brandqueue',
            name='entry',
        ),
        migrations.RemoveField(
            model_name='categoryqueue',
            name='entry',
        ),
        migrations.RemoveField(
            model_name='kindqueue',
            name='entry',
        ),
        migrations.RemoveField(
            model_name='productqueue',
            name='entry',
        ),
        migrations.DeleteModel(
            name='BrandQueue',
        ),
        migrations.DeleteModel(
            name='CategoryQueue',
        ),
        migrations.DeleteModel(
            name='KindQueue',
        ),
        migrations.DeleteModel(
            name='ProductQueue',
        ),
    ]
