# -*- coding: utf-8 -*-

from django.db.models import Max, Min
from apps.catalog.models import *
from apps.denormalization.core import Denormalizer
from apps.denormalization.models import PreparedBrand
from apps.denormalization.models import PreparedProduct
from apps.denormalization.models import PreparedCategory
from apps.helpers.utils import get_category_parent_by_level
import json


class BrandDenormalizer(Denormalizer):
    def __init__(self, is_full, log):
        Denormalizer.__init__(
            self,
            denormalizer_model='PreparedBrand',
            catalog_model='Brand',
            is_full=is_full,
            log=log
        )

    def _build_sources(self):
        self.valid_brands_id = [
            p.brand.pk for p in PreparedProduct.objects.all().distinct('brand').only('brand').select_related('brand')
        ]

        source_id = []

        if self._is_full:
            source_id = [b.pk for b in Brand.objects.filter(pk__in=self.valid_brands_id)]
        else:
            pass

        source_id = list(source_id)
        chunk_size = 50
        source_id_chunks = [source_id[i:i + chunk_size] for i in range(0, len(source_id), chunk_size)]

        if not self._log.steps:
            self._log.steps = len(source_id_chunks)

        step = self._log.step
        self._log.step += 1
        self._log.save()

        if 0 <= step < self._log.steps:
            self._sources = Brand.objects.filter(pk__in=source_id_chunks[step])
        else:
            self._sources = []

    def _denormalization(self, obj):
        self._prepared_products = PreparedProduct.objects.filter(brand=obj).exclude(withdrawn=True)
        prices = [p.price for p in self._prepared_products]

        common_fields = (
            'name', 'slug', 'note', 'seo_text',
            'search_tags', 'metatag_title', 'metatag_keywords',
            'metatag_description',
        )

        dobj = PreparedBrand(entry=obj)
        BrandDenormalizer.copy_fields(obj, dobj, common_fields)

        dobj.url = obj.get_absolute_url()
        dobj.logo = obj.logo.name if obj.logo else ''
        dobj.letter = dobj.name[:1]
        dobj.country = obj.country
        dobj.product_count = len(prices)
        dobj.min_price = round(min(prices))
        dobj.max_price = round(max(prices))
        dobj.categories = json.dumps(self._build_categories())

        return dobj

    def _build_categories(self):
        valid_category_id = set([c.entry.pk for c in PreparedCategory.objects.all().only('entry')])
        root_category_id = []
        categories = []

        for pi in self._prepared_products.distinct('category').select_related('category'):
            root = get_category_parent_by_level(pi.category, 1)
            if root:
                root_category_id.append(root.pk)

        for root in Category.objects.filter(pk__in=set(root_category_id)):
            category = self.serialize_category(root, url=True)

            descentats_id = [root.pk, ] + list([c.pk for c in root.get_descendants()])
            category_products = self._prepared_products.filter(category__pk__in=descentats_id)
            price_stat = category_products.aggregate(Max('price'), Min('price'))

            category['children'] = list(
                [self.serialize_category(c, url=True) for c in root.get_children().filter(pk__in=valid_category_id)]
            )
            category['count'] = category_products.count()
            category['min_price'] = price_stat['price__min'] or 0
            category['max_price'] = price_stat['price__max'] or 0

            categories.append(category)

        return categories
