from django.core.management.base import BaseCommand
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from apps.denormalization.models import Log
from time import time
import uuid
import os


def get_key():
    has_unique_key = False
    key = None

    while not has_unique_key:
        key = str(uuid.uuid1())
        has_unique_key = Log.objects.filter(key=key).count() == 0

    return key


def get_log(key):
    try:
        log = Log.objects.get(key=key)
    except ObjectDoesNotExist:
        log = Log(key=key, step=0)

    return log


def prepare(title, cell, full=True):
    key = get_key()
    log = get_log(key)
    start = time()
    log.whois = title
    log.save()

    shell_command = 'cd %s && %s %s/manage.py denormalization --key="%s" --cell="%s" --full' % (
        settings.BASE_DIR,
        settings.PYTHON_PATH,
        settings.BASE_DIR,
        key,
        cell,
    )

    do = True

    while do:
        old_step = log.step

        os.system(shell_command)

        log = get_log(key)

        if log.step == old_step:
            do = False
        elif log.step >= log.steps:
            do = False

    log = get_log(key)
    log.total = int(time() - start)
    log.save()


class Command(BaseCommand):
    help = 'Денормализация моделей'

    def add_arguments(self, parser):
        parser.add_argument('--full', action='store_true', help='Полная денормализация игнорируя очередь')
        parser.add_argument('--all', action='store_true', help='Денормализация всех моделей')
        parser.add_argument('--products', '-p', action='store_true', help='Денормализация товаров')
        parser.add_argument('--kinds', '-k', action='store_true', help='Денормализация разновидностей')
        parser.add_argument('--categories', '-c', action='store_true', help='Денормализация категорий')
        parser.add_argument('--brands', '-b', action='store_true', help='Денормализация брендов')
        parser.add_argument('--relink', '-r', action='store_true', help='Перелинковка')
        parser.add_argument('--templates', '-t', action='store_true', help='Кэширование шаблонов')

    def handle(self, *args, **options):
        full = 'full' in options and options['full']
        all_models = 'all' in options and options['all']
        products = 'products' in options and options['products']
        kinds = 'kinds' in options and options['kinds']
        categories = 'categories' in options and options['categories']
        brands = 'brands' in options and options['brands']
        relink = 'relink' in options and options['relink']
        templates = 'templates' in options and options['templates']

        if all_models:
            products = True
            kinds = True
            categories = True
            brands = True
            relink = True
            templates = True

        if products:
            prepare(title='Товары', cell='products', full=full)

        if kinds:
            prepare(title='Разновидности', cell='kinds', full=full)

        if categories:
            prepare(title='Категории', cell='categories', full=full)

        if brands:
            prepare(title='Бренды', cell='brands', full=full)

        if relink:
            prepare(title='Перелинковка', cell='relink')

        if templates:
            prepare(title='Кэширование шаблонов', cell='templates')
