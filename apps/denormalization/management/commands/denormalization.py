from django.core.management.base import BaseCommand
from apps.denormalization.products import ProductDenormalizer
from apps.denormalization.categories import CategoryDenormalizer
from apps.denormalization.brands import BrandDenormalizer
from apps.denormalization.relink import *
from apps.denormalization.templates import *
from apps.denormalization.models import Log


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--full', action='store_true')
        parser.add_argument('--cell', type=str, default='')
        parser.add_argument('--key', type=str, default='')

    def handle(self, *args, **options):
        cell, log, key = None, None, None

        full = 'full' in options and options['full']

        if 'key' in options and options['key']:
            key = str(options['key'])

        if 'cell' in options and options['cell']:
            cell = str(options['cell']).lower()

        if key:
            log = Log.objects.get(key=key)

        if cell == 'products' and log:
            pdn = ProductDenormalizer(is_full=full, log=log)
            pdn.prepare()
        elif cell == 'categories' and log:
            cdn = CategoryDenormalizer(is_full=full, log=log)
            cdn.prepare()
        elif cell == 'brands' and log:
            bdn = BrandDenormalizer(is_full=full, log=log)
            bdn.prepare()
        elif cell == 'relink' and log:
            relink_products()
            relink_categories()
            relink_brands()

            log.step, log.steps = 1, 1
            log.save()
        elif cell == 'templates' and log:
            prepare_templates()

            log.step, log.steps = 1, 1
            log.save()
