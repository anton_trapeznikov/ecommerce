from apps.catalog.models import Category
from apps.denormalization.models import *


def relink_products():
    RingProducts.objects.all().delete()

    # Формирование списка товаров
    legal_products = None
    withdrawn_products = None

    legal_products = set(list([x.entry_id for x in PreparedProduct.objects.filter(withdrawn=False)]))
    withdrawn_products = list([x.entry_id for x in PreparedProduct.objects.filter(withdrawn=True)])

    rings = []  # Список списков (колец)
    ring = []   # Конкретное кольцо

    for parent in Category.objects.filter(level=0):
        descendant_ids = [x.entry_id for x in PreparedProduct.objects.filter(category__in=parent.get_descendants())]
        ring += list(set(list(descendant_ids)) & legal_products)

        if len(ring) > 500:
            rings.append(set(ring))
            ring = []

    if len(ring) > 5:
        rings.append(set(ring))

    if withdrawn_products and len(withdrawn_products) > 1:
        rings.append(set(withdrawn_products))

    # Сохранение
    inserting = []
    for r in rings:
        last = list(r)[-1]
        prev = None

        for id in r:
            if prev:
                ri = RingProducts(of_id=id, to_id=prev)
            else:
                ri = RingProducts(of_id=id, to_id=last)

            prev = id
            inserting.append(ri)

    if inserting:
        RingProducts.objects.bulk_create(inserting, 500)


def relink_categories():
    RingCategories.objects.all().delete()

    # Формирование списка категорий, денормализованных для всех локаций
    legal_categories = set(list([c.entry_id for c in PreparedCategory.objects.all()]))

    ring = []

    for parent in Category.objects.filter(level=0):
        if parent.pk in legal_categories:
            ring.append(parent.pk)

        ring = ring + list(set(list([c.pk for c in parent.get_descendants()])) & legal_categories)

    ring = ring

    # Сохранение
    inserting = []
    last = list(ring)[-1] if ring else None
    prev = None

    for id in ring:
        if prev:
            ri = RingCategories(of_id=id, to_id=prev)
        else:
            ri = RingCategories(of_id=id, to_id=last)

        prev = id
        inserting.append(ri)

    if inserting:
        RingCategories.objects.bulk_create(inserting, 500)


def relink_brands():
    RingBrands.objects.all().delete()

    # Формирование списка брендов, денормализованных для всех локаций
    legal_brands = set(list([b.entry_id for b in PreparedBrand.objects.all()]))

    ring = set(legal_brands)

    # Сохранение
    inserting = []
    last = list(ring)[-1] if ring else None
    prev = None

    for id in ring:
        if prev:
            ri = RingBrands(of_id=id, to_id=prev)
        else:
            ri = RingBrands(of_id=id, to_id=last)

        prev = id
        inserting.append(ri)

    if inserting:
        RingBrands.objects.bulk_create(inserting, 500)
