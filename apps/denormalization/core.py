from abc import ABCMeta, abstractmethod
from django.db.models.fields.files import ImageFieldFile
from decimal import Decimal
from django.conf import settings
from inspect import ismethod
import django
import json


class Denormalizer(object):
    __metaclass__ = ABCMeta

    def __init__(self, denormalizer_model, catalog_model, is_full=False, log=None):
        self._log = log or None
        self._denormalizer_model = django.apps.apps.get_model('denormalization', denormalizer_model)
        self._catalog_model = django.apps.apps.get_model('catalog', catalog_model)

        self._is_full = is_full
        self._new_objects = []
        self._batch_size = settings.DENORMALIZATION_TRANSACTION_SIZE
        self._sources = []

    @abstractmethod
    def _build_sources(self):
        raise NotImplementedError('Abstract method raise')

    @abstractmethod
    def _denormalization(self, obj):
        raise NotImplementedError('Abstract method raise')

    def _after_save(self):
        pass

    def _clear_queue(self):
        pass

    def prepare(self):
        self._build_sources()

        for obj in self._sources:
            dobj = self._denormalization(obj)

            if dobj:
                self._new_objects.append(dobj)

        self._save()
        self._after_save()
        self._clear_queue()

    def _save(self):
        if self._log and self._new_objects:
            self._log.entries = json.dumps(list([e.entry_id for e in self._new_objects]))
            self._log.save()

        self._denormalizer_model.objects.filter(entry__in=self._sources).delete()

        if self._new_objects:
            self._denormalizer_model.objects.bulk_create(self._new_objects, self._batch_size)

    # Вспомогательные методы

    def category_valid(self, category):
        if category:
            return (category.get_ancestors().filter(is_active=False).count() == 0) and category.is_active

        return False

    def brand_valid(self, brand):
        if brand:
            return brand.is_active

        return False

    @staticmethod
    def copy_fields(source, recipient, fields=[]):
        for field in fields:
            if hasattr(source, field):
                setattr(recipient, field, getattr(source, field))

        return True

    @staticmethod
    def copy_dict_fields(source, recipient, fields=[]):
        for field in fields:
            if field in source:
                recipient[field] = source[field]

        return True

    @staticmethod
    def flat_obj_fields_to_dict(obj, fields=[]):
        returned = dict()

        for field in fields:
            if hasattr(obj, field):
                if ismethod(getattr(obj, field)):
                    raw_data = getattr(obj, field)()
                else:
                    raw_data = getattr(obj, field)

                data = None

                if isinstance(raw_data, ImageFieldFile):
                    data = raw_data.name
                elif isinstance(raw_data, Decimal):
                    data = float(raw_data)
                elif isinstance(raw_data, (int, float, bool, unicode, str)):
                    data = raw_data
                else:
                    raise Exception('unknown field type')

                if data:
                    returned[field] = data
            else:
                raise Exception('dont exists field')

        return returned

    def serialize_brand(self, brand, logo=False, slug=False, url=False):
        b = dict()

        b['id'] = brand.pk
        b['name'] = brand.name

        if logo:
            b['logo'] = brand.logo.name if brand.logo else ''

        if slug:
            b['slug'] = brand.slug

        if url:
            b['url'] = brand.get_absolute_url()

        return b

    def serialize_category(self, category, slug=False, url=False):
        c = dict()

        c['id'] = category.pk
        c['name'] = category.name
        c['menu_name'] = category.menu_name or category.name

        if slug:
            c['slug'] = category.slug

        if url:
            c['url'] = category.get_absolute_url()

        return c
