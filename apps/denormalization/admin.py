from django.contrib import admin
from apps.denormalization.models import *


class LogAdmin(admin.ModelAdmin):
    list_filter = ['whois']
    list_display = ('key', 'step', 'steps', 'whois', 'created_at', 'updated_at', 'total')
    readonly_fields = ('key', 'step', 'steps', 'whois', 'created_at', 'updated_at', 'total', 'entries')


admin.site.register(Log, LogAdmin)
