# -*- coding: utf-8 -*-

from apps.catalog.models import *
from apps.denormalization.core import Denormalizer
from apps.denormalization.models import PreparedProduct
from apps.denormalization.models import PreparedCategory
import json


class CategoryDenormalizer(Denormalizer):
    def __init__(self, is_full, log):
        Denormalizer.__init__(
            self,
            denormalizer_model='PreparedCategory',
            catalog_model='Category',
            is_full=is_full,
            log=log
        )

    def _build_sources(self):
        self.valid_category_id = []

        for product in PreparedProduct.objects.all().distinct('category').only('category').select_related('category'):
            self.valid_category_id.append(product.category.pk)
            self.valid_category_id += list([c.pk for c in product.category.get_ancestors()])

        self.valid_category_id = set(self.valid_category_id)

        source_id = []

        if self._is_full:
            source_id = [c.pk for c in Category.objects.filter(pk__in=self.valid_category_id)]
        else:
            pass

        source_id = list(source_id)
        chunk_size = 100
        source_id_chunks = [source_id[i:i + chunk_size] for i in range(0, len(source_id), chunk_size)]

        if not self._log.steps:
            self._log.steps = len(source_id_chunks)

        step = self._log.step
        self._log.step += 1
        self._log.save()

        if 0 <= step < self._log.steps:
            self._sources = Category.objects.filter(pk__in=source_id_chunks[step])
        else:
            self._sources = []

    def _denormalization(self, obj):
        descendants_id = list([c.pk for c in obj.get_descendants().filter(pk__in=self.valid_category_id)])
        categories_id = descendants_id + [obj.pk, ]

        self._prepared_products = PreparedProduct.objects.filter(category__pk__in=categories_id).exclude(withdrawn=True)
        prices = [p.price for p in self._prepared_products]

        common_fields = (
            'name', 'slug', 'note', 'menu_name',
            'search_tags', 'metatag_title', 'metatag_keywords',
            'metatag_description', 'level',
        )

        dobj = PreparedCategory(entry=obj)

        CategoryDenormalizer.copy_fields(obj, dobj, common_fields)

        dobj.url = obj.get_absolute_url()
        dobj.image = obj.image.name if obj.image else ''
        dobj.descendants = json.dumps(descendants_id)
        dobj.product_count = len(prices)
        dobj.min_price = round(min(prices))
        dobj.max_price = round(max(prices))
        dobj.menu_data = json.dumps(self._build_menu_data(obj))
        dobj.brands = json.dumps(self._build_brands(self._prepared_products))
        dobj.countries = json.dumps(self._build_countries(self._prepared_products))
        dobj.features = json.dumps(self._build_specification(self._prepared_products))
        dobj.second_name = obj.second_name or obj.name
        dobj.lines = json.dumps(self._build_lines(self._prepared_products))
        return dobj

    def _build_menu_data(self, category):
        return {
            'ancestors': list([self.serialize_category(c, url=True) for c in category.get_ancestors()]),
            'children': list([self.serialize_category(c, url=True) for c in category.get_children()]),
        }

    def _build_countries(self, products):
        countries = []
        for p in products.filter(country__isnull=False).distinct('country').select_related('country'):
            countries.append({
                'name': p.country.name,
                'slug': p.country.slug,
            })

        countries.sort(key=lambda t: t['name'])

        return countries

    def _build_lines(self, products):
        lines = []
        for p in products.filter(line__isnull=False).distinct('line').select_related('line'):
            lines.append({
                'id': p.line.pk,
                'name': p.line.name,
                'slug': p.line.slug,
                'url': p.line.get_absolute_url(),
                'note': p.line.note
            })
        return lines

    def _build_brands(self, products):
        brands = list(
            [self.serialize_brand(p.brand, slug=True) for p in products.distinct('brand').select_related('brand')]
        )
        brands.sort(key=lambda t: t['name'])

        return brands

    def _build_specification(self, products):
        specification = {}

        spec_list = Specification.objects.filter(
            product__in=[p.entry for p in products.distinct('entry').select_related('entry')]
        ).select_related('feature')

        for s in spec_list:
            value = s.value
            slugify = s.slugify
            feature = s.feature

            if feature.pk not in specification:
                specification[feature.pk] = {
                    'name': feature.name,
                    'units': feature.units or None,
                    'values': [],
                    'is_numeric': False,
                }

            specification[feature.pk]['values'].append((value, slugify))

        for fpk, data in dict(specification).items():
            values_index = []
            digital_values = []
            clean_values = []
            is_digital = True

            for v in data['values']:
                raw_value = v[0]
                slug = v[1]
                digital_value = None

                if slug not in values_index:
                    values_index.append(slug)

                    try:
                        digital_value = float(raw_value.replace(',', '.').replace(' ', ''))
                    except (ValueError, TypeError):
                        is_digital = False
                        digital_value = None

                    clean_values.append((slug, raw_value))
                    digital_values.append((slug, digital_value))

            if digital_values and is_digital:
                digital_values.sort(key=lambda t: t[1])

            clean_values.sort(key=lambda t: t[1])

            specification[fpk]['is_numeric'] = is_digital
            specification[fpk]['values'] = digital_values if is_digital else clean_values

        return specification
