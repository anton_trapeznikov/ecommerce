from django.apps import AppConfig


class CatalogConfig(AppConfig):
    name = 'apps.catalog'
    verbose_name = "Каталог товаров"

    def ready(self):
        import apps.catalog.signals
