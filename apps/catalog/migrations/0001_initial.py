# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-09-28 17:39
from __future__ import unicode_literals

import apps.helpers.utils
from django.db import migrations, models
import django.db.models.deletion
import django.db.models.manager
import mptt.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Brand',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=255, verbose_name='Название')),
                ('slug', models.SlugField(max_length=255, unique=True, verbose_name='ЧПУ')),
                ('is_active', models.BooleanField(db_index=True, default=True, verbose_name='Активность')),
                ('metatag_title', models.CharField(blank=True, default='', max_length=255, verbose_name='Title (метатег)')),
                ('metatag_keywords', models.CharField(blank=True, default='', max_length=255, verbose_name='Keywords (метатег)')),
                ('metatag_description', models.CharField(blank=True, default='', max_length=255, verbose_name='Description (метатег)')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Создан')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Изменен')),
                ('search_tags', models.CharField(blank=True, default='', help_text='Эти слова будут добавлены в поисковый индекс', max_length=255, verbose_name='Теги для поиска')),
                ('logo', models.ImageField(blank=True, null=True, upload_to=apps.helpers.utils.generate_upload_name, verbose_name='Логотип')),
                ('note', models.TextField(blank=True, default='', verbose_name='Описание')),
                ('seo_text', models.TextField(blank=True, default='', verbose_name='SEO-описание')),
            ],
            options={
                'verbose_name': 'Бренд',
                'verbose_name_plural': 'Бренды',
            },
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=255, verbose_name='Название')),
                ('slug', models.SlugField(max_length=255, unique=True, verbose_name='ЧПУ')),
                ('is_active', models.BooleanField(db_index=True, default=True, verbose_name='Активность')),
                ('metatag_title', models.CharField(blank=True, default='', max_length=255, verbose_name='Title (метатег)')),
                ('metatag_keywords', models.CharField(blank=True, default='', max_length=255, verbose_name='Keywords (метатег)')),
                ('metatag_description', models.CharField(blank=True, default='', max_length=255, verbose_name='Description (метатег)')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Создан')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Изменен')),
                ('search_tags', models.CharField(blank=True, default='', help_text='Эти слова будут добавлены в поисковый индекс', max_length=255, verbose_name='Теги для поиска')),
                ('image', models.ImageField(blank=True, null=True, upload_to=apps.helpers.utils.generate_upload_name, verbose_name='Картинка')),
                ('menu_name', models.CharField(blank=True, default='', max_length=256, verbose_name='Имя для меню')),
                ('second_name', models.CharField(blank=True, default='', max_length=256, verbose_name='Второе имя')),
                ('note', models.TextField(blank=True, default='', verbose_name='Описание')),
                ('low_priority', models.BooleanField(default=False, help_text='Товары этой категории в списке будут отображаться после остальных', verbose_name='Низкоприоритетная категория')),
                ('lft', models.PositiveIntegerField(db_index=True, editable=False)),
                ('rght', models.PositiveIntegerField(db_index=True, editable=False)),
                ('tree_id', models.PositiveIntegerField(db_index=True, editable=False)),
                ('level', models.PositiveIntegerField(db_index=True, editable=False)),
                ('parent', mptt.fields.TreeForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='children', to='catalog.Category', verbose_name='Родительская категория')),
            ],
            options={
                'verbose_name': 'Категория',
                'verbose_name_plural': 'Категории',
            },
            managers=[
                ('tree', django.db.models.manager.Manager()),
            ],
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=255, verbose_name='Название')),
                ('slug', models.SlugField(max_length=255, unique=True, verbose_name='ЧПУ')),
            ],
            options={
                'verbose_name': 'Страна',
                'verbose_name_plural': 'Страны',
            },
        ),
        migrations.CreateModel(
            name='Feature',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=255, verbose_name='Название')),
                ('units', models.CharField(blank=True, default='', max_length=16, verbose_name='Единица измерения')),
                ('for_filter', models.BooleanField(db_index=True, default=True, verbose_name='Показывать в фильтре')),
                ('for_itemcard', models.BooleanField(db_index=True, default=True, verbose_name='Показывать в карточке товара')),
            ],
            options={
                'verbose_name': 'Характеристика',
                'verbose_name_plural': 'Характеристики',
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=255, verbose_name='Название')),
                ('slug', models.SlugField(max_length=255, unique=True, verbose_name='ЧПУ')),
                ('is_active', models.BooleanField(db_index=True, default=True, verbose_name='Активность')),
                ('metatag_title', models.CharField(blank=True, default='', max_length=255, verbose_name='Title (метатег)')),
                ('metatag_keywords', models.CharField(blank=True, default='', max_length=255, verbose_name='Keywords (метатег)')),
                ('metatag_description', models.CharField(blank=True, default='', max_length=255, verbose_name='Description (метатег)')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Создан')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Изменен')),
                ('search_tags', models.CharField(blank=True, default='', help_text='Эти слова будут добавлены в поисковый индекс', max_length=255, verbose_name='Теги для поиска')),
                ('code', models.CharField(help_text='Должен быть уникален в рамках каталога', max_length=32, unique=True, verbose_name='Код товара')),
                ('withdrawn', models.BooleanField(db_index=True, default=False, verbose_name='Товар снят с продажи')),
                ('note', models.TextField(blank=True, default='', verbose_name='Описание')),
                ('price', models.FloatField(blank=True, db_index=True, null=True, verbose_name='Цена')),
                ('instock', models.BooleanField(db_index=True, default=False, verbose_name='Наличие')),
                ('hight_priority', models.BooleanField(default=False, help_text='Товар в списке будет отображаться перед остальными', verbose_name='Приоритетный товар')),
                ('_main_image', models.ImageField(blank=True, null=True, upload_to=apps.helpers.utils.generate_upload_name, verbose_name='Фотография')),
                ('analog', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='catalog.Product', verbose_name='Аналог')),
                ('brand', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='catalog.Brand', verbose_name='Бренд')),
                ('category', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='catalog.Category', verbose_name='Категория')),
            ],
            options={
                'verbose_name': 'Товар',
                'verbose_name_plural': 'Товары',
            },
        ),
        migrations.CreateModel(
            name='ProductImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to=apps.helpers.utils.generate_upload_name, verbose_name='Фотография')),
                ('is_main', models.BooleanField(default=False, verbose_name='Основное изображение')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='catalog.Product', verbose_name='Товар')),
            ],
            options={
                'verbose_name': 'Фото товара',
                'verbose_name_plural': 'Фото товаров',
            },
        ),
        migrations.CreateModel(
            name='ProductKind',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=255, verbose_name='Название')),
                ('slug', models.SlugField(max_length=255, unique=True, verbose_name='ЧПУ')),
                ('is_active', models.BooleanField(db_index=True, default=True, verbose_name='Активность')),
                ('metatag_title', models.CharField(blank=True, default='', max_length=255, verbose_name='Title (метатег)')),
                ('metatag_keywords', models.CharField(blank=True, default='', max_length=255, verbose_name='Keywords (метатег)')),
                ('metatag_description', models.CharField(blank=True, default='', max_length=255, verbose_name='Description (метатег)')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Создан')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Изменен')),
                ('search_tags', models.CharField(blank=True, default='', help_text='Эти слова будут добавлены в поисковый индекс', max_length=255, verbose_name='Теги для поиска')),
                ('note', models.TextField(blank=True, default='', verbose_name='Описание')),
            ],
            options={
                'verbose_name': 'Разновидность товара',
                'verbose_name_plural': 'Разновидности товаров',
            },
        ),
        migrations.CreateModel(
            name='ProductLine',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=255, verbose_name='Название')),
                ('slug', models.SlugField(max_length=255, unique=True, verbose_name='ЧПУ')),
                ('is_active', models.BooleanField(db_index=True, default=True, verbose_name='Активность')),
                ('metatag_title', models.CharField(blank=True, default='', max_length=255, verbose_name='Title (метатег)')),
                ('metatag_keywords', models.CharField(blank=True, default='', max_length=255, verbose_name='Keywords (метатег)')),
                ('metatag_description', models.CharField(blank=True, default='', max_length=255, verbose_name='Description (метатег)')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Создан')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Изменен')),
                ('search_tags', models.CharField(blank=True, default='', help_text='Эти слова будут добавлены в поисковый индекс', max_length=255, verbose_name='Теги для поиска')),
                ('image', models.ImageField(blank=True, null=True, upload_to=apps.helpers.utils.generate_upload_name, verbose_name='Логотип')),
                ('note', models.TextField(blank=True, default='', verbose_name='Описание')),
            ],
            options={
                'verbose_name': 'Продуктовая линия',
                'verbose_name_plural': 'Продуктовые линии',
            },
        ),
        migrations.CreateModel(
            name='Specification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.CharField(max_length=128, verbose_name='Значение')),
                ('slugify', models.CharField(db_index=True, max_length=255, verbose_name='Значение')),
                ('feature', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='catalog.Feature', verbose_name='Характеристика')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='catalog.Product', verbose_name='Товар')),
            ],
            options={
                'verbose_name': 'Характеристика товара',
                'verbose_name_plural': 'Характеристики товаров',
            },
        ),
        migrations.AddField(
            model_name='product',
            name='kind',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='catalog.ProductKind', verbose_name='Разновидность товара'),
        ),
        migrations.AddField(
            model_name='product',
            name='line',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='catalog.ProductLine', verbose_name='Продуктовая линия'),
        ),
        migrations.AlterUniqueTogether(
            name='feature',
            unique_together=set([('name', 'units')]),
        ),
        migrations.AddField(
            model_name='brand',
            name='country',
            field=models.ForeignKey(blank=True, help_text='Принадлежность бренда', null=True, on_delete=django.db.models.deletion.CASCADE, to='catalog.Country', verbose_name='Страна'),
        ),
    ]
