from haystack import indexes
from apps.catalog.models import Product, Category, Brand
from apps.denormalization.models import *


class ProductIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    code = indexes.CharField(boost=2.500)
    tags = indexes.CharField(model_attr='search_tags', boost=2.000)
    name = indexes.CharField(model_attr='name', boost=1.500)
    category_name = indexes.CharField(model_attr='category__name', boost=0.750)
    category_tags = indexes.CharField(model_attr='category__search_tags', boost=1.000)
    brand_name = indexes.CharField(model_attr='brand__name', boost=0.250)
    brand_tags = indexes.CharField(model_attr='brand__search_tags', boost=0.500)
    parent_category_name = indexes.CharField(boost=0.125)
    parent_category_tags = indexes.CharField(boost=0.250)

    def get_model(self):
        return Product

    def index_queryset(self, using=None):
        return Product.objects.filter(pk__in=[p['entry_id'] for p in PreparedProduct.objects.all().values('entry_id')])

    def prepare_code(self, object):
        return object.code.lower()

    def prepare_name(self, object):
        return object.name.lower()

    def prepare_tags(self, object):
        return object.search_tags.lower()

    def prepare_category_name(self, object):
        return object.category.name.lower()

    def prepare_category_tags(self, object):
        return object.category.search_tags.lower()

    def prepare_brand_name(self, object):
        return object.brand.name.lower()

    def prepare_brand_tags(self, object):
        return object.brand.search_tags.lower()

    def prepare_parent_category_name(self, object):
        return ', '.join([c.name.lower() for c in object.category.get_ancestors()])

    def prepare_parent_category_tags(self, object):
        return ', '.join([c.search_tags.lower() for c in object.category.get_ancestors()])


class CategoryIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    name = indexes.CharField(model_attr='name', boost=1.125)
    tags = indexes.CharField(model_attr='search_tags', boost=1.500)
    second_name = indexes.CharField(model_attr='search_tags', boost=1.100)
    parent_category_name = indexes.CharField(boost=0.725)
    parent_category_tags = indexes.CharField(boost=0.500)

    def get_model(self):
        return Category

    def index_queryset(self, using=None):
        return Category.objects.filter(
            pk__in=[p['entry_id'] for p in PreparedCategory.objects.all().values('entry_id')]
        )

    def prepare_name(self, object):
        return object.name.lower()

    def prepare_tags(self, object):
        return object.search_tags.lower()

    def prepare_second_name(self, object):
        return object.second_name.lower()

    def prepare_parent_category_name(self, object):
        return ', '.join([c.name.lower() for c in object.get_ancestors()])

    def prepare_parent_category_tags(self, object):
        return ', '.join([c.search_tags.lower() for c in object.get_ancestors()])


class BrandIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    name = indexes.CharField(model_attr='name', boost=1.000)
    tags = indexes.CharField(model_attr='search_tags', boost=1.500)

    def get_model(self):
        return Brand

    def index_queryset(self, using=None):
        return Brand.objects.filter(pk__in=[b['entry_id'] for b in PreparedBrand.objects.all().values('entry_id')])

    def prepare_name(self, object):
        return object.name.lower()

    def prepare_tags(self, object):
        return object.search_tags.lower()
