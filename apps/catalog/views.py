from django.contrib.auth.decorators import login_required
from django.template.response import TemplateResponse
from django.core.exceptions import ObjectDoesNotExist
from django.template.loader import render_to_string
from django.http import Http404, HttpResponse
from django.conf import settings
from apps.helpers.utils import paginate, to_int, to_float, distribute_columns, get_metatags_params
from apps.helpers.utils import get_query, make_queryset, declination
from ecommerce.models import TextBlocks, Settings
from apps.denormalization.models import *
from apps.catalog.models import *
from apps.catalog.forms import *
import random
import json

'''
description=settings.TERCERO_TASKS_TEMPLATES["client_documents_uploaded"]["message"].format(**{
    'first_name': instance.first_name,
    'last_name': instance.last_name,
})
'''

def get_404_or_500(request, model, slug, log_name):
    raw_entries = model.objects.filter(slug=slug)
    entry_id = raw_entries[0].pk if raw_entries else None
    current_log = Log.objects.all().last()
    processed_entries = []

    if current_log and current_log.whois == log_name and current_log.total is None and current_log.entries is not None:
        try:
            processed_entries = json.loads(current_log.entries)
        except Exception:
            processed_entries = []

    if entry_id and processed_entries and entry_id in processed_entries:
        return TemplateResponse(request=request, template='preparations.html', context={}, status=500)
    else:
        raise Http404


def index(request):
    categories = list(PreparedCategory.objects.filter(level=0).order_by('name'))

    for c in categories:
        c.column_weight = len(c.children) if c.children else 0

    columns = distribute_columns(categories, 3)

    head_settings = Settings.objects.filter(parameter__in=('catalog_h1', 'catalog_h2'))
    headers = {s.parameter: s.value.strip() for s in head_settings if s.value}
    textBlocks = TextBlocks.objects.filter(block='catalog_index')

    brands = [b.name for b in PreparedBrand.objects.all().only('name')]
    product_count = PreparedProduct.objects.all().only('name').count()
    rounded_product_count = round(product_count / 100.0) * 100

    meatatags_template = settings.METATAGS['catalog_root']

    title = meatatags_template['title'].format(**{
        'brands': ', '.join(brands),
    })

    keywords = meatatags_template['keywords'].format(**{
        'brands': str(', '.join(brands)).lower(),
    })

    description = meatatags_template['description'].format(**{
        'product_count': rounded_product_count,
        'product_declination': declination(
            number=rounded_product_count,
            cases=('товар', 'товара', 'товаров'),
        ),
        'brand_count': len(brands),
        'brand_declination': declination(
            number=len(brands),
            cases=('бренда', 'брендов', 'брендов'),
        ),
        'brands': ', '.join(brands),
    })

    request.meta_tags = get_metatags_params(
        whois='catalog_index',
        title=title,
        keywords=keywords,
        description=description,
    )

    return TemplateResponse(request, 'catalog/index.html', {
        'columns': columns,
        'text': textBlocks[0] if textBlocks else None,
        'headers': headers,
        'brands': brands,
        'product_count': product_count,
    })


def category(request, slug):
    try:
        category = PreparedCategory.objects.get(slug=slug)
    except ObjectDoesNotExist:
        return get_404_or_500(
            request=request,
            model=Category,
            slug=slug,
            log_name='Категории'
        )

    route_root, route_products = False, False

    if category.level > 0:
        route_products = True
    else:
        have_products = PreparedProduct.objects.filter(
            category=category.entry
        ).count() > 0

        if have_products:
            route_products = True
        else:
            route_root = True

    if route_root:
        return root_category(request, category)
    elif route_products:
        return products(request, category)


def root_category(request, category):
    subcategories = PreparedCategory.objects.filter(
        entry__parent_id=category.entry_id
    )

    for s in subcategories:
        s.count = s.product_count

    request.meta_tags = get_metatags_params(
        whois='root_category',
        obj=category
    )

    return TemplateResponse(request, 'catalog/root_category.html', {
        'category': category,
        'subcategories': subcategories,
    })


def products(request, category):
    request.meta_tags = get_metatags_params(whois='category', obj=category)
    request_types = ('list', 'quantity', 'paging')
    request_type = request.GET.get('request-type', None)

    if request_type and request_type in request_types:
        pass
    else:
        request_type = request_types[0]

    small_menu_data = category.menu

    # Извлечение товаров и фильтрация

    products = PreparedProduct.objects.filter(category_id__in=[category.entry_id, ] + json.loads(category.descendants))
    filter_form = FilterForm(request, category)
    products = filter_form.filter(products)

    if request_type in ('list', 'paging'):
        # Сортировка
        products = products.order_by(*filter_form.order_by)

        # Пагинация
        products_per_page = 10
        paginator = paginate(
            items=products,
            current_page=to_int(request.GET.get('page', '1'), 1),
            item_per_page=products_per_page
        )

        next_page_size = paginator['item_count'] - paginator['page'] * products_per_page
        if next_page_size > products_per_page:
            next_page_size = products_per_page
        elif next_page_size < 0:
            next_page_size = 0

        filter_form.initial_field(field_type='page', initial=paginator['page'])

        products = paginator['items']
    elif request_type == 'quantity':
        return HttpResponse(json.dumps({
            'quantity': products.count(),
            'token': request.GET.get('request-token', ''),
        }))

    products_only = request.GET.get('products-only', None) == 'yes'
    template = 'catalog/products.html' if not products_only else 'catalog/products__list.html'

    context = {
        'products': products,
        'next_page_size': next_page_size,
        'page': paginator['page'],
        'page_count': paginator['page_count'],
    }

    if not products_only:
        view = request.COOKIES.get('view', 'blocks')
        if view not in ('blocks', 'list', 'small-blocks'):
            view = 'blocks'

        has_view = request.COOKIES.get('has-view', False)

        relink_to = None
        try:
            rc = RingCategories.objects.get(of_id=category.entry_id)
            relink_to = {
                'url': rc.to.get_absolute_url(),
                'name': rc.to.name
            }
        except ObjectDoesNotExist:
            relink_to = None

        additional_context = {
            'category': category,
            'view': view,
            'has_view': has_view,
            'small_menu_data': small_menu_data,
            'filter_form': filter_form,
            'relink_to': relink_to,
            'pages': range(1, context['page_count'] + 1),
        }

        for var, val in additional_context.items():
            if var not in context:
                context[var] = val

    return TemplateResponse(request, template, context)


def brand(request, slug):
    try:
        brand = PreparedBrand.objects.get(slug=slug)
    except ObjectDoesNotExist:
        return get_404_or_500(
            request=request,
            model=Brand,
            slug=slug,
            log_name='Бренды'
        )

    request.meta_tags = get_metatags_params(whois='brand', obj=brand)

    columns = 1

    if brand.logo:
        columns += 1

    if brand.note:
        columns += 1

    return TemplateResponse(request, 'catalog/brand.html', {
        'brand': brand,
        'columns': columns,
        'categories': brand.children,
        'root_categories': [
            p.parent for p in Category.objects.filter(
                pk__in=[c['id'] for c in brand.children]
            ).select_related(
                'parent'
            ).distinct(
                'parent'
            ).order_by(
                'parent'
            ).only(
                'parent'
            )
        ],
    })


def brand_list(request):
    request.meta_tags = get_metatags_params(whois='brands')

    letter_index = {}
    country_index = {}

    for b in PreparedBrand.objects.all().select_related('country').order_by('name'):
        letter = b.letter.upper()
        country = b.country.name.title() if b.country else 'Неизвестно'

        if country not in country_index:
            country_index[country] = []

        if letter not in letter_index:
            letter_index[letter] = []

        letter_index[letter].append(b)
        country_index[country].append(b)

    letters = [{'name': l, 'brands': brands} for l, brands in letter_index.items()]
    countries = [{'name': c, 'brands': brands} for c, brands in country_index.items()]

    letters.sort(key=lambda v: v['name'])
    countries.sort(key=lambda v: v['name'])

    head_settings = Settings.objects.filter(parameter__in=('brands_h1', 'brands_h2'))
    headers = {s.parameter: s.value.strip() for s in head_settings if s.value}
    textBlocks = TextBlocks.objects.filter(block='brand_index')

    return TemplateResponse(request, 'catalog/brands.html', {
        'countries': countries,
        'letters': letters,
        'text': textBlocks[0] if textBlocks else None,
        'headers': headers,
    })


def line(request, slug):
    return HttpResponse('its line')


def kind(request, slug):
    return HttpResponse('its kind')


def product(request, slug):
    try:
        product = PreparedProduct.objects.get(slug=slug)
    except ObjectDoesNotExist:
        return get_404_or_500(request=request, model=Product, slug=slug, log_name='Товары')

    request.meta_tags = get_metatags_params(whois='product', obj=product)

    features = [f for f in product.feauture_list if f['for_itemcard']]

    kinds = []
    if product.kind_products:
        kinds = PreparedProduct.objects.filter(entry__id__in=product.kind_products)

    prices = (product.price - 0.25 * product.price, product.price + 0.25 * product.price)

    all_simulars = PreparedProduct.objects.filter(
        category=product.category,
        price__gte=prices[0],
        price__lte=prices[1]
    )

    simular_id = [x['pk'] for x in all_simulars.exclude(brand_id=product.brand_id).values('pk')]

    #abc = product.images_list
    #assert False
    return TemplateResponse(request, 'catalog/product.html', {
        'product': product,
        'features': features,
        'has_description': product.note and len(product.note) > 10,
        'has_features': len(features) > 0,
        'kinds': kinds,
        'microdata': product.note and len(product.note) > 10,
        'simular': PreparedProduct.objects.filter(pk__in=random.sample(simular_id, min(4, len(simular_id)))),
    })


def popupSearch(request):
    query = request.GET.get('q')
    objects = make_queryset(query)

    products_count = objects.models(Product).count()

    product_index = [int(p.pk) for p in objects.models(Product)[:7]]
    category_index = [int(c.pk) for c in objects.models(Category)[:7]]
    brand_index = [int(b.pk) for b in objects.models(Brand)[:7]]

    products, categories, brands = [], [], []

    if product_index:
        products = list(PreparedProduct.objects.filter(entry_id__in=product_index))
        products.sort(key=lambda t: product_index.index(t.entry_id))

    if category_index:
        categories = list(PreparedCategory.objects.filter(entry_id__in=category_index))
        categories.sort(key=lambda t: category_index.index(t.entry_id))

    if brand_index:
        brands = list(PreparedBrand.objects.filter(entry_id__in=brand_index))
        brands.sort(key=lambda t: brand_index.index(t.entry_id))

    response = {
        'isEmpty': product_index == category_index == brand_index == [],
        'productCount': products_count,
        'popup': None,
        'token': request.GET.get('token', None),
    }

    if not response['isEmpty']:
        response['popup'] = render_to_string('search/popup.html', {
            'products_count': products_count,
            'products': products,
            'categories': categories,
            'brands': brands,
            'query': query,
        })

    return HttpResponse(json.dumps(response))


def search(request):
    if request.GET.get('isPopup', None) == 'yes':
        return popupSearch(request)

    raw_query = request.GET.get('q', '')
    stemmed_words = get_query(raw_query)

    if len(stemmed_words) == 1:
        query = stemmed_words[0]
    else:
        query = raw_query

    query = '*%s*' % query

    objects = make_queryset(raw_query)
    page = to_int(request.GET.get('page'), 1)
    products_per_page = 20

    paginator = paginate(
        items=[int(p.pk) for p in objects.models(Product)],
        current_page=page,
        item_per_page=products_per_page
    )

    product_index = paginator['items']
    products = list(PreparedProduct.objects.filter(entry_id__in=product_index))
    products.sort(key=lambda t: product_index.index(t.entry_id))

    request.meta_tags = get_metatags_params(whois='search')

    return TemplateResponse(request, 'catalog/search.html', {
        'old_search_query': raw_query,
        'products': products,
        'page': paginator['page'],
        'page_count': paginator['page_count'],
        'pages': range(1, paginator['page_count'] + 1),
    })


@login_required
def products_by_admin(request):
    if not request.user.is_superuser:
        raise Http404

    form = AdminFilterForm(request=request)
    products_per_page = to_int(request.GET.get('page-size', 25), 25)

    all_products = form.filter(products=Product.objects.all()).order_by('name')

    paginator = paginate(
        items=all_products,
        current_page=form.page,
        item_per_page=products_per_page
    )

    products_pks = [p.pk for p in paginator['items']]

    products_images = {}
    for pi in ProductImage.objects.filter(product_id__in=products_pks):
        if pi.product_id not in products_images:
            products_images[pi.product_id] = []

        try:
            width = pi.image.width
            height = pi.image.height
        except Exception:
            width, height = None, None

        if width and height:
            products_images[pi.product_id].append({
                'image': pi.image.name,
                'is_main': pi.is_main,
                'width': width,
                'height': height,
                'pk': pi.pk,
            })

    specification = Specification.objects.filter(product__in=all_products)

    products_specification = {}
    features = []

    for fi in specification.distinct('feature').select_related('feature'):
        features.append({'pk': fi.feature.pk, 'name': fi.feature.name, 'units': fi.feature.units})

    features.sort(key=lambda t: t['name'])

    specification = specification.filter(product_id__in=products_pks)

    for fi in specification:
        if fi.value:
            if fi.product_id not in products_specification:
                products_specification[fi.product_id] = {}

            if fi.feature_id not in products_specification[fi.product_id]:
                products_specification[fi.product_id][fi.feature_id] = []

            products_specification[fi.product_id][fi.feature_id].append(fi.value)

    products = []
    max_images = max([len(products_images[k]) for k in products_images] or [0, ])
    for p in paginator['items']:
        images = products_images.get(p.pk, [])
        while len(images) < max_images:
            images.append(None)

        features_index = products_specification.get(p.pk, {})
        product_features = [{'pk': f['pk'], 'value': features_index.get(f['pk'], [])} for f in features]

        price = ''
        if p.price:
            if round(p.price) == p.price:
                price = int(p.price)
            else:
                price = str(p.price).replace('.', ',')

        product = {
            'pk': p.pk,
            'name': p.name,
            'category': p.category_id or '',
            'brand': p.brand_id or '',
            'line': p.line_id or '',
            'note': p.note,
            'images': images,
            'features': product_features,
            'price': price,
        }

        products.append(product)

    another_features = Feature.objects.all().exclude(pk__in=[f['pk'] for f in features]).order_by('name')

    categories = [{'pk': c.pk, 'name': c.name} for c in Category.objects.all().order_by('name').only('pk', 'name')]
    brands = [{'pk': b.pk, 'name': b.name} for b in Brand.objects.all().order_by('name').only('pk', 'name')]
    lines = [{'pk': l.pk, 'name': l.name} for l in ProductLine.objects.all().order_by('name').only('pk', 'name')]

    return HttpResponse(
        json.dumps({
            'page': paginator['page'],
            'pages': list(range(1, paginator['page_count'] + 1)),
            'products': products,
            'categories': categories,
            'brands': brands,
            'lines': lines,
            'max_images': max_images,
            'features': features,
            'another_features': [{'pk': f.pk, 'name': f.name, 'units': f.units} for f in another_features],
        })
    )


@login_required
def save_by_admin(request):
    if not request.user.is_superuser:
        raise Http404

    new_images = []

    for new_img in json.loads(request.POST.get('new-images')):
        image_file = request.FILES.get(new_img['field'], None)

        if image_file:
            new_images.append(ProductImage(product_id=to_int(new_img['pk']), image=image_file))

    if new_images:
        ProductImage.objects.bulk_create(new_images, 25)

    image_actions = {to_int(k): v for k, v in json.loads(request.POST.get('image-actions')).items()}

    for img in ProductImage.objects.filter(pk__in=image_actions.keys()):
        if img.is_main != image_actions[img.pk]['is_main']:
            img.is_main = image_actions[img.pk]['is_main']
            img.save()

    names = json.loads(request.POST.get('names'))
    notes = json.loads(request.POST.get('notes'))
    prices = json.loads(request.POST.get('prices'))
    categories = json.loads(request.POST.get('categories'))
    brands = json.loads(request.POST.get('brands'))
    lines = json.loads(request.POST.get('lines'))

    products = Product.objects.filter(pk__in=[to_int(k) for k in names.keys() if to_int(k)])
    trash_note_signs = ['<p>', '</p>', '&nbsp;', '<br>', '<br/>', '<div>', '</div>', ' ', ]

    for p in products:
        pid = '%s' % p.pk
        can_save = False

        if p.name != names[pid]:
            p.name = names[pid]
            can_save = True

        price = prices[pid] or None
        price = to_float(price.replace(',', '.') if price else price)

        brand = to_int(brands[pid])
        if p.brand_id != brand:
            p.brand_id = brand
            can_save = True

        category = to_int(categories[pid])
        if p.category_id != category:
            p.category_id = category
            can_save = True

        line = to_int(lines[pid])
        if p.line_id != line:
            p.line_id = line
            can_save = True

        if p.price != price:
            p.price = price
            can_save = True

        raw_note = notes[pid] or ''
        test_note = raw_note.lower()
        for trash in trash_note_signs:
            test_note = test_note.replace(trash, '')

        note = raw_note if test_note else ''
        if p.note != note:
            p.note = note
            can_save = True

        if can_save:
            p.save()

    feature_index = {}
    for spec in Specification.objects.filter(product__in=products):
        if spec.product_id not in feature_index:
            feature_index[spec.product_id] = {}

        if spec.feature_id not in feature_index[spec.product_id]:
            feature_index[spec.product_id][spec.feature_id] = []

        feature_index[spec.product_id][spec.feature_id].append(spec)

    raw_features, clean_feautures = json.loads(request.POST.get('features')), {}
    value_separators = ['<div>', '</div>', '<p>', '</p>', '<br/>', ]

    for key in raw_features:
        pid = to_int(key)
        clean_feautures[pid] = {}

        for f in raw_features[key]:
            fid = to_int(f)
            raw_values = raw_features[key][f]

            for separator in value_separators:
                raw_values = raw_values.replace(separator, '<br>')

            clean_feautures[pid][fid] = [v for v in raw_values.split('<br>') if v]

    features_for_delete, features_for_create = [], []
    for pid in clean_feautures:
        for fid in clean_feautures[pid]:
            old_spec = feature_index.get(pid, {}).get(fid, [])

            values = set(clean_feautures[pid][fid])
            old_values = set([f.value for f in old_spec])

            for f in old_spec:
                if f.value not in values:
                    features_for_delete.append(f.pk)

            for new_val in values - old_values:
                features_for_create.append({
                    'fid': fid,
                    'pid': pid,
                    'value': new_val,
                })

    Specification.objects.filter(pk__in=features_for_delete).delete()
    for v in features_for_create:
        spec = Specification(product_id=v['pid'], feature_id=v['fid'], value=v['value'])
        spec.save()

    return HttpResponse('its work')
