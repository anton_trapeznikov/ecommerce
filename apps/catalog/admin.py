from django.contrib import admin
from mptt.admin import MPTTModelAdmin
from apps.catalog.models import *
from apps.helpers.thumbnail import get_thumbnail
from ckeditor.widgets import CKEditorWidget


class CategoryAdmin(MPTTModelAdmin):
    mptt_level_indent = 20
    list_filter = ('is_active', 'parent', 'low_priority')
    list_display = ('name', 'slug', 'parent', 'low_priority', 'is_active',)
    prepopulated_fields = {'slug': ('name',)}
    search_fields = ['name', 'slug']
    readonly_fields = ('updated_at', 'created_at',)

    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget()},
    }


class BrandAdmin(admin.ModelAdmin):
    list_filter = ('is_active', 'country')
    list_display = ('name', 'slug', 'country', 'is_active',)
    prepopulated_fields = {'slug': ('name',)}
    search_fields = ['name', 'slug']
    readonly_fields = ('updated_at', 'created_at',)

    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget()},
    }


class LineAdmin(admin.ModelAdmin):
    list_filter = ('is_active',)
    list_display = ('name', 'slug', 'is_active',)
    prepopulated_fields = {'slug': ('name',)}
    search_fields = ['name', 'slug']
    readonly_fields = ('updated_at', 'created_at',)

    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget()},
    }


class CountryAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug',)
    prepopulated_fields = {'slug': ('name',)}
    search_fields = ['name', 'slug']


class KindAdmin(admin.ModelAdmin):
    list_filter = ('is_active',)
    list_display = ('name', 'slug', 'is_active',)
    prepopulated_fields = {'slug': ('name',)}
    search_fields = ['name', 'slug']
    readonly_fields = ('updated_at', 'created_at',)

    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget()},
    }


class FeatureAdmin(admin.ModelAdmin):
    list_filter = ('for_filter', 'for_itemcard')
    list_display = ('name', 'units', 'for_filter', 'for_itemcard')
    search_fields = ['name', ]


class ProductImageInline(admin.TabularInline):
    model = ProductImage
    extra = 1
    fk_name = 'product'


class SpecificationInline(admin.TabularInline):
    model = Specification
    extra = 1
    fk_name = 'product'
    exclude = ('slugify',)


class ProductAdmin(admin.ModelAdmin):
    list_filter = ('is_active', 'withdrawn', 'instock', 'hight_priority', 'brand', 'category',)
    list_display = (
        'name', 'brand', 'category', 'line', 'kind',
        'instock', 'price', 'hight_priority', 'get_product_thumbnail'
    )
    prepopulated_fields = {'slug': ('name',)}
    search_fields = ['name', 'slug']
    readonly_fields = ('updated_at', 'created_at', '_main_image')
    inlines = [ProductImageInline, SpecificationInline]

    def get_product_thumbnail(self, obj):
        image = obj._main_image
        thumbnail = get_thumbnail(image or '', mode='fit', size='32')

        if thumbnail and len(thumbnail) > 20:
            return '''
                <a
                target="_blank"
                href="/files/%s">
                <div style="
                width:32px;
                height: 32px;
                background-position: center center;
                background-repeat: no-repeat;
                background-image: url(%s);">
                </div></a>
            ''' % (image, thumbnail)
        else:
            return '&mdash;'

    get_product_thumbnail.short_description = 'Фото'
    get_product_thumbnail.allow_tags = True

    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget()},
    }


admin.site.register(Category, CategoryAdmin)
admin.site.register(Brand, BrandAdmin)
admin.site.register(ProductLine, LineAdmin)
admin.site.register(ProductKind, KindAdmin)
admin.site.register(Feature, FeatureAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Country, CountryAdmin)
