from django.db.models.signals import post_save, pre_save, pre_delete
from django.dispatch import receiver
from apps.catalog.models import *
from apps.denormalization.queue import QueueManager


@receiver(pre_save, dispatch_uid='catalog__pre-save')
def catalog_pre_save(sender, **kwargs):
    instance = kwargs['instance'] if 'instance' in kwargs else None

    if instance:
        QueueManager().prepare_instance(instance=instance)

    return True


@receiver(post_save, dispatch_uid='catalog__post-save')
def catalog_post_save(sender, **kwargs):
    instance = kwargs['instance'] if 'instance' in kwargs else None

    if instance:
        QueueManager().manage_queue(prepared_instance=instance)

    if instance and isinstance(instance, ProductImage):
        product = instance.product

        images = ProductImage.objects.filter(product=product).order_by('-is_main')
        image = images[0] if images else None

        if image is None:
            product._main_image = None
        else:
            product._main_image = image.image.name

        product.save()

    return True


@receiver(pre_delete, dispatch_uid='catalog__pre-delete')
def catalog_pre_delete(sender, **kwargs):
    instance = kwargs['instance'] if 'instance' in kwargs else None

    #if instance:
    #    QueueManager().prepare_instance(instance=instance)
    #    QueueManager().manage_queue(prepared_instance=instance)

    if instance and isinstance(instance, ProductImage):
        product = instance.product

        images = ProductImage.objects.filter(product=product).exclude(pk=instance.pk).order_by('-is_main')
        image = images[0] if images else None

        if image is None:
            product._main_image = None
        else:
            product._main_image = image.image.name

        product.save()

    return True
