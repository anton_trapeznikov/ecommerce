from django.db import models
from django.core.urlresolvers import reverse
from mptt.models import MPTTModel, TreeForeignKey, TreeManager
from apps.helpers.utils import generate_upload_name, transliteration
from apps.helpers.models import UrlMixin, ActiveMixin, MetatagsMixin, LogMixin, SearchMixin, ChangedFieldsMixin


class Country(UrlMixin, ChangedFieldsMixin):
    class Meta:
        verbose_name = 'Страна'
        verbose_name_plural = 'Страны'


class Category(MPTTModel, UrlMixin, ActiveMixin, MetatagsMixin, LogMixin, SearchMixin, ChangedFieldsMixin):
    image = models.ImageField(upload_to=generate_upload_name, blank=True, null=True, verbose_name='Картинка')
    menu_name = models.CharField(max_length=256, blank=True, default='', verbose_name='Имя для меню')
    second_name = models.CharField(max_length=256, blank=True, default='', verbose_name='Второе имя')
    note = models.TextField(blank=True, default='', verbose_name='Описание')
    parent = TreeForeignKey(
        'self',
        blank=True,
        null=True,
        related_name='children',
        verbose_name='Родительская категория'
    )
    low_priority = models.BooleanField(
        default=False,
        blank=True,
        verbose_name='Низкоприоритетная категория',
        help_text='Товары этой категории в списке будут отображаться после остальных'
    )
    tree = TreeManager()

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    class MPTTMeta:
        order_insertion_by = ['name']

    def get_absolute_url(self):
        return reverse('catalog:category', kwargs={'slug': self.slug})


class Brand(UrlMixin, ActiveMixin, MetatagsMixin, LogMixin, SearchMixin, ChangedFieldsMixin):
    country = models.ForeignKey(
        Country,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name='Страна',
        help_text='Принадлежность бренда'
    )
    logo = models.ImageField(upload_to=generate_upload_name, blank=True, null=True, verbose_name='Логотип')
    note = models.TextField(blank=True, default='', verbose_name='Описание')
    seo_text = models.TextField(blank=True, default='', verbose_name='SEO-описание')

    class Meta:
        verbose_name = 'Бренд'
        verbose_name_plural = 'Бренды'

    def get_absolute_url(self):
        return reverse('catalog:brand', kwargs={'slug': self.slug})


class ProductLine(UrlMixin, ActiveMixin, MetatagsMixin, LogMixin, SearchMixin, ChangedFieldsMixin):
    image = models.ImageField(upload_to=generate_upload_name, blank=True, null=True, verbose_name='Логотип')
    note = models.TextField(blank=True, default='', verbose_name='Описание')

    class Meta:
        verbose_name = 'Продуктовая линия'
        verbose_name_plural = 'Продуктовые линии'

    def get_absolute_url(self):
        return reverse('catalog:line', kwargs={'slug': self.slug})


class ProductKind(UrlMixin, ActiveMixin, MetatagsMixin, LogMixin, SearchMixin, ChangedFieldsMixin):
    note = models.TextField(blank=True, default='', verbose_name='Описание')

    class Meta:
        verbose_name = 'Разновидность товара'
        verbose_name_plural = 'Разновидности товаров'

    def get_absolute_url(self):
        return reverse('catalog:kind', kwargs={'slug': self.slug})


class Product(UrlMixin, ActiveMixin, MetatagsMixin, LogMixin, SearchMixin, ChangedFieldsMixin):
    code = models.CharField(
        max_length=32,
        unique=True,
        verbose_name='Код товара',
        help_text='Должен быть уникален в рамках каталога'
    )
    category = models.ForeignKey(Category, blank=True, null=True, on_delete=models.SET_NULL, verbose_name='Категория')
    brand = models.ForeignKey(Brand, blank=True, null=True, on_delete=models.SET_NULL, verbose_name='Бренд')
    line = models.ForeignKey(
        ProductLine,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name='Продуктовая линия'
    )
    kind = models.ForeignKey(
        ProductKind,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name='Разновидность товара'
    )
    withdrawn = models.BooleanField(default=False, blank=True, db_index=True, verbose_name='Товар снят с продажи')
    analog = models.ForeignKey('self', blank=True, null=True, verbose_name='Аналог')
    note = models.TextField(blank=True, default='', verbose_name='Описание')
    price = models.FloatField(blank=True, null=True, db_index=True, verbose_name='Цена')
    instock = models.BooleanField(default=False, blank=True, db_index=True, verbose_name='Наличие')
    hight_priority = models.BooleanField(
        default=False,
        blank=True,
        verbose_name='Приоритетный товар',
        help_text='Товар в списке будет отображаться перед остальными'
    )
    _main_image = models.ImageField(upload_to=generate_upload_name, blank=True, null=True, verbose_name='Фотография')

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'

    def get_absolute_url(self):
        return reverse('catalog:product', kwargs={'slug': self.slug})


class Feature(models.Model):
    name = models.CharField(max_length=255, db_index=True, verbose_name='Название')
    units = models.CharField(max_length=16, blank=True, default='', verbose_name='Единица измерения')
    for_filter = models.BooleanField(default=True, db_index=True, verbose_name='Показывать в фильтре')
    for_itemcard = models.BooleanField(default=True, db_index=True, verbose_name='Показывать в карточке товара')

    class Meta():
        verbose_name = 'Характеристика'
        verbose_name_plural = 'Характеристики'
        unique_together = ('name', 'units')

    def __str__(self):
        return '%s, %s' % (self.name, self.units) if self.units else self.name


class Specification(models.Model):
    product = models.ForeignKey(Product, verbose_name='Товар')
    feature = models.ForeignKey(Feature, verbose_name='Характеристика')
    value = models.CharField(max_length=128, verbose_name='Значение')
    slugify = models.CharField(max_length=255, db_index=True, verbose_name='Значение')

    class Meta():
        verbose_name = 'Характеристика товара'
        verbose_name_plural = 'Характеристики товаров'

    def __str__(self):
        return 'Характеристика №%s' % self.pk

    def save(self, *args, **kwargs):
        replaces = [('/', ' div '), ('\\', ' div '), (',', '-pt-'), ('.', '-pt-'), (';', '-')]

        pretty_value = self.value
        for replace_condition in replaces:
            pretty_value = pretty_value.replace(replace_condition[0], replace_condition[1])

        self.slugify = transliteration(pretty_value)
        super(Specification, self).save(*args, **kwargs)


class ProductImage(models.Model, ChangedFieldsMixin):
    product = models.ForeignKey(Product, verbose_name='Товар')
    image = models.ImageField(upload_to=generate_upload_name, verbose_name='Фотография')
    is_main = models.BooleanField(default=False, blank=True, verbose_name='Основное изображение')

    class Meta():
        verbose_name = 'Фото товара'
        verbose_name_plural = 'Фото товаров'

    def __str__(self):
        return 'Фото №%s' % self.pk
