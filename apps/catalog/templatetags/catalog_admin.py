from django import template
from apps.catalog.forms import AdminFilterForm
import locale


locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')
register = template.Library()


@register.inclusion_tag('admin/catalog_extra/product_list.html', takes_context=True)
def build_extra_catalog(context):
    request = context['request']
    filter_form = AdminFilterForm(request=request)

    return {
        'request': request,
        'filter_form': filter_form,
    }
