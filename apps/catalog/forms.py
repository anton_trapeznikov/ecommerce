from django.db.models import Q
from apps.helpers.utils import to_int
from apps.catalog.models import Category, Brand, ProductLine
import json


class FilterForm(object):
    def __init__(self, request, category, exclude_fields=None):
        self.default_block_size = 7
        self.GET = request.GET.copy()
        self.form_fields = []
        self.category = category
        self.min_price = category.min_price or 0
        self.max_price = category.max_price or 0
        self.ordering = None
        self.init_order = 'default'

        if int(self.max_price) != self.max_price:
            self.max_price = int(self.max_price) + 1
        else:
            self.max_price = int(self.max_price)

        if int(self.min_price) != self.min_price:
            self.min_price = int(round(self.min_price))
        else:
            self.min_price = int(self.min_price)

        self.init_price_fields()
        self.init_instock_fields()
        self.init_brands()
        self.init_countries()
        self.init_lines()
        self.init_features()
        self.init_hidden_fields()

    def initial_field(self, field_type, initial):
        for field in self.fields:
            if field['type'] == field_type:
                field['initial'] = initial

    def rebuildCheckboxChoices(self, field):
        field['is_collapse'] = len(field['initial']) == 0 if field['type'] == 'feature' else False
        field['quantity'] = len(field['choices'])

        if field['quantity'] == field['collapse_border']:
            field['collapse_border'] = field['quantity']
        elif field['quantity'] > field['collapse_border']:
            if len(field['initial']) > 0:
                selected_choices, unselected_choices = [], []

                for choice in field['choices']:
                    if choice[0] in field['initial']:
                        selected_choices.append(choice)
                    else:
                        unselected_choices.append(choice)

                field['choices'] = selected_choices + unselected_choices
                if len(selected_choices) >= field['collapse_border']:
                    field['collapse_border'] = len(selected_choices) + 1

        return field

    def filter(self, products):
        for field in self.fields:
            if field['type'] == 'brand':
                if field['initial']:
                    products = products.filter(brand__slug__in=field['initial'])
            elif field['type'] == 'instock':
                if field['initial']:
                    products = products.filter(instock=True)
            elif field['type'] == 'country':
                if field['initial']:
                    products = products.filter(country__slug__in=field['initial'])
            elif field['type'] == 'line':
                if field['initial']:
                    products = products.filter(line__slug__in=field['initial'])
            elif field['type'] == 'feature':
                if field['initial']:
                    tags = ['%s_%s' % (field['feature_id'], slug) for slug in field['initial']]
                    products = products.filter(specification__contains=tags)
            elif field['type'] == 'price':
                if field['default'] != field['initial']:
                    if field['attr'] == 'max':
                        products = products.filter(price__lte=field['initial'])
                    elif field['attr'] == 'min':
                        products = products.filter(price__gte=field['initial'])

        return products

    def init_lines(self):
        lines = json.loads(self.category.lines)
        valid_choice = set([line['slug'].lower() for line in lines])
        set_choice = set([l.lower() for l in self.GET.getlist('line')])

        if lines:
            field = {
                'label': 'Продуктовые линии',
                'name': 'line',
                'whois': 'checkbox',
                'choices': [(line['slug'].lower(), line['name']) for line in lines],
                'initial': valid_choice & set_choice,
                'type': 'line',
                'collapse_border': self.default_block_size,
            }

            self.form_fields.append(self.rebuildCheckboxChoices(field))

    def init_brands(self):
        brand_list = json.loads(self.category.brands)
        valid_choice = set([brand['slug'].lower() for brand in brand_list])
        set_choice = set([b.lower() for b in self.GET.getlist('brand')])

        if brand_list and len(brand_list) > 1:
            field = {
                'label': 'Бренды',
                'name': 'brand',
                'whois': 'checkbox',
                'choices': [(brand['slug'].lower(), brand['name']) for brand in brand_list],
                'initial': valid_choice & set_choice,
                'type': 'brand',
                'collapse_border': self.default_block_size,
            }

            self.form_fields.append(self.rebuildCheckboxChoices(field))

    def init_instock_fields(self):
        instock_param = self.GET.get('instock', None)
        only_instock = instock_param.lower() == 'yes' if instock_param else False

        instock_field = {
            'name': 'instock',
            'whois': 'instock',
            'short_label': 'Наличие',
            'label': 'Показывать только товары в наличии',
            'initial': only_instock,
            'instock_value': 'yes',
            'type': 'instock',
        }

        self.form_fields.append(instock_field)

    def init_countries(self):
        country_list = json.loads(self.category.countries)
        valid_choice = set([country['slug'].lower() for country in country_list])
        set_choice = set([c.lower() for c in self.GET.getlist('country')])

        if country_list and len(country_list) > 1:
            field = {
                'label': 'Страны',
                'name': 'country',
                'whois': 'checkbox',
                'choices': [(country['slug'], country['name']) for country in country_list],
                'initial': valid_choice & set_choice,
                'type': 'country',
                'collapse_border': self.default_block_size,
            }

            self.form_fields.append(self.rebuildCheckboxChoices(field))

    def init_features(self):
        features = json.loads(self.category.features)

        if features:
            for fpk, fdata in features.items():
                feature = '%s, %s' % (fdata['name'], fdata['units']) if fdata['units'] else fdata['name']
                field_name = 'f%s' % fpk
                choices_list = [val for val in fdata['values']]
                valid_choice = set([c[0].lower() for c in choices_list])
                set_choice = set([c.lower() for c in self.GET.getlist(field_name)])

                field = {
                    'label': feature,
                    'is_numeric': fdata['is_numeric'],
                    'name': field_name,
                    'type': 'feature',
                    'whois': 'checkbox',
                    'feature_id': fpk,
                    'choices': choices_list,
                    'initial': valid_choice & set_choice,
                    'collapse_border': self.default_block_size,
                }

                self.form_fields.append(self.rebuildCheckboxChoices(field))

    def init_price_fields(self):
        current_min_price = to_int(self.GET.get('price_min', self.min_price), self.min_price)
        current_max_price = to_int(self.GET.get('price_max', self.max_price), self.max_price)

        if (current_min_price > current_max_price) or (current_min_price < self.min_price):
            current_min_price = self.min_price

        if current_max_price > self.max_price:
            current_max_price = self.max_price

        price_min_field = {
            'label': 'Цена, руб',
            'name': 'price_min',
            'whois': 'price',
            'attr': 'min',
            'type': 'price',
            'default': self.min_price,
            'initial': current_min_price,
        }

        price_max_field = {
            'label': 'Цена, руб',
            'name': 'price_max',
            'whois': 'price',
            'attr': 'max',
            'type': 'price',
            'default': self.max_price,
            'initial': current_max_price,
        }

        self.form_fields.append(price_min_field)
        self.form_fields.append(price_max_field)

    def init_hidden_fields(self):
        current_page = to_int(self.GET.get('page', '1'), 1)
        current_order = self.GET.get('order', 'default')
        current_order = current_order.lower()

        ordering = {
            'default': ['-priority', '-instock', '-have_image', 'price'],
            'name': ['name', ],
            'price-great': ['price', '-instock', '-have_image'],
            'price-less': ['-price', '-instock', '-have_image'],
        }

        if current_order in ordering:
            self.ordering = ordering[current_order]
        else:
            self.ordering = ordering['default']
            current_order = 'default'

        self.init_order = current_order

        order_field = {
            'name': 'order',
            'whois': 'hidden',
            'default': 'default',
            'type': 'order',
            'initial': current_order,
        }

        page_field = {
            'name': 'page',
            'whois': 'hidden',
            'default': '1',
            'initial': current_page,
            'type': 'page',
        }

        self.form_fields.append(order_field)
        self.form_fields.append(page_field)

    @property
    def fields(self):
        return self.form_fields

    @property
    def order_by(self):
        return self.ordering

    @property
    def order_name(self):
        return self.init_order


class AdminFilterForm(object):
    def __init__(self, request):
        self.GET = request.GET.copy()
        self.form_fields = []

        self._init_categories()
        self._init_brands()
        self._init_lines()
        self._init_name()
        self._init_page()

    def _init_categories(self):
        self.categories = Category.objects.all()

        valid_choice = set([c.pk for c in self.categories])
        set_choice = set([to_int(c) for c in self.GET.getlist('cid')])

        self.init_categories = valid_choice & set_choice

    def _init_brands(self):
        self.brands = Brand.objects.all()

        valid_choice = set([b.pk for b in self.brands])
        set_choice = set([to_int(b) for b in self.GET.getlist('bid')])

        self.init_brands = valid_choice & set_choice

    def _init_lines(self):
        self.lines = ProductLine.objects.all()

        valid_choice = set([l.pk for l in self.lines])
        set_choice = set([to_int(l) for l in self.GET.getlist('lid')])

        self.init_lines = valid_choice & set_choice

    def _init_name(self):
        self.init_name = self.GET.get('name', '')

    def _init_page(self):
        self.page = to_int(self.GET.get('page'), 1)

    def filter(self, products):
        if self.init_brands:
            products = products.filter(brand_id__in=self.init_brands)

        if self.init_lines:
            products = products.filter(line_id__in=self.init_lines)

        if self.init_categories:
            products = products.filter(category_id__in=self.init_categories)

        if self.init_name:
            products = products.filter(Q(code__icontains=self.init_name) | Q(name__icontains=self.init_name))

        return products
