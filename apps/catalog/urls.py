from django.conf.urls import url
import apps.catalog.views

urlpatterns = [
    url(r'^$', apps.catalog.views.index, name='index'),
    url(r'^search/$', apps.catalog.views.search, name='search'),
    url(r'^brands/$', apps.catalog.views.brand_list, name='brand_list'),
    url(r'^brand/(?P<slug>[-\w]+)/$', apps.catalog.views.brand, name='brand'),
    url(r'^line/(?P<slug>[-\w]+)/$', apps.catalog.views.line, name='line'),
    url(r'^kind/(?P<slug>[-\w]+)/$', apps.catalog.views.kind, name='kind'),
    url(r'^product/(?P<slug>[-\w]+)/$', apps.catalog.views.product, name='product'),
    url(r'^admin-extra/get-products/$', apps.catalog.views.products_by_admin, name='products_by_admin'),
    url(r'^admin-extra/save-products/$', apps.catalog.views.save_by_admin, name='save_by_admin'),
    url(r'^(?P<slug>[-\w]+)/$', apps.catalog.views.category, name='category'),
]
