from django.contrib import admin
from ecommerce.models import *
from ckeditor.widgets import CKEditorWidget


class SettingsAdmin(admin.ModelAdmin):
    list_display = ('parameter', 'value')


class PhonesAdmin(admin.ModelAdmin):
    list_display = ('phone', 'order_no')
    search_fields = ['phone', ]


class AddressAdmin(admin.ModelAdmin):
    list_display = ('address', 'order_no')
    search_fields = ['address', ]


class PageAdmin(admin.ModelAdmin):
    list_filter = ('is_active',)
    list_display = ('name', 'slug', 'is_active',)
    prepopulated_fields = {'slug': ('name',)}
    search_fields = ['name', 'slug']
    readonly_fields = ('updated_at', 'created_at',)


class CustomScriptAdmin(admin.ModelAdmin):
    list_filter = ('is_active',)
    list_display = ('name', 'for_body',)
    search_fields = ['name', ]


class TextBlocksAdmin(admin.ModelAdmin):
    list_display = ('block',)

    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget(config_name='page')},
    }


class MenuAdmin(admin.ModelAdmin):
    list_display = ('name', 'href', "order_no")


class NotificationRecipientsAdmin(admin.ModelAdmin):
    list_display = ('email',)
    search_fields = ['email', ]


admin.site.register(Settings, SettingsAdmin)
admin.site.register(Page, PageAdmin)
admin.site.register(CustomScript, CustomScriptAdmin)
admin.site.register(HeadMenu, MenuAdmin)
admin.site.register(FooterMenu, MenuAdmin)
admin.site.register(TextBlocks, TextBlocksAdmin)
admin.site.register(NotificationRecipients, NotificationRecipientsAdmin)
admin.site.register(Phones, PhonesAdmin)
admin.site.register(Address, AddressAdmin)
