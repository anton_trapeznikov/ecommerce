import os

DEBUG = False
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = 'cmx84plduxc%u-^l_^qbyu+wr&$lb-ev@k(l#(o_f-l#1)=8_v'
ALLOWED_HOSTS = ['localhost', 'belorekb.ru']

ADMINS = ('anton.trapeznikov@gmail.com',)
MANAGERS = ('anton.trapeznikov@gmail.com',)
SEND_BROKEN_LINK_EMAILS = True

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sites',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'mptt',
    'compressor',
    'haystack',
    'ckeditor',
    'ckeditor_uploader',
    'ecommerce.apps.EcommerceConfig',
    'apps.catalog.apps.CatalogConfig',
    'apps.homepage.apps.HomepageConfig',
    'apps.denormalization.apps.DenormalizationConfig',
    'apps.forms.apps.FormsConfig',
    'apps.order.apps.OrderConfig',
    'apps.manage.apps.ManageConfig',
    'apps.importer.apps.ImporterConfig',
    'apps.helpers',
]

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'ecommerce.urls'

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/5",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "SOCKET_CONNECT_TIMEOUT": 3,
            "SOCKET_TIMEOUT": 3,
            "IGNORE_EXCEPTIONS": True,
        }
    }
}

DJANGO_REDIS_LOG_IGNORED_EXCEPTIONS = True
SESSION_ENGINE = "django.contrib.sessions.backends.cache"
SESSION_CACHE_ALIAS = "default"

CACHED_TEMPLATES = (
    {
        'template': 'core/footer.html',
        'is_common': True,
        'name': 'footer',
    },
    {
        'template': 'core/header.html',
        'is_common': True,
        'name': 'header',
    },
    {
        'template': 'core/head_custom_scripts.html',
        'is_common': True,
        'name': 'head_scripts',
    },
    {
        'template': 'homepage/homepage__content.html',
        'is_common': False,
        'name': 'homepage',
    },
    {
        'template': 'json_ld/organization_sheme.html',
        'is_common': False,
        'name': 'contacts_json_ld',
    },
)

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates'), ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.core.context_processors.media',
                'django.core.context_processors.static',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'ecommerce.processors.cache_processor',
                'ecommerce.processors.metatags_processor',
                'apps.order.processors.cart_processor',
                'ecommerce.processors.canonical_processor',
            ],
        },
    },
]

WSGI_APPLICATION = 'ecommerce.wsgi.application'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LANGUAGE_CODE = 'ru-RU'
TIME_ZONE = 'Asia/Yekaterinburg'
USE_I18N = True
USE_L10N = True
USE_TZ = True

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'www/static')

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
)

MEDIA_URL = '/files/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'www/files')

STATICFILES_DIRS = (os.path.join(BASE_DIR, 'static'),)

DENORMALIZATION_TRANSACTION_SIZE = 500

CKEDITOR_JQUERY_URL = '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js'
CKEDITOR_UPLOAD_PATH = 'uploads/editor/'
CKEDITOR_IMAGE_BACKEND = 'pillow'
CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'adminStandart',
        'height': 300,
        'width': 600,
        'forcePasteAsPlainText': 'true',
    },
    'extended': {
        'toolbar': 'adminExtended',
        'height': 300,
        'width': 600,
        'forcePasteAsPlainText': 'true',
    },
    'page': {
        'toolbar': 'page',
        'height': 300,
        'width': 600,
        'forcePasteAsPlainText': 'true',
    },
}

DEFAULT_FROM_EMAIL = 'no-reply@belorekb.ru'
SERVER_EMAIL = 'no-reply@belorekb.ru'
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.yandex.ru'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'no-reply@belorekb.ru'
EMAIL_HOST_PASSWORD = '8Xb*Ns9fJhNu'

CART_COOKIES_NAME = 'belorekb_cart'

SITE_ID = 1

ORGANIZATION_NAME = 'Belorekb.ru'
ORGANIZATION_SHORT_DESCRIPTION = 'Магазин доступной и качественной косметики.'

METATAGS = {
    'catalog_root': {
        'title': 'Белорусская косметика от {brands}',
        'keywords': '{brands}',
        'description': 'Парфюмерия и косметика из Белоруссии! {product_count} '
                       '{product_declination} от {brand_count} '
                       '{brand_declination}: {brands} ',
    },

    'contacts': {
        'title': 'Адреса и контакты',
        'keywords': 'екатеринбург, верхняя, пышма',
        'description': 'Магазины: {addresses}.'
                       'Телефоны: {phones}',
    },

    'card': {
        'title': 'Белорусская косметика от {brands}',
        'keywords': '{brands}',
        'description': 'Парфюмерия и косметика из Белоруссии! {product_count} '
                       '{product_declination} от {brand_count} '
                       '{brand_declination}: {brands} ',
    },
}


try:
    from .local_settings import *
    from .haystack_settings import *
    # from .haystack_local_settings import *
except ImportError:
    pass

if DEBUG:
    LOGGING = {
        "version": 1,
        "disable_existing_loggers": True,
        "formatters": {
            "simple": {
                "format": "%(levelname)s %(message)s"
            },
        },
        "handlers": {
            "console": {
                "level": "DEBUG",
                "class": "logging.StreamHandler",
                "formatter": "simple"
            },
            "logfile": {
                "class": "logging.handlers.WatchedFileHandler",
                "filename": os.path.join(BASE_DIR, "log/belorekb.log")
            },
        },
        "loggers": {
            "django": {
                "handlers": ["logfile", "console"],
                "level": "ERROR",
                "propagate": True,
            },
            "ecommerce": {
                "handlers": ["logfile", "console"],
                "level": "DEBUG",
                "propagate": True,
            },
        },
    }
else:
    import raven

    INSTALLED_APPS.append('raven.contrib.django.raven_compat')

    RAVEN_CONFIG = {
        'dsn': 'https://31e27b3de591488eb708152fa69849f8:2ddfb13f26e14513851b3b47b44e5549@sentry.io/219134',
        'release': raven.fetch_git_sha(os.path.dirname(os.pardir)),
    }

    LOGGING = {
        "version": 1,
        "disable_existing_loggers": True,
        "filters": {
            "require_debug_false": {
                "()": "django.utils.log.RequireDebugFalse"
            }
        },
        "handlers": {
            "sentry": {
                "class": "raven.contrib.django.raven_compat.handlers.SentryHandler",
            },
        },
        "loggers": {
            "django": {
                "handlers": ["sentry"],
                "level": "ERROR",
                "propagate": True,
            },
            "ecommerce": {
                "handlers": ["sentry"],
                "level": "WARNING",
                "propagate": True,
            },
        },
    }

COMPRESS_ENABLED = not DEBUG
COMPRESS_OUTPUT_DIR = 'compress'
COMPRESS_CSS_FILTERS = [
    'compressor.filters.css_default.CssAbsoluteFilter',
    'compressor.filters.cssmin.CSSMinFilter'
]