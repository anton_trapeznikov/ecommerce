import os

HAYSTACK_XAPIAN_LANGUAGE = 'russian'

try:
    import xapian
    XAPIAN_FLAGS = (
        xapian.QueryParser.FLAG_PHRASE |
        xapian.QueryParser.FLAG_BOOLEAN |
        xapian.QueryParser.FLAG_LOVEHATE |
        xapian.QueryParser.FLAG_WILDCARD |
        xapian.QueryParser.FLAG_PURE_NOT |
        xapian.QueryParser.FLAG_PARTIAL |
        xapian.QueryParser.FLAG_SYNONYM
    )
except ImportError:
    XAPIAN_FLAGS = None

HAYSTACK_DEFAULT_OPERATOR = 'OR'


HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'xapian_backend.XapianEngine',
        'PATH': os.path.join(os.path.dirname(__file__), 'xapian_index'),
        'HAYSTACK_XAPIAN_LANGUAGE': 'russian',
        # 'INCLUDE_SPELLING': True,
        # 'BATCH_SIZE': 100,
        'FLAGS': XAPIAN_FLAGS,
    },
}
