# -*- coding: utf-8 -*-

from ecommerce.models import Settings
from apps.denormalization.models import PreparedTemplates


def canonical_processor(request):
    try:
        canonical = request.build_absolute_uri('?')
    except Exception:
        canonical = None

    return {
        'rel_canonical': canonical,
    }


def cache_processor(request):
    return {
        'cache': {c.name: c.content for c in PreparedTemplates.objects.filter(is_common=True)},
    }


def metatags_processor(request):
    fields = ['default_title', 'default_keywords', 'default_description']
    whois = 'default'
    variables = ['', '', '']
    blanks = {
        'title': None,
        'default_title': None,
        'keywords': None,
        'default_keywords': None,
        'description': None,
        'default_description': None,
    }

    try:
        if hasattr(request, 'meta_tags'):
            meta_tags_data = request.meta_tags
            whois = meta_tags_data['whois']
            variables[0] = meta_tags_data['title']
            variables[1] = meta_tags_data['keywords']
            variables[2] = meta_tags_data['description']
    except Exception:
        whois = 'default'
        variables = ['', '', '']

    if whois == 'page':
        fields += ['page_title', 'page_keywords', 'page_description']
    elif whois == 'catalog_index':
        fields += ['catalog_index_title', 'catalog_index_keywords', 'catalog_index_description']
    elif whois == 'root_category':
        fields += ['root_category_title', 'root_category_keywords', 'root_category_description']
    elif whois == 'category':
        fields += ['category_title', 'category_keywords', 'category_description']
    elif whois == 'brand':
        fields += ['brand_title', 'brand_keywords', 'brand_description']
    elif whois == 'brands':
        fields += ['brands_title', 'brands_keywords', 'brands_description']
    elif whois == 'product':
        fields += ['product_title', 'product_keywords', 'product_description']
    elif whois == 'homepage':
        fields += ['homepage_title', 'homepage_keywords', 'homepage_description']
    elif whois == 'search':
        fields += ['search_title', 'search_keywords', 'search_description']

    for s in Settings.objects.filter(parameter__in=fields):
        if 'title' in s.parameter:
            if 'default_' in s.parameter:
                blanks['default_title'] = s.value
            else:
                blanks['title'] = s.value

        if 'keywords' in s.parameter:
            if 'default_' in s.parameter:
                blanks['default_keywords'] = s.value
            else:
                blanks['keywords'] = s.value

        if 'description' in s.parameter:
            if 'default_' in s.parameter:
                blanks['default_description'] = s.value
            else:
                blanks['description'] = s.value

    title = blanks['title'] or blanks['default_title'] or ''
    keywords = blanks['keywords'] or blanks['default_keywords'] or ''
    description = blanks['description'] or blanks['default_description'] or ''

    while '__X__' in title:
        title = title.replace('__X__', variables[0])

    while '__X__' in keywords:
        keywords = keywords.replace('__X__', variables[1])

    while '__X__' in description:
        description = description.replace('__X__', variables[2])

    return {
        'metatag_title': title,
        'metatag_keywords': keywords,
        'metatag_description': description,
    }
