from django.db import models
from django.core.urlresolvers import reverse
from apps.helpers.models import UrlMixin, ActiveMixin, MetatagsMixin, LogMixin, OrderMixin
from ckeditor_uploader.fields import RichTextUploadingField


class NotificationRecipients(models.Model):
    email = models.EmailField(verbose_name='Адрес электронной почты')

    class Meta:
        verbose_name = 'Получатель уведомлений'
        verbose_name_plural = 'Получатели уведомлений'

    def __str__(self):
        return self.email


class Settings(models.Model):
    SETTINGS_CODE = (
        ('robots', 'Содержимое файла robots.txt'),

        ('default_title', 'Title по умолчанию'),
        ('default_keywords', 'Keywords по умолчанию'),
        ('default_description', 'Description по умолчанию'),

        ('homepage_title', 'Title главной страницы'),
        ('homepage_keywords', 'Keywords главной страницы'),
        ('homepage_description', 'Description главной страницы'),

        ('page_title', 'Title страницы сайта'),
        ('page_keywords', 'Keywords страницы сайта'),
        ('page_description', 'Description страницы сайта'),

        ('catalog_index_title', 'Title корня каталога'),
        ('catalog_index_keywords', 'Keywords корня каталога'),
        ('catalog_index_description', 'Description корня каталога'),

        ('root_category_title', 'Title корневой категории'),
        ('root_category_keywords', 'Keywords корневой категории'),
        ('root_category_description', 'Description корневой категории'),

        ('category_title', 'Title категории'),
        ('category_keywords', 'Keywords категории'),
        ('category_description', 'Description категории'),

        ('brand_title', 'Title бренда'),
        ('brand_keywords', 'Keywords бренда'),
        ('brand_description', 'Description бренда'),

        ('brands_title', 'Title списка брендов'),
        ('brands_keywords', 'Keywords списка брендов'),
        ('brands_description', 'Description списка брендов'),

        ('product_title', 'Title карточки товара'),
        ('product_keywords', 'Keywords карточки товара'),
        ('product_description', 'Description карточки товара'),

        ('search_title', 'Title страницы поиска'),
        ('search_keywords', 'Keywords страницы поиска'),
        ('search_description', 'Description страницы поиска'),

        ('catalog_h1', 'Заголовок каталога'),
        ('catalog_h2', 'Подзаголовок каталога'),

        ('brands_h1', 'Заголовок списка брендов'),
        ('brands_h2', 'Подзаголовок списка брендов'),
    )


    parameter = models.CharField(max_length=32, unique=True, choices=SETTINGS_CODE, verbose_name='Параметр')
    value = models.TextField(blank=True, default='', verbose_name='Значение')

    class Meta:
        verbose_name = 'Настройка сайта'
        verbose_name_plural = 'Настройки сайта'

    def __str__(self):
        human_value = None
        if self.parameter:
            for code in self.SETTINGS_CODE:
                if self.parameter == code[0]:
                    human_value = code[1]

        human_value = human_value or self.parameter or 'Настройки сайта'

        return human_value


class Page(UrlMixin, ActiveMixin, MetatagsMixin, LogMixin):
    content = RichTextUploadingField(blank=True, verbose_name='Контент')

    class Meta:
        verbose_name = 'Страница сайта'
        verbose_name_plural = 'Страницы сайта'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('page', kwargs={'slug': self.slug})


class CustomScript(ActiveMixin):
    name = models.CharField(
        max_length=255,
        unique=True,
        verbose_name='Название',
        help_text='Это поле необходимо для человекопонятного обозначения содержимого. В логике участия не принимает.'
    )
    code = models.TextField(
        verbose_name='Содержимое',
        help_text='Содержимое будет размещено на странице "как есть"'
    )
    for_body = models.BooleanField(
        default=True,
        blank=True,
        verbose_name='Вставлять в BODY',
        help_text='Если установлено, то содержимое будет вставлено в теге BODY, в противном случае в теге HEAD'
    )

    class Meta:
        verbose_name = 'Сторонний скрипт\контент'
        verbose_name_plural = 'Сторонние скрипты\контент'

    def __str__(self):
        return self.name


class HeadMenu(ActiveMixin, OrderMixin):
    name = models.CharField(max_length=32, unique=True, verbose_name='Имя пункта верхнего меню')
    href = models.CharField(max_length=255, blank=True, default='', verbose_name='Ссылка')

    class Meta:
        verbose_name = 'Пункт верхнего меню'
        verbose_name_plural = 'Пункты верхнего меню'

    def __str__(self):
        return self.name


class FooterMenu(ActiveMixin, OrderMixin):
    name = models.CharField(max_length=32, unique=True, verbose_name='Пункт меню')
    href = models.CharField(max_length=255, blank=True, default='', verbose_name='Ссылка')

    class Meta:
        verbose_name = 'Пункт нижнего меню'
        verbose_name_plural = 'Пункты нижнего меню'

    def __str__(self):
        return self.name


class Phones(OrderMixin):
    phone = models.CharField(max_length=32, unique=True, verbose_name='Телефон')
    for_order = models.BooleanField(
        default=False,
        blank=True,
        verbose_name='Для приема заказов'
    )
    for_consultation = models.BooleanField(
        default=False,
        blank=True,
        verbose_name='Для консультаций'
    )
    note = models.CharField(
        max_length=255,
        blank=True,
        default='',
        verbose_name='Примечание'
    )

    class Meta:
        verbose_name = 'Телефон'
        verbose_name_plural = 'Телефоны'

    def __str__(self):
        return self.phone

    @property
    def clean_phone(self):
        return '+%s' % ''.join([n for n in self.phone if n.isdigit()])


class Address(OrderMixin):
    address = models.CharField(
        max_length=128,
        unique=True,
        verbose_name='Адрес',
        help_text='Короткая человекопонятная запись адреса',
    )

    title = models.CharField(
        max_length=512,
        blank=True,
        default='',
        verbose_name='Название магазина',
    )

    city = models.CharField(
        max_length=32,
        blank=True,
        default='',
        verbose_name='Название населенного пункта',
    )

    region = models.CharField(
        max_length=32,
        blank=True,
        default='',
        verbose_name='Название региона',
    )

    street_address = models.CharField(
        max_length=32,
        blank=True,
        default='',
        verbose_name='Адрес в населенном пункте',
    )

    market = models.CharField(
        max_length=128,
        blank=True,
        default='',
        verbose_name='Название ТЦ',
        help_text='Если магазин расположен в ТЦ, то его необходимо \
                   указать в этом поле.'
    )

    latitude = models.CharField(
        max_length=32,
        blank=True,
        null=True,
        verbose_name='Широта',
        help_text='Координаты можно взять в сервисе интерактивных карт.'
    )

    longitude = models.CharField(
        max_length=32,
        blank=True,
        null=True,
        verbose_name='Долгота',
        help_text='Координаты можно взять в сервисе интерактивных карт.'
    )

    opening_hours = models.CharField(
        max_length=32,
        blank=True,
        null=True,
        verbose_name='Режим работы',
        help_text='В машинопонятном виде. Например, Mo-Su 10:00-20:00.'
    )

    opening_hours_verbose = models.CharField(
        max_length=64,
        blank=True,
        null=True,
        verbose_name='Режим работы',
        help_text='В человекопонятном виде.'
    )

    map_code = models.TextField(
        blank=True,
        null=True,
        verbose_name='Код интерактивной карты Google',
    )

    class Meta:
        verbose_name = 'Адрес магазина'
        verbose_name_plural = 'Адреса магазинов'

    def __str__(self):
        return self.address


class TextBlocks(models.Model):
    BLOCK_CODE = (
        ('catalog_index', 'Текст на главной странице каталога'),
        ('brand_index', 'Текст в списке брендов'),
        ('homepage', 'Текст на главной странице'),
    )

    block = models.CharField(max_length=32, unique=True, choices=BLOCK_CODE, verbose_name='Текстовый блок')
    text = models.TextField(blank=True, default='', verbose_name='Текст')

    class Meta:
        verbose_name = 'Текстовый блок'
        verbose_name_plural = 'Текстовые блоки'

    def __str__(self):
        human_value = None
        if self.block:
            for code in self.BLOCK_CODE:
                if self.block == code[0]:
                    human_value = code[1]

        human_value = human_value or self.parameter or 'Текстовый блок'

        return human_value
