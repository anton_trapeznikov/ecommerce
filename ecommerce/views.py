from django.core.exceptions import ObjectDoesNotExist
from django.template.response import TemplateResponse
from django.http import Http404, HttpResponse
from django.conf import settings
from ecommerce.models import Page, Settings, Address, Phones
from apps.helpers.utils import get_metatags_params
from apps.order.models import Order, OrderItem
import mimetypes
import os


def page(request, slug):
    try:
        page = Page.objects.get(slug=slug, is_active=True)
    except ObjectDoesNotExist:
        raise Http404

    request.meta_tags = get_metatags_params(whois='page', obj=page)

    return TemplateResponse(request, 'core/page.html', {
        'page': page,
    })


def page_404(request):
    return TemplateResponse(request, '404.html', {})


def robots_txt(request):
    content = ''
    try:
        setting = Settings.objects.get(parameter='robots')
        content = setting.value
    except ObjectDoesNotExist:
        content = ''

    return TemplateResponse(
        request,
        'core/robots.html',
        {'content': content},
        content_type='text/plain'
    )


def sitemap(request):
    directory = os.path.join(settings.MEDIA_ROOT, 'sitemap')
    file_path = os.path.join(directory, 'sitemap.xml')

    if os.path.exists(file_path):
        file_sock = open(file_path, 'rb')
        mimetype = mimetypes.guess_type(file_path)[0]
        return HttpResponse(file_sock, content_type=mimetype)

    raise Http404


def thank_you_page(request):
    uid = request.GET.get('order')
    order = Order.objects.filter(uid=uid).first()
    if not order:
        raise Http404

    return TemplateResponse(request, 'order/thank_you.html', {
        'order': order,
    })


def contact_page(request):
    addresses = Address.objects.all().order_by('order_no')
    phones = Phones.objects.all().order_by('order_no')

    meatatags_template = settings.METATAGS['contacts']

    title = meatatags_template['title']
    keywords = meatatags_template['keywords']
    description = meatatags_template['description'].format(**{
        'addresses': '; '.join(a.address for a in addresses),
        'phones': ' '.join(p.phone for p in phones),
    })

    request.meta_tags = get_metatags_params(
        whois='page',
        title=title,
        keywords=keywords,
        description=description,
    )

    return TemplateResponse(request, 'core/contacts.html', {
        'addresses': addresses,
        'phones': phones,
        'with_json_ld_contacts': True,
    })
