from django.contrib.auth import views as auth_views
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
import apps.homepage.views
import ecommerce.views

urlpatterns = [
    url(r'^$', apps.homepage.views.homepage, name='homepage'),
    url(r'^admin/', admin.site.urls),
    url(r'^admin/password_reset/$', auth_views.password_reset, name='admin_password_reset'),
    url(r'^admin/password_reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    url(
        r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$',
        auth_views.password_reset_confirm,
        name='password_reset_confirm'
    ),
    url(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),
    url(r'^robots.txt', ecommerce.views.robots_txt, name='robots_txt'),
    url(r'^404/', ecommerce.views.page_404, name='page_404'),
    url(r'^thank-you/', ecommerce.views.thank_you_page, name='thank_you_page'),
    url(r'^contacts/', ecommerce.views.contact_page, name='contacts'),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^forms/', include('apps.forms.urls', namespace='forms')),
    url(r'^catalog/', include('apps.catalog.urls', namespace='catalog')),
    url(r'^cart/', include('apps.order.urls', namespace='order')),
    url(r'^manage/', include('apps.manage.urls', namespace='manage')),
    url(r'^sitemap.xml$', ecommerce.views.sitemap, name='sitemap'),
    url(r'^(?P<slug>[-\w]+)/$', ecommerce.views.page, name='page'),
]

if settings and settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
