from django.core.management.base import BaseCommand
from django.template.loader import render_to_string
from django.conf import settings
from apps.denormalization.models import *
from ecommerce.models import Page
import os


class Command(BaseCommand):
    help = 'Построение карты сайта'

    def handle(self, *args, **options):
        urls = [{'url': '/'}, {'url': '/catalog/'}, {'url': '/catalog/brands/'}]

        for p in Page.objects.filter(is_active=True):
            urls.append({
                'url': p.get_absolute_url,
                'lastmod': p.updated_at or p.created_at,
            })

        catalog_entries = list(PreparedProduct.objects.all().select_related('entry'))
        catalog_entries += list(PreparedCategory.objects.all().select_related('entry'))
        catalog_entries += list(PreparedBrand.objects.all().select_related('entry'))

        for ce in catalog_entries:
            urls.append({
                'url': ce.url,
                'lastmod': ce.entry.updated_at or ce.entry.created_at,
            })

        sitemap_content = render_to_string('sitemap_xml.html', {'urls': urls})

        directory = os.path.join(settings.MEDIA_ROOT, 'sitemap')
        file_path = os.path.join(directory, 'sitemap.xml')

        if not os.path.exists(directory):
            os.makedirs(directory)

        if os.path.exists(file_path):
            os.remove(file_path)

        sitemap_file = open(file_path, 'w+')
        sitemap_file.write(sitemap_content)
        sitemap_file.close()
