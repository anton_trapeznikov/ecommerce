from django import template
from django.utils.safestring import mark_safe
from django.utils import timezone
from apps.denormalization.models import *
from apps.denormalization.models import PreparedProduct
from ecommerce.models import Address, Phones
from django.db.models import Max, Min
from django.conf import settings
from apps.helpers.utils import *
from ecommerce.models import *
import locale


locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')
register = template.Library()


@register.simple_tag(takes_context=True)
def include_prepared(context, path):
    templates = PreparedTemplates.objects.filter(path=path)
    return mark_safe(templates[0].content) if templates else ''


@register.inclusion_tag('core/head_custom_scripts__inner.html', takes_context=True)
def build_head_custom_scripts(context):
    request = context['request']

    return {
        'request': request,
        'custom_scripts': CustomScript.objects.filter(for_body=False),
    }


@register.inclusion_tag('json_ld/organization_sheme__inner.html')
def build_organization_sheme():
    phones = Phones.objects.all()

    return {
        'price_stat': PreparedProduct.objects.all().aggregate(Max('price'), Min('price')),
        'main_phone': phones[0] if phones else None,
        'organization_name': settings.ORGANIZATION_NAME,
        'organization_description': settings.ORGANIZATION_SHORT_DESCRIPTION,
        'phones': phones,
        'addresses': Address.objects.all(),
    }


@register.inclusion_tag('core/footer__inner.html', takes_context=True)
def build_footer(context):
    request = context['request']

    return {
        'request': request,
        'custom_scripts': CustomScript.objects.filter(for_body=True),
        'footer_menu': FooterMenu.objects.all().order_by('order_no'),
        'phones': Phones.objects.all().order_by('order_no'),
        'address': Address.objects.all().order_by('order_no'),
        'current_year': timezone.now().date().year,
    }


@register.inclusion_tag('core/header__inner.html', takes_context=True)
def build_header(context):
    request = context['request']
    catalog_menu = PreparedCategory.objects.filter(level=0).order_by('name')
    for category in catalog_menu:
        children = list(PreparedCategory.objects.filter(entry__parent_id=category.entry_id).order_by('name'))
        category.has_children = False
        for c in children:
            category.has_children = True
            c.column_weight = 1
            c.column_weight += len(c.children) if c.children else 0

        category.columns = distribute_columns(children, 3)

    return {
        'request': request,
        'head_menu': HeadMenu.objects.all(),
        'phones': Phones.objects.all().order_by('order_no'),
        'catalog_menu': catalog_menu,
    }
