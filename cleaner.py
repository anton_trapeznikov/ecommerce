from apps.catalog.models import Product



# trash = ('12шт/уп', '')
trash = ('NEW', '')
# trash = ('//', ' ')
# trash = ('()', '')
# trash = ('**', ' ')
trash = ('  ', ' ')

can_save = True

products = Product.objects.filter(name__icontains=trash[0])

for p in products:
    name = p.name
    print('--------------------')
    print(name)
    new_name = name.replace(trash[0], trash[1])
    new_name = new_name.strip()
    print(new_name)
    if can_save:
        p.name = new_name
        p.save()

